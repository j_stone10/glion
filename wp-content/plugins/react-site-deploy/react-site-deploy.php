<?php
defined( 'ABSPATH' ) OR exit;
/*
Plugin Name: React Site Deploy
Description: Plugin for deploying site to stage or prod
Version: 1.0
Author: Jamie Stone
Author URI: http://bubblweb.eu
*/

function rsd_admin_menu() {
	add_menu_page( 'React Site Deploy', 'React Deploy', 'manage_options', 'rsd', 'rsd_admin_page', 'dashicons-admin-site', 85 );
}
add_action( 'admin_menu', 'rsd_admin_menu' );

function rsd_admin_page() {
	?>
	<div class="wrap">
		<h1>React Site Deploy</h1>
		<p>You can use this plugin to publish your content to your Stage or Production sites.</p>
		<p>When you are ready to push new changes to the live site, the following process should be followed.</p>
		<ol>
			<li>Click publish to stage button. <small>Please only click once so as to not fire mulitple rebuilds simultaneously</small> </li>
			<li>You will receive email confirmation shortly after that the build process has started</li>
			<li>Once the build process has completed, you will receive a further success confirmation. After this you can test the site at the stage url: </li>
			<li>If everything looks good when tested, proceed to the next stage, which is clicking the publish to live button</li>
			<li>The process will repeat and when you receive final confirmation email go to the prod site at: to check everything looks good</li>
			<li>Happy days, we're now live! And how much easier was that?</li>
	
</ol>
<p>If you experience any issues throughout the build process, please do not hesistate to contact <a href="mailto:jamie.s@pomegranate.co.uk">Jamie Stone</a> for assistance.</p>
		
	<!-- PUBLISH TO STAGE BUTTON -->

		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=rsd">
		<input type="submit" name="publish-to-stage" value="Deploy to Stage"><br>
		</form>

		<?php 
		// var_dump($_POST);
		if(isset($_POST['publish-to-stage']))  { 
			$custom_data = array(
				'data_1' => 'value'
			);
			$webhook_names = array(
				'publish-to-stage'
			);

			do_action( 'wp_webhooks_send_to_webhook', $custom_data, $webhook_names );

			echo "<p>Your site is now building to stage. Please check email for confirmation when complete";
		 }  
		 ?>

<!-- PUBLISH TO LIVE BUTTON -->
		 
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=rsd">
		<input type="submit" name="publish-to-live" value="Deploy to Production"><br>
		</form>

		<?php 
		// var_dump($_POST);
		if(isset($_POST['publish-to-live']))  { 
			$custom_data = array(
				'data_1' => 'value'
			);
			$webhook_names = array(
				'publish-to-live'
			);

			do_action( 'wp_webhooks_send_to_webhook', $custom_data, $webhook_names );

			echo "<p>Your site is now building to production site. Please check email for confirmation when complete";
		 }  
		 ?>




	</div>
	<?php
}
?>