<?php
/*
Plugin Name: Glion Pay
Version: 1.1
Description: Custom invoice payment plugin, for Glion.edu
Author: Details.ch
Text Domain: glion_pay
*/
global $glionpay_db_version;
$glionpay_db_version = '1.1';

class glion_pay {
    private $defaults = array(
        'version'                           => '1.0'
    );

    function __construct() {

          //add quick links to plugins page
        $plugin = plugin_basename( __FILE__ );
        if ( is_admin() ) {
            add_filter("plugin_action_links_$plugin", array(&$this, 'settings_link'));
            register_activation_hook( __FILE__, array( &$this, 'activate' ) );
            register_deactivation_hook(__FILE__, array(&$this, 'deactivate' ));
        }
        register_activation_hook( __FILE__, array( &$this, 'glionpay_install') );
        wp_enqueue_script('glionpay', plugins_url( 'js/glionpay.js', __FILE__ ), array( 'jquery' ), $this->defaults['version'], 'footer');
        wp_enqueue_style( 'glionpay', plugins_url( 'css/glionpay.css', __FILE__ ) );
        add_shortcode("glionpay", array( $this, "glionpay_shortcode"));
        add_action( 'plugins_loaded', array( $this, 'glionpay_update_db_check' ));
    }// end function

    function activate() {
        // stuff to do when the plugin is activated
        $this->set_defaults();
    }// end function

    function deactivate() {
        // stuff to do when plugin is deactivated
        // i.e. delete_option('glion_pay');
        $options = $this->get_options();
        if (isset($options))
            delete_option( 'glion_pay' );
    }// end function

    function settings_link($links) {
        $settings_link = '<a href="options-general.php?page=glion-pay/admin.php">Settings</a>';
        array_unshift( $links, $settings_link );
        return $links;
    }// end function

    function get_options() {
        $options = get_option( 'glion_pay' );
        if ( !is_array( $options ) )
            $options = $this->set_defaults();
        return $options;
    }// end function

    function set_defaults() {
        $options = array(
            'api_user' => 'API_244126_11862201',
            'api_pwd' => 'JsonApiPwd1_GkcU7TA4',
            'customer_id' => '244126',
            'terminal_id' => '17905521',
            'merchant_email' => 'fbe@details.ch',
            'currency_code' => 'EUR',
            'saferpay_server' => 'https://test.saferpay.com',
        );
        update_option( 'glion_pay', $options );
        return $options;
    }// end function

    function glionpay_install () {
        global $wpdb;
        global $glionpay_db_version;
        $installed_ver = get_option( "glionpay_db_version" );
        $table_name = $wpdb->prefix . "glionpay_transaction";
        $charset_collate = $wpdb->get_charset_collate();
        if ( $installed_ver != $glionpay_db_version ) {
            $sql = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                requestid tinytext NOT NULL,
                token tinytext NOT NULL,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );

            update_option( 'glionpay_db_version', $glionpay_db_version );
        }
    }
    
    function glionpay_update_db_check() {
        global $glionpay_db_version;
        $installed_ver = get_option( "glionpay_db_version" );
        if ($installed_ver != $glionpay_db_version) {
            $this->glionpay_install();
        }
    }

    function glionpay_shortcode($atts) {
        $payform = '<form id="glionPayForm" method="post" action="'.plugins_url().'/glion-pay/payment.php">'.
        '<label>'.__('Student ID').'</label><input type="text" name="studentid" id="studentid"><br>'.
        '<label>'.__('Student First name').'</label><input type="text" name="firstname" id="firstname"><br>'.
        '<label>'.__('Student Last name').'</label><input type="text" name="lastname" id="lastname"><br>'.
        '<label>'.__('Amount in US Dollars').'</label><input type="text" name="amount" id="amount" placeholder="0.00"><br>'.
        '<input type="submit" value="'.__('Pay').'">'.
        '</form>';
        return $payform;
    }

    function getPendingTransactions() {
        global $wpdb;
        $tokens = array();
        $table_name = $wpdb->prefix.'glionpay_transaction';
        $results = $wpdb->get_results( "SELECT * FROM {$table_name} ", OBJECT );

        foreach ($results as $transaction) {
            $tokens[$transaction->requestid] = $transaction->token;
        }
        return $tokens;
    }

    function clearTransaction($requestId) {
        global $wpdb;    
        $table_name = $wpdb->prefix . 'glionpay_transaction';
        $wpdb->delete( 
            $table_name, 
            array( 
                'requestId' => $requestId, 
            ) 
        );
    }

    function addTransaction($requestId, $token) {
        global $wpdb;    
        $table_name = $wpdb->prefix . 'glionpay_transaction';
        
        $wpdb->insert( 
            $table_name, 
            array( 
                'time' => current_time( 'mysql' ), 
                'requestId' => $requestId, 
                'token' => $token, 
            ) 
        );
    }
}// end class
$glion_pay = new glion_pay();

if ( is_admin() ) {
    include_once dirname( __FILE__ ).'/admin.php';
}