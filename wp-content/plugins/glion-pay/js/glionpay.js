( function ( $ ) {
    $('#glionPayForm').submit(function() {
        return glionpay_checkForm();
    })
    function glionpay_checkForm() {
        $validated = true;
        if ($('#studentid').val() == '') {
            alert('Student Id is mandatory');
            $validated = false;
        }
        if ($('#firstname').val() == '') {
            alert('First name is mandatory');
            $validated = false;
        }
        if ($('#lastname').val() == '') {
            alert('Last name is mandatory');
            $validated = false;
        }
        if ($('#amount').val() == '') {
            alert('Amount is mandatory');
            $validated = false;
        } else  if (!$.isNumeric($('#amount').val())) {
            alert('Amount must be numeric');
            $validated = false;
        } else if(parseFloat($('#amount').val())>99999) {
            alert('Amount cannot be over 100000');
            $validated = false;
        }
        return $validated;
    }
} )( jQuery );

