<?php
// load wordpress
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

//$username and $password for the http-Basic Authentication
//$url is the SaferpayURL eg. https://www.saferpay.com/api/Payment/v1/PaymentPage/Initialize
//$payload is a multidimensional array, that assembles the JSON structure. Example see above
function do_curl($username,$password,$url, $payload){
    //Set Options for CURL
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_VERBOSE, true);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //Return Response to Application
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //Set Content-Headers to JSON
    curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Accept: application/json; charset=utf-8"));
    //Execute call via http-POST
    curl_setopt($curl, CURLOPT_POST, true);
    //Set POST-Body
    //convert DATA-Array into a JSON-Object
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
    //WARNING!!!!!
    //This option should NOT be "false"
    //Otherwise the connection is not secured
    //You can turn it of if you're working on the test-system with no vital data
    //PLEASE NOTE:
    //Under Windows (using WAMP or XAMP) it is necessary to manually download and save the necessary SSL-Root certificates!
    //To do so, please visit: http://curl.haxx.se/docs/caextract.html and Download the .pem-file
    //Then save it to a folder where PHP has write privileges (e.g. the WAMP/XAMP-Folder itself)
    //and then put the following line into your php.ini:
    //curl.cainfo=c:\path\to\file\cacert.pem
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
    //HTTP-Basic Authentication for the Saferpay JSON-API.
    //This will set the authentication header and encode the password & username in Base64 for you
    curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
    //CURL-Execute & catch response
    $jsonResponse = curl_exec($curl);
    //Get HTTP-Status
    //Abort if Status != 200
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ( $status != 200 ) {
        //IF ERROR
        //Get http-Body (if aplicable) from CURL-response
        $body = json_decode(curl_multi_getcontent($curl),true);
        //Build array, containing the body (Response data, like Error-messages etc.) and the http-status-code
        $response = array(
            "status" => $status,
            "body" => $body
        );
    }else{
        //IF OK
        //Convert response into an Array
        $body = json_decode($jsonResponse, true);
        //Build array, containing the body (Response-data) and the http-status-code
        $response = array(
            "status" => $status,
            "body" => $body
        );
    }
    //IMPORTANT!!!
    //Close connection!
    curl_close($curl);
    //$response, again, is a multi-dimensional Array, containing the status-code ($response["status"]) and the API-response (if available) itself ($response["body"])
    return $response;
}

// Build Saferpay call
global $glion_pay;
$options = $glion_pay->get_options();
$apiUser = $options['api_user'];
$apiPwd = $options['api_pwd'];
$saferPayCustomerId = $options['customer_id'];
$terminalId = $options['terminal_id'];
$currencyCode = $options['currency_code'];
$successUrl = $options['success_url'];
$failUrl = $options['fail_url'];
$merchantEmail = $options['merchant_email'];
$saferPayServer = $options['saferpay_server'];

// for each transaction token stored, check if payment accepted then do capture event
foreach ($glion_pay->getPendingTransactions() as $requestId => $token) {
    // check assertion to get transactionId
    $payload = array(
        'RequestHeader' => array(
            'SpecVersion' => "1.7",
            'CustomerId' => $saferPayCustomerId,
            'RequestId' => $requestId,
            'RetryIndicator' => 0,
        ),
            'Token' => $token
    );
    $responseAssert = do_curl($apiUser, $apiPwd, $saferPayServer.'/api/Payment/v1/PaymentPage/Assert', $payload);
    // Assert should return transactionId if ok
    if ($responseAssert['status'] == 200) {
        $payload = array(
            'RequestHeader' => array(
                'SpecVersion' => "1.7",
                'CustomerId' => $saferPayCustomerId,
                'RequestId' => $requestId,
                'RetryIndicator' => 0,
            ),
            'TransactionReference' => array(
                'TransactionId' => $responseAssert['body']['Transaction']['Id'],
            
            )
        );
        // send Capture payment 
        $response = do_curl($apiUser, $apiPwd, $saferPayServer.'/api/Payment/v1/Transaction/Capture', $payload);
        if (($response['status'] == 200) || ($response['status'] == 402)){
            // remove token from list when captured or already captured
            $glion_pay->clearTransaction($requestId);
        }
    } else if ($responseAssert['status'] == 402) {
        // remove token if payment canceled
        $glion_pay->clearTransaction($requestId);
    }
}


?>