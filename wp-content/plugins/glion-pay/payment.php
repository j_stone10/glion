<?php
// load wordpress
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

// Build Saferpay call
global $glion_pay;
$options = $glion_pay->get_options();
$apiUser = $options['api_user'];
$apiPwd = $options['api_pwd'];
$saferPayCustomerId = $options['customer_id'];
$terminalId = $options['terminal_id'];
$currencyCode = $options['currency_code'];
$successUrl = $options['success_url'];
$failUrl = $options['fail_url'];
$merchantEmail = $options['merchant_email'];
$saferPayServer = $options['saferpay_server'];
// Form inputs

function clean_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = str_replace(
        array('à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ',':',';','.',',',"'",'"','/','\\','  ',' ','!','?'),
        array('a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','','','','',' ',' ','','',' ','-','',''),
        htmlspecialchars($data));
    $data = preg_replace("#[^0-9a-zA-Z\-]#", "", $data);
    return $data;
}
$studentId = clean_input($_POST["studentid"]);
$firstname = clean_input($_POST["firstname"]);
$lastname = clean_input($_POST["lastname"]);
$orderId = strtoupper($studentId.'-'.$firstname.'-'.$lastname);
$amount = abs(floatval(htmlspecialchars(trim($_POST["amount"])))*100);

$payload = array(
    'RequestHeader' => array(
        'SpecVersion' => "1.7",
        'CustomerId' => $saferPayCustomerId,
        'RequestId' => $studentId.'-'.time(),
        'RetryIndicator' => 0,
        'ClientInfo' => array(
            'ShopInfo' => "Glion.edu",
        )
    ),
    'TerminalId' => $terminalId,
    'Payment' => array(
        'Amount' => array(
            'Value' => $amount,
            'CurrencyCode' => $currencyCode
        ),
        'OrderId' => $orderId,
        'PayerNote' => "Glion invoice",
        'Description' => $orderId
    ),
    'Payer' => array(
        'LanguageCode' => "en"
    ),
    'ReturnUrls' => array(
        'Success' => $successUrl,
        'Fail' => $failUrl
    ),
    'Notification' => array(
        'MerchantEmail' => $merchantEmail,
        'NotifyUrl' => get_site_url() . "/wp-content/plugins/glion-pay/notifyme.php"
    ),
    'DeliveryAddressForm' => array(
        'Display' => true,
        'MandatoryFields' => array("CITY","COUNTRY","EMAIL","FIRSTNAME","LASTNAME","PHONE","SALUTATION","STATE","STREET","ZIP")
    )
);

//$username and $password for the http-Basic Authentication
//$url is the SaferpayURL eg. https://www.saferpay.com/api/Payment/v1/PaymentPage/Initialize
//$payload is a multidimensional array, that assembles the JSON structure. Example see above
function do_curl($username,$password,$url, $payload){
    //Set Options for CURL
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_VERBOSE, true);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //Return Response to Application
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //Set Content-Headers to JSON
    curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Accept: application/json; charset=utf-8"));
    //Execute call via http-POST
    curl_setopt($curl, CURLOPT_POST, true);
    //Set POST-Body
    //convert DATA-Array into a JSON-Object
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
    //WARNING!!!!!
    //This option should NOT be "false"
    //Otherwise the connection is not secured
    //You can turn it of if you're working on the test-system with no vital data
    //PLEASE NOTE:
    //Under Windows (using WAMP or XAMP) it is necessary to manually download and save the necessary SSL-Root certificates!
    //To do so, please visit: http://curl.haxx.se/docs/caextract.html and Download the .pem-file
    //Then save it to a folder where PHP has write privileges (e.g. the WAMP/XAMP-Folder itself)
    //and then put the following line into your php.ini:
    //curl.cainfo=c:\path\to\file\cacert.pem
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
    //HTTP-Basic Authentication for the Saferpay JSON-API.
    //This will set the authentication header and encode the password & username in Base64 for you
    curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
    //CURL-Execute & catch response
    $jsonResponse = curl_exec($curl);
    //Get HTTP-Status
    //Abort if Status != 200
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ( $status != 200 ) {
        //IF ERROR
        //Get http-Body (if aplicable) from CURL-response
        $body = json_decode(curl_multi_getcontent($curl),true);
        //Build array, containing the body (Response data, like Error-messages etc.) and the http-status-code
        $response = array(
            "status" => $status,
            "body" => $body
        );
    }else{
        //IF OK
        //Convert response into an Array
        $body = json_decode($jsonResponse, true);
        //Build array, containing the body (Response-data) and the http-status-code
        $response = array(
            "status" => $status,
            "body" => $body
        );
    }
    //IMPORTANT!!!
    //Close connection!
    curl_close($curl);
    //$response, again, is a multi-dimensional Array, containing the status-code ($response["status"]) and the API-response (if available) itself ($response["body"])
    return $response;
}
$response = do_curl($apiUser, $apiPwd, $saferPayServer.'/api/Payment/v1/PaymentPage/Initialize', $payload);
if ($response['status'] == 200) {
    // add token to pending transaction
    $glion_pay->addTransaction($response['body']["ResponseHeader"]["RequestId"], $response["body"]["Token"]);
    header('Location:'.$response["body"]["RedirectUrl"]);
} else {
    echo "<h1>Saferpay Error:</h1>";
    error_log(print_r($response['body'], true));
    ?>
    <table border="1" cellpadding="5" cellspacing="0">
        <tbody>
        <?php foreach($response['body'] as $key => $value): ?>
            <tr>
                <td><?php echo $key; ?></td><td><?php print_r($value); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php
}
?>