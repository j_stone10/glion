<?php
class glion_pay_admin {

    function __construct() {
        // stuff to do when the plugin is loaded
        add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
    }

    function admin_menu() {
        add_options_page( 'Glion Pay Settings', 'Glion Pay Settings', 'manage_options', __FILE__, array( &$this, 'settings_page' ) );
    }// end function

    function settings_page() {

        global $glion_pay;
        $options = $glion_pay->get_options();

        if ( isset($_POST['update']) ) {

            // check user is authorised
            if ( function_exists( 'current_user_can' ) && !current_user_can( 'manage_options' ) ) {
                die( 'Sorry, not allowed...' );
            }
            check_admin_referer( 'glion_pay_settings' );
            $options['api_user'] = trim( $_POST['api_user'] );
            $options['api_pwd'] = trim( $_POST['api_pwd'] );
            $options['customer_id'] = trim( $_POST['customer_id'] );
            $options['terminal_id'] = trim( $_POST['terminal_id'] );
            $options['currency_code'] = trim( $_POST['currency_code'] );
            $options['success_url'] = trim( $_POST['success_url'] );
            $options['fail_url'] = trim( $_POST['fail_url'] );
            $options['merchant_email'] = trim( $_POST['merchant_email'] );
            $options['saferpay_server'] = trim( $_POST['saferpay_server'] );

            update_option( 'glion_pay', $options );

            echo '<div id="message" class="updated fade"><p><strong>Settings saved.</strong></p></div>';

        }// end if

        echo '<div class="wrap">'
            .'<h2>Glion Pay Settings Settings</h2>'
            .'<form method="post">';
        if ( function_exists( 'wp_nonce_field' ) ) wp_nonce_field( 'glion_pay_settings' );
        echo '<h3>Saferpay settings</h3>'
            .'<table class="form-table" style="width:auto">'
            .'<tr>'
            .'<td>Saferpay API User:&nbsp;</td><td><input name="api_user" id="api_user" style="width:200px;" value="'.$options['api_user'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Saferpay APU Password:&nbsp;</td><td><input name="api_pwd" id="api_pwd" style="width:200px;" value="'.$options['api_pwd'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Saferpay Server:&nbsp;</td><td><input name="saferpay_server" id="saferpay_server" style="width:200px;" value="'.$options['saferpay_server'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Saferpay Customer Id:&nbsp;</td><td><input name="customer_id" id="customer_id" style="width:200px;" value="'.$options['customer_id'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Merchant Email:&nbsp;</td><td><input type="email" name="merchant_email" id="merchant_email" style="width:200px;" value="'.$options['merchant_email'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Terminal Id:&nbsp;</td><td><input name="terminal_id" id="terminal_id" style="width:200px;" value="'.$options['terminal_id'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Currency code:&nbsp;</td><td><input name="currency_code" id="currency_code" style="width:200px;" value="'.$options['currency_code'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Success URL&nbsp;</td><td><input name="success_url" id="success_url" style="width:200px;" value="'.$options['success_url'].'"></td>'
            .'</tr>'
            .'<tr>'
            .'<td>Cancel/Error URL:&nbsp;</td><td><input name="fail_url" id="fail_url" style="width:200px;" value="'.$options['fail_url'].'"></td>'
            .'</tr>'
            .'</table>'
            .'<p class="submit"><input type="submit" name="update" class="button-primary" value="Save Changes" /></p>'
            .'</form>'
            .'</div>';

    }// end function

}// end class
$glion_pay_admin = new glion_pay_admin;
