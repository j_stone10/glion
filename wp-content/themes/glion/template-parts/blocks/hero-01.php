<?php
/**
 * Block Name: Hero-01
 */
?>
<div class="block-hero-01">
	<div class="row hero">
		<div class="background_image">
			<img src="<?php the_field('hero_image'); ?>" />
		</div>
		<div class="scrollElementStart foreground_text col-md-9 col-11 m-auto">
		<h1 class="mb-3"><?php the_field('hero_title'); ?>
			</h1>
			<p><?php the_field('hero_text'); ?></p>
		</div>
	</div>
</div>