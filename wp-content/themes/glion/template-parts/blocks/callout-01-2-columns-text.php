<?php
/**
 * Block Name: Callout-01 2 Columns Text
 */
?>
<div class="block-callout-01-2-columns-text container-fluid body-copy">
	<div class="wrapper row">
		<div class="scrollElement container my-5">
			<div class="row">
				<div class="col-12 col-md-6">
					<?php the_field('text_1'); ?>
				</div>
				<div class="col-12 col-md-6">
					<?php the_field('text_2'); ?>
				</div>
			</div>
		</div>
	</div>
</div>