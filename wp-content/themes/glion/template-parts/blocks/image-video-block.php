
   <div id="<?php if ( get_field('image_video_anchor_id') ) { echo the_field('image_video_anchor_id'); } ?>" class="image-video-block row line-after <?php the_field('image_video_content_bg_colour') ?> <?php if ( get_field('image_video_hide_mobile')) { echo 'd-none d-md-flex '; } ?> <?php if ( get_field('image_video_hide_desktop__tablet')) { echo 'd-flex d-md-none '; } ?>">
      <div class="col-md-6 col-12  p-0  <?php if ( get_field('media_side') == 'right' ) { echo "order-2"; } ?>">
         <?php if ( get_field('media_type') == 'video' ) : ?>
         <div class="video_wrapper">
            <a data-toggle="modal" data-target="#content1">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo get_field('image_video_content_image')['url']; ?>" alt="<?php echo get_field('image_video_content_image')['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
         <?php else : ?>
         <img  style="min-width: 100%"  src="<?php echo get_field('image_video_content_image')['url']; ?>" alt="<?php echo get_field('image_video_content_image')['alt']; ?>" />
         <?php endif; ?>
      </div>
      <div class="wrapper col-md-6 col-12 pr-md-0 d-flex ind <?php if ( get_field('media_side') == 'right' ) { echo "order-1"; } ?>">
         <div class="row program-text individual mr-md-0">
            <div class="scrollElement col-xl-8 col-lg-11 col-11 m-auto p-md-3 pt-md-5">
               <?php the_field('image_video_content'); ?>
               <?php if (get_field('image_video_content_link')) : ?> 
               <div class="row">
                  <div class="d-inline pl-3">
                     <a class="mt-5  btn btn-primary" href="<?php echo get_field('image_video_content_link')['url'] ?>"><?php  echo get_field('image_video_content_link')['title'] ?></a>
                  </div>
               </div>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
   <?php if ( get_field('media_type') == 'video' ) : ?>
<div class="modal fade" id="content1" tabindex="-1" role="dialog" aria-labelledby="content1" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php  the_field('image_video_content_video') ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
   <?php endif; ?>