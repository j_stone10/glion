<?php
/**
 * Block Name: Callout-05 CTA
 */
?>
<div class="block-callout-05-CTA container-fluid py-5 open-day-cta">
	<div class="wrapper row">
		<div class="scrollElement col pt-4 pb-5 text-center">
			<h5><?php the_field('title'); ?></h5>
			<a href="<?php the_field('link'); ?>"><?php the_field('button'); ?></a>						
		</div>
	</div>
</div>