<?php
/**
 * Block Name: Tabarea-01 Programs
 */
?>
<div class="block-tabarea-01-programs <?php if( is_page( 35124 ) ) {echo "remote-learning-page";} ?>">
    <div class="wrapper row">
        <div class="scrollElement col-md-6 col-12 pr-md-0 program-content">
            <div class="row mr-md-0">
                <div class="col-12 program-title">
                    <h3><?php the_field('program_header', 23) ?></h3>
                    <div class="program_study_text">
                        <svg width="28" height="18" viewBox="0 0 28 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M25.042 1.15227C25.038 0.510246 24.544 -0.00422567 23.9376 2.61653e-05L14.0461 0.0361668L4.15451 2.61653e-05C3.54808 -0.00422567 3.05411 0.512372 3.05009 1.15227L3.04407 2.03028V14.9303L14.0461 14.9027L25.0481 14.9303L25.042 1.15227ZM23.7087 13.4932L14.0461 13.5187L4.38342 13.4932L4.36535 1.65824C4.36535 1.54344 4.4537 1.45203 4.56013 1.45203H23.532"
                                fill="#F0D4C6"></path>
                            <path
                                d="M27.8092 15.9504L17.6385 15.9122L17.3494 16.4139H10.6245L10.3634 15.8867L0.194779 15.8484C0.0863453 15.8484 0 15.9398 0 16.0546V16.6966C0 16.7795 0.0461847 16.8539 0.116466 16.8858L1.55622 17.5513C1.93574 17.7256 2.34337 17.817 2.75702 17.817H25.247C25.6606 17.8191 26.0542 17.8149 26.4337 17.6427L27.8775 16.9879C27.9498 16.956 27.9959 16.8816 27.9959 16.7987L28 16.1566C28.002 16.044 27.9156 15.9504 27.8092 15.9504Z"
                                fill="#F0D4C6"></path>
                            <path
                                d="M15.3172 8.56258C16.3232 8.04386 17.018 6.95114 17.018 5.68622C17.018 3.92171 15.6666 2.49097 13.9999 2.49097C12.3333 2.49097 10.9819 3.92171 10.9819 5.68622C10.9819 6.95114 11.6766 8.04386 12.6826 8.56258C10.6365 9.1706 9.13647 11.1605 9.13647 13.5202H18.8634C18.8634 11.1583 17.3634 9.1706 15.3172 8.56258Z"
                                fill="#F0D4C6"></path>
                        </svg>
                        <?php the_field('programs_header_remote_study_text', 23) ?>
                    </div>
                </div>
            </div>
            <div class="row mr-md-0">
                <div class="col-12 p-0">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li class="nav-item darken active m-0" href="#bachelors_this"
                            data-target=".bachelors_this, .bachelors_that" data-toggle="tab">
                            <a class="nav-link text-center nav-item">
                                <?php the_field('program_1_tab_title', 23) ?>
                            </a>
                        </li>
                        <li class="nav-item darken m-0" href="#masters_this" data-target=".masters_this, .masters_that"
                            data-toggle="tab">
                            <a class="nav-link text-center nav-item">
                                <?php the_field('program_2_tab_title', 23) ?></a>
                        </li>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="row program-text mr-md-0">
                <div class="col-xl-9 col-lg-11 col-11 m-auto pl-md-3 pl-lg-0">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade show bachelors_this">
                            <h3> <?php the_field('program_1_title', 23) ?>
                            </h3>
                            <p> <?php the_field('program_1_details', 23) ?></p>
                            <p class="remote_learning_text batchelors">
                                <strong>
                                    <?php the_field('remote_learning_overview_text', 4169) ?>
                                </strong>
                            </p>
                            <a class="mt-5 btn btn-primary bronze"
                                href="<?php echo get_field('program_1_link', 23)['url'] ?>"
                                role="button"><?php echo get_field('program_1_link', 23)['title'] ?></a>
                        </div>
                        <div class="tab-pane fade hide masters_this">
                            <h3><?php the_field('program_2_title', 23) ?></h3>
                            <p><?php the_field('program_2_details', 23) ?></p>
                            <?php if (get_field('program_2_link', 23)) : ?><a class="mt-5 btn btn-primary bronze"
                                href="<?php echo get_field('program_2_link', 23)['url'] ?>"
                                role="button"><?php echo get_field('program_2_link', 23)['title'] ?></a><?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12  p-0">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade show bachelors_that">
                    <img class="img-fluid" src="<?php echo get_field('program_1_image', 23)['url']; ?>"
                        alt="<?php echo get_field('program_1_image', 23)['alt']; ?>" />
                </div>
                <div class="tab-pane fade hide  masters_that">
                    <img class="img-fluid" src="<?php echo get_field('program_1_image', 23)['url']; ?>"
                        alt="<?php echo get_field('program_1_image', 23)['alt']; ?>" />
                    <div class="row p-0 m-0">
                        <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                            <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                                <h6 class="masters-int-title accordion_title accordion_title_black_text coloured">
                                    <?php the_field('program_2_block_1_title', 23) ?></h6>
                                <div class="expand-icon d-block d-lg-none"> <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" />
                                </div>
                                <div class="white-bg masters-int-text accordion_text  pb-5 p-md-0">
                                    <p><?php the_field('program_2_block_1_text', 23) ?></p>
                                    <p class="remote_learning_text masters">
                                        <?php the_field('remote_learning_overview_text', 1748) ?></p>
                                    <a class="learn_more"
                                        href="<?php echo get_field('testing_new_link_11', 23)['url'] ?>"><?php  echo get_field('testing_new_link_11', 23)['title'] ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                            <div class="col-xl-9 col-lg-11  col-12 m-auto px-0 py-md-3">
                                <h6 class=" business-masters-title accordion_title accordion_title_white_text coloured">
                                    <?php the_field('program_2_block_2_title', 23) ?></h6>
                                <div class="expand-icon d-block d-lg-none"> <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" />
                                </div>
                                <div class="white-bg business-masters-text  accordion_text  pb-5 p-md-0">
                                    <p><?php the_field('program_2_block_2_text', 23) ?></p>
                                    <p class="remote_learning_text masters white">
                                        <?php the_field('remote_learning_overview_text', 1758) ?></p>
                                    <a class="learn_more white"
                                        href="<?php echo get_field('testing_new_link_22', 23)['url'] ?>"><?php  echo get_field('testing_new_link_22', 23)['title'] ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row p-0 m-0">
                        <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                            <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                                <h6 class=" masters-hosp-title accordion_title accordion_title_black_text coloured">
                                    <?php the_field('program_2_block_3_title', 23) ?></h6>
                                <div class="expand-icon d-block d-lg-none"> <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" />
                                </div>
                                <div class="white-bg masters-hosp-text  accordion_text  pb-5 p-md-0">
                                    <p><?php the_field('program_2_block_3_text', 23) ?></p>
                                    <p class="remote_learning_text masters">
                                        <?php the_field('remote_learning_overview_text', 1769) ?></p>
                                    <a class="learn_more"
                                        href="<?php echo get_field('testing_new_link_33', 23)['url'] ?>"><?php  echo get_field('testing_new_link_33', 23)['title'] ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                            <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                                <h6 class=" masters-lux-title accordion_title accordion_title_white_text coloured">
                                    <?php the_field('program_2_block_4_title', 23) ?></h6>
                                <div class="expand-icon d-block d-lg-none"> <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" />
                                </div>
                                <div class="white-bg masters-lux-text  accordion_text  pb-5 p-md-0">
                                    <p><?php the_field('program_2_block_4_text', 23) ?></p>
                                    <p class="remote_learning_text masters white">
                                        <?php the_field('remote_learning_overview_text', 1777) ?></p>
                                    <a class="learn_more white"
                                        href="<?php echo get_field('testing_new_link_44', 23)['url'] ?>"><?php  echo get_field('testing_new_link_44', 23)['title'] ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>