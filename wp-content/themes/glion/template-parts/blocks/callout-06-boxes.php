<?php
/**
 * Block Name: Callout-06 Boxes
 */
?>
<div class="block-callout-06-boxes container py-5">
    <div class="wrapper row">
    	<div class="scrollElement col-10 mx-auto mb-3 mt-md-4 text-center">
    		<h5><?php the_field('title'); ?></h5>					
		</div>
	</div>	
	<div class="wrapper row">
		<div class="scrollElement col-12 col-md-6 mt-4 text-center">
            <div class="module-boxes">
				<h5><?php the_field('box1_title'); ?></h5>
				<a href="<?php the_field('box1_link'); ?>"><?php the_field('box1_button'); ?></a>	
			</div>					
		</div>
		<div class="scrollElement col-12 col-md-6 mt-4 text-center">
			<div class="module-boxes">
				<h5><?php the_field('box2_title'); ?></h5>
				<a href="<?php the_field('box2_link'); ?>"><?php the_field('box2_button'); ?></a>	
			</div>						
		</div>
	</div>
</div>