<?php
/**
 * Block Name: Quote-01 
 */
?>
<div class="block-quote-01 ">
	<div class="wrapper row testimonial shield mt-5 mb-5 justify-content-center align-items-center <?php the_field('background_colour_quote')?> ">
      <div class="scrollElement col-md-7 col-9 p-0 p-md-5 m-auto text-center">
         <h2><?php the_field('quote_text') ?>
         </h2>
      </div>
   </div>
</div>