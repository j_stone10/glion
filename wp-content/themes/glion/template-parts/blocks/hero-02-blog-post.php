<?php
   /**
    * Block Name: Hero-02-Blog-Post-With-Share
    */
   ?>
<div class="block-hero-01">
<div class="row hero">
   <div class="background_image d-none d-md-block">
      <img src="<?php echo get_field('hero_image_blog')['url']; ?>" alt="<?php echo get_field('hero_image_blog')['alt']; ?>" />
   </div>
   <div class="background_image_featured d-block d-md-none">
   <?php the_post_thumbnail(); ?>
   </div>
</div>
<div class="row share mb-3">
   <div class="container">
      <div class="row my-1">
         <div class="col" style="max-width:75px">
            <img class="social_share_toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/share.svg" />
         </div>
         <div class="col social_view pt-1">
            <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo "<a   class='social-share wattsap' data-toggle='tooltip; data-placement='top' title='Share on Wattsap' target='_blank' href='https://api.whatsapp.com/send?text=" . urlencode($actual_link) . "' data-action='share/whatsapp/share'>"; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/email.svg" /></a>
            <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" class='social-share facebook' href="#" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/messenger.svg" /></a>
            <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" class='social-share copy'  href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/whatson.svg" /></a>
            <a data-toggle="tooltip" data-placement="top"  class='social-share email' title="Share via Email" href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mail.svg" /></a>   
         </div>
      </div>
   </div>
</div>