<?php
   /**
    * Block Name: Blog Content
    */
   ?>
<div class="block-blog-content">
    <div class="container">
        <?php the_field('blog_content'); ?>
    </div>
</div>