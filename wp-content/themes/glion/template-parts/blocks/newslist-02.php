<?php
/**
 * Block Name: Newslist-02
 */
?>
<div id="newsList" class="block-newslist-02 container-fluid bg-ugly-blue position-relative">
	<div class="wrapper row">
		<div class="scrollElement container my-5">
			<div class="row">
				<div class="col mb-5 life-at-les-roches text-center">
					<h5 class="mb-0"><?php the_field('title'); ?></h5>
				</div>
			</div>
			<div class="row mt-2 mt-md-4 card-columns">
				
				<?php 
					// the query
					$the_query = new WP_Query( array(
						'category_name' => get_field('category'),
                        // 'category__not_in' => '900' ,
						'posts_per_page' => 4,
					)); 
                ?>
				<div class="col">
					<div class="card-deck home-posts">													
						<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div class="card mb-4">
								<div class="home-post-thumbnail">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
								<div class="card-body">
									<div class="home-post-category d-none d-md-block">
										<?php the_category(); ?>
									</div>
									<div class="home-post-title mb-5 mb-md-3">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
									<div class="home-post-excerpt d-none d-md-block">
										<?php the_excerpt(); ?>
									</div>
									<div class="home-post-cta">
										<a href="<?php the_permalink(); ?>"></a>													
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
						<?php else : ?>
						<p><?php __('No News'); ?></p>
						<?php endif; ?>
					</div>
				</div>				
			</div>
			<div class="row">
				<div class="col mt-4 mt-md-0 mt-lg-5 life-at-les-roches-cta text-center">
					<a href="<?php the_field('link'); ?>"><?php the_field('link_text'); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>