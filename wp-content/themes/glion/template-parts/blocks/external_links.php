<?php
   /**
    * Block Name: External Links Custom Block
    */

   ?>
<div id="externalLinks" class="ext-link-modal-popop">
   <div class="wrapper row py-5 <?php the_field('external_background_colour')?>">
      <div class=" container ">
         <div class="row">
            <div class="col-12">
               <h2 class="mb-3"><?php the_field('title_of_external_link_block') ?></h2>
            </div>
         </div>
        
         <div id="videosModals" class="row">
            <?php
               if( have_rows('external_links') ): 
               
               $total = count(get_field('external_links'));
                  // echo $total;
               $count = 0;
               // echo $count;
               $number = 8;					
               while ( have_rows('external_links') ) : the_row(); ?>						
            <div class="col-12 col-md-3 order-1 order-md-2 home-about mb-2 card-deck home-posts">
               <a type="button" class="modal-hover" target="_blank" href="<?php  the_sub_field('external_link') ?>">
                  <div class="card mb-2 video-container">              
                     <div class="card-body details">               
                        <div class="publication-text"><?php  the_sub_field('publication') ?></div>
                        <div class="title-text"><?php  the_sub_field('title') ?></div>
                        <div class="short-text"><?php  the_sub_field('date') ?></div>
                     </div>
                  </div>
               </a>   
            </div>   
               <?php endwhile; endif; ?>     
         </div>
      </div>
   </div>
</div>
