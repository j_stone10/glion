<?php
   /**
    * Block Name: Accordian-01
    */
   ?>
<div class="block-accordian-01">

   <div class=" row">
   <?php if( have_rows('accordion_row') ): $accordion++; ?>
      <?php while ( have_rows('accordion_row') ) : the_row(); ?>
      <div class=" col-12 p-0 accordion">
         <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
         <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
         <div class="accordion_text">
            <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
            <div class="row ">
               <div class="col-12">
                  <?php the_sub_field('content_left'); ?>
               </div>
            </div>
            <?php else : ?>
            <div class="row">
               <div class="col-12 col-md-6">
                  <?php the_sub_field('content_left'); ?>
               </div>
               <div class="col-12 col-md-6">
                  <?php the_sub_field('content_right'); ?>
               </div>
            </div>
            <?php endif; ?>
         </div>
      </div>
      <?php endwhile; ?>
   <?php endif; ?>
   </div>     

</div>