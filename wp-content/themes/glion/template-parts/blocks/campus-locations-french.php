<?php
   /**
    * Block Name: Campus Locations
    */
   ?>
<div class="block-campuslocations">
   <div class="wrapper row my-5">
      <div id="campus-carousel" class="scrollElement carousel slide mb-5 col-12 p-0" data-ride="carousel" data-interval="false">
         <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <div class="col-12 col-md-6 campus-rollover p-0 px-md-3 active <?php if ( get_field('show_campuses') == 'Switzerland') { echo "col-md-12 px-0"; } elseif ( get_field('show_campuses') == 'London') { echo "d-none"; } else { echo "px-md-3"; }  ?>">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <?php if ( get_field('show_campuses') == 'Switzerland') : ?><img class="img-fluid"  src="<?php echo get_field('campus_1_image_wide', 4549)['url']; ?>" alt="<?php echo get_field('campus_1_image_wide', 4549)['alt']; ?>" /><?php else : ?><img class="img-fluid"  src="<?php echo get_field('campus_1_image', 4549)['url']; ?>" alt="<?php echo get_field('campus_1_image', 4549)['alt']; ?>" /><?php endif; ?>
                  </div>
                  <div class="absolute col-9 m-auto text-center">
                     <h2><?php the_field('campus_1_title', 4549) ?></h2>
                     <p><?php the_field('campus_1_location', 4549) ?></p>
                  </div>
                  <button class="btn btn-primary blue" ><?php  echo get_field('campus_1_link_homee', 4549)['title'] ?></button>
                  <a href="<?php echo get_field('campus_1_link_homee', 4549)['url'] ?>"></a>
               </div>
            </div>
            <div class="col-12 col-md-6 campus-rollover p-0 px-md-3 <?php if ( get_field('show_campuses') == 'London') { echo "col-md-12 d-block px-0"; } elseif ( get_field('show_campuses') == 'Switzerland') { echo "d-none col-md-12"; } else { echo "px-md-3"; }   ?>">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <?php if ( get_field('show_campuses') == 'London') : ?><img class="img-fluid"  src="<?php echo get_field('campus_2_image_wide', 4549)['url']; ?>" alt="<?php echo get_field('campus_2_image_wide', 4549)['alt']; ?>" /><?php else : ?><img class="img-fluid"  src="<?php echo get_field('campus_2_image', 4549)['url']; ?>" alt="<?php echo get_field('campus_2_image', 4549)['alt']; ?>" /><?php endif; ?>
                  </div>
                  <div class="absolute col-9 m-auto  text-center">
                     <h2><?php the_field('campus_2_title', 4549) ?></h2>
                     <p><?php the_field('campus_2_location', 4549) ?></p>
                  </div>
                  <button class="btn btn-primary blue" ><?php  echo get_field('campus_2_link_homee', 4549)['title'] ?></button>
                  <a href="<?php echo get_field('campus_2_link_homee', 4549)['url'] ?>"></a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>