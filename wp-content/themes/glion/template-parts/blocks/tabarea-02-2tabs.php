<?php
   /**
    * Block Name: Tabarea-02 2 Tabs
    */
   ?>
<div class="block-tabarea-02-2tabs py-5">
   <div class="wrapper row">
      <div class="scrollElement col">
         <h3 class="text-center mb-3"><?php the_field('title'); ?></h3>
      </div>
   </div>
   <div class="wrapper">
      <div class=" row line-after">
         <ul class=" nav nav-tabs m-auto home-programmes two-tabs d-flex" role="tablist">
            <li class="nav-item col-6 ml-auto">
               <button class="mt-5  btn btn-primary active mr-5" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab"><?php the_field('tab_1'); ?></button>
               <!-- <a class="nav-link active" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab"><?php  //the_field('tab_1'); ?></a> -->
            </li>
            <li class="nav-item col mr-auto">
               <button class="mt-5 btn btn-primary" href="#tabarea-02-2tabs-tab2" role="tab" data-toggle="tab"><?php the_field('tab_2'); ?></button>
            </li>
         </ul>
      </div>
      </div>
   <div class="row">
      <div class="tab-content module-tabs-content">
         <!-- TAB 1 -->
         <div role="tabpanel" class="tab-pane flex-tabs w-100 py-5 active" id="tabarea-02-2tabs-tab1">
            <div class="wrapper row">
               <div class=" col-6 p-1 p-md-5 <?php the_field('tab_1_column_1_colour');  if ( get_field('tab_1_number_columns') == 'one column') { echo " col-12"; } ?> ">
                  <div class="scrollElement col d-block text-center">
                     <?php the_field('tab_1_content'); ?>
                  </div>
               </div>
               <div class=" col-6 p-1 p-md-5 <?php the_field('tab_1_column_2_colour'); if ( get_field('tab_1_number_columns') == 'one column') { echo " d-none"; } ?> ">
                  <div class="scrollElement col d-block text-center">
                     <?php the_field('tab_1_content_2'); ?>
                  </div>
               </div>
            </div>
         </div>
         <!-- TAB 2 -->
         <div role="tabpanel" class="tab-pane flex-tabs w-100 py-5" id="tabarea-02-2tabs-tab2">
            <div class="wrapper row">
               <div class=" col-6 p-1 p-md-5  <?php the_field('tab_2_column_1_colour'); if ( get_field('tab_2_number_columns') == 'one column') { echo " col-12"; } ?> ">
                  <div class="scrollElement col d-block text-center">
                     <?php the_field('tab_2_content'); ?>
                  </div>
               </div>
               <div class=" col-6 p-1 p-md-5 b <?php the_field('tab_2_column_2_colour'); if ( get_field('tab_2_number_columns') == 'one column') { echo " d-none"; } ?> ">
                  <div class="scrollElement col d-block text-center">
                     <?php the_field('tab_2_content_2'); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>