<?php
   /**
    * Block Name: Video Custom Block
    */

   ?>
<div class="video-modal-popop ">
   <div class="wrapper row py-5 <?php the_field('background_colour_video'); ?>">
      <div class=" container ">
         <div class="row">
            <div class="col-12">
               <h2 class="mb-5"><?php the_field('title_of_main_video_gallery') ?></h2>
            </div>
         </div>
       
         <div id="videosModals" class="row">
            <?php
               if( have_rows('video_popup') ): 
               
               $total = count(get_field('video_popup'));
                  // echo $total;
               $count = 0;
               // echo $count;
               $number = 39;					
               while ( have_rows('video_popup') ) : the_row(); ?>						
            <div class="col-12 col-md-4 home-about mb-5 card-deck home-posts <?php if ( $count >= 9 ) {  echo "d-none"; } ?>">
               <a type="button" class="modal-hover" data-toggle="modal" data-target="#vidPopup<?php echo $count ?>">
                  <div class="card mb-2 video-container">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="video-image home-post-thumbnail">
                              <div class="playBTN"></div>
                              <?php $image = get_sub_field('placeholder_graphic'); ?>
                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                           </div>
                        </div>
                        <div class="col-sm-12  p-0">
                           <div class="card-body details">
                              <div class="title-text"><?php  the_sub_field('title') ?></div>
                              <div class="sub-title d-none d-sm-block"><?php  the_sub_field('sub_title') ?></div>
                              <div class="short-text d-none d-sm-block"><?php  the_sub_field('short_description') ?></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </a>
               <!-- Modal -->
               <div class="modal fade" data-target="#myModal" id="vidPopup<?php echo $count ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="marketo-line"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="p-4 p-md-3">
                           <div class="video-body p-2">
                              <iframe class="video_id" width="650" height="315" src="<?php  the_sub_field('video_link') ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           </div>
                           <div class="video-content p-2">
                            
                              <h5 class="modal-title"><?php  the_sub_field('title') ?></h5>
                            
                              <?php if ( get_sub_field('presenter') ) : ?> 
                                 <p class="presenter"><strong>Presenter:</strong> <?php  the_sub_field('presenter') ?></p>
                              <?php endif; ?>  
                              <?php if ( get_sub_field('moderator') ) : ?> 
                              <p class="moderator"><strong>Moderator:</strong> <?php  the_sub_field('moderator') ?></p>
                              <?php endif; ?>  
                              
                              <p class="full-description"><?php  the_sub_field('full_description') ?></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               if ($count == $number) {
               	// we've shown the number, break out of loop
               	break;
               } ?>					
            <?php $count++; endwhile;
               else : endif;
               ?>
            <?php // echo do_shortcode("[ff id='1']") ?>
            <?php // curator_feed( '3c2003b5-6346-4dc6-8586-9546ae4aee41' ); ?>
         </div>
         <div class="learn_more_container m-auto  text-center">
                <a id="gallery-load-more" href="#" style="display: none;">Load more</a>
         </div>
      </div>
   </div>
</div>
<script>
 $(function() {
   var count = 0 ;
   $( "#videosModals .col-12" ).each(function() {
   	
   	
   		$( this ).addClass('class' + count);
   
   		count++;
   		console.log(count)
   });
   
   
   var counter = 9;

   if ( $( "#videosModals .col-12.d-none" ).length > 0 ) {
      $('#gallery-load-more').show();
   }
   
   $('#gallery-load-more').on('click', function(event){
      event.preventDefault();
   	counter = counter + 9;
   	console.log(counter);
   
   	// console.log(count - 3)
   	remainingPosts = ( $( "#videosModals .col-12.d-none" ).length - 2)
   	console.log(remainingPosts)
   	for (i = 0; i < counter; i++) {
   	$( "#videosModals .col-12.d-none" ).each(function() {
   	
   		$(  '.class' + i ).removeClass('d-none');
   		// $( '.class3').removeClass( "d-none" );
   		// $( '.class4').removeClass( "d-none" );
   	
   		
   	});
   	if (remainingPosts < 3) {
   		$('#gallery-load-more').hide();
   	} 
   
   }
  
   })

   

   $('.modal-content button').on('click', function(e) {
      var video = $(this).siblings().find('iframe').attr("src");
      var videoId = $(this).siblings().find('iframe');
      console.log(video)
      $(videoId).attr("src","");
      $(videoId).attr("src",video);
    });
   })

   	
</script>