<?php
   /**
    * Block Name: Editorial-Content-01 
    */
   ?>
<div id="<?php if ( get_field('div_id') ) { echo the_field('div_id'); } ?>" class="block-editorial-content-01 <?php if ( get_field('editorial_hide_mobile')) { echo 'd-none d-md-block '; } ?> <?php if ( get_field('editorial_hide_desktop')) { echo 'd-block d-md-none '; } ?>">
   <?php if ( get_field('number_columns') == 'Single column') : ?>
   <div class="wrapper row">
      <div class=" col-12 <?php the_field('background_colour')?> ">
         <div class="row" style="margin: <?php if ( get_field('inner_margin') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin') == '300px') { echo "18.8rem"; } ?> 0">
            <div class="scrollElement col-12 m-auto <?php if ( get_field('inner_width') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width') == '50%') { echo "col-md-7"; } else  if ( get_field('inner_width') == '50%') { echo "col-md-7"; } else if ( get_field('inner_width') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width') == '100%') { echo "col-md-12"; } ?> ">
               <?php the_field('content1'); ?>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
   <?php if( have_rows('single_column_accordion_row') ) :  ?>
   <?php while ( have_rows('single_column_accordion_row') ) : the_row(); ?>
      <div class="col-12 p-0 accordion m-auto <?php if ( get_field('inner_width') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width') == '50%') { echo "col-md-7"; } else if ( get_field('inner_width') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width') == '100%') { echo "col-md-12"; } ?> ">
         
         <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
         <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
         <div class="accordion_text">
            <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
            <div class="row ">
            <div class="col-12 m-auto  ">
                  <?php the_sub_field('content_left'); ?>
               </div>
            </div>
            <?php else : ?>
            <div class="row">
               <div class="col-12 col-md-6">
                  <?php the_sub_field('content_left'); ?>
               </div>
               <div class="col-12 col-md-6">
                  <?php the_sub_field('content_right'); ?>
               </div>
            </div>
            <?php endif; ?>
         </div>
      </div>
      <?php endwhile; ?>
   <?php endif; ?>
   </div>

   <?php elseif ( get_field('number_columns') == 'Two columns') : ?>
   <div class="row">
      <div class="col-md-6 col-12 <?php the_field('column_1_background_colour2')?>"">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_two') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_two') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_two') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_two') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_two') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_two') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_two') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_two') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_two') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_two') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_1_content2'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row ">
            <?php if( have_rows('column_1_content_2_accordion') ):  ?>
            <?php while ( have_rows('column_1_content_2_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
                   <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-6 col-12  <?php the_field('column_2_background_colour2')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_two') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_two') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_two') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_two') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_two') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_two') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_two') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_two') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_two') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_two') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_2_content2'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_2_content_2_accordion') ):  ?>
            <?php while ( have_rows('column_2_content_2_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <?php elseif ( get_field('number_columns') == 'Three columns') : ?>
   <div class="row">
      <div class="col-md-4 col-12 <?php the_field('column_1_background_colour3')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_three') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_three') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_three') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_three') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_three') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_three') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_three') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_three') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_three') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_three') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_1_content3'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_1_content_3_accordion') ):  ?>
            <?php while ( have_rows('column_1_content_3_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-4 col-12 <?php the_field('column_2_background_colour3')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_three') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_three') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_three') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_three') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_three') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_three') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_three') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_three') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_three') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_three') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_2_content3'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_2_content_3_accordion') ):  ?>
            <?php while ( have_rows('column_2_content_3_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-4 col-12 <?php the_field('column_3_background_colour3')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_three') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_three') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_three') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_three') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_three') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_three') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_three') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_three') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_three') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_three') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_3_content3'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_3_content_3_accordion') ):  ?>
            <?php while ( have_rows('column_3_content_3_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <?php elseif ( get_field('number_columns') == 'Four columns') : ?>
   <div class="row">
      <div class="col-md-3 col-12 <?php the_field('column_1_background_colour4')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_four') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_four') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_four') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_four') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_four') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_four') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_four') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_four') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_four') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_four') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_1_content4'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_1_content_4_accordion') ):  ?>
            <?php while ( have_rows('column_1_content_4_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-3 col-12 <?php the_field('column_2_background_colour4')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_four') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_four') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_four') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_four') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_four') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_four') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_four') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_four') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_four') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_four') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_2_content4'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_2_content_4_accordion') ):  ?>
            <?php while ( have_rows('column_2_content_4_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-3 col-12 <?php the_field('column_3_background_colour4')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_four') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_four') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_four') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_four') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_four') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_four') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_four') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_four') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_four') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_four') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_3_content4'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_3_content_4_accordion') ):  ?>
            <?php while ( have_rows('column_3_content_4_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-3 col-12 <?php the_field('column_4_background_colour4')?>">
         <div class="row">
            <div class="col-12">
               <div class="row" style="margin: <?php if ( get_field('inner_margin_four') == '50px') { echo "3.125rem"; } else  if ( get_field('inner_margin_four') == "100px") { echo "6.25rem"; } else if ( get_field('inner_margin_four') == '150px') { echo "9.375rem"; } else if ( get_field('inner_margin_four') == '200px') { echo "12.5rem"; } else if ( get_field('inner_margin_four') == '250px') { echo "15.675rem"; } else if ( get_field('inner_margin_four') == '300px') { echo "18.8rem"; } ?> 0">
                  <div class="col-12 m-auto <?php if ( get_field('inner_width_four') == '25%') { echo "col-md-3"; } else  if ( get_field('inner_width_four') == '50%') { echo "col-md-6"; } else if ( get_field('inner_width_four') == '75%') { echo "col-md-9"; } else if ( get_field('inner_width_four') == '100%') { echo "col-md-12"; } ?> ">
                     <?php the_field('column_4_content4'); ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <?php if( have_rows('column_4_content_4_accordion') ):  ?>
            <?php while ( have_rows('column_4_content_4_accordion') ) : the_row(); ?>
            <div class="col-12 p-0 accordion  accordion-border">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php if ( get_sub_field('number_acc_columns') == 'Single column') : ?>
                  <div class="row ">
                     <div class="col-12">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                  </div>
                  <?php else : ?>
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_left'); ?>
                     </div>
                     <div class="col-12 col-md-6">
                        <?php the_sub_field('content_right'); ?>
                     </div>
                  </div>
                  <?php endif; ?>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <?php endif; ?>
</div>