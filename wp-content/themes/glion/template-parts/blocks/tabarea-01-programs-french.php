<?php
/**
 * Block Name: Tabarea-01 Programs
 */
?>
<div class="block-tabarea-01-programs">
<div class="wrapper row">
      <div class="scrollElement col-md-6 col-12 pr-md-0 program-content">
         <div class="row mr-md-0">
            <div class="col-12 program-title">
               <h3><?php the_field('program_header', 4549) ?></h3>
            </div>
         </div>
         <div class="row mr-md-0">
            <div class="col-12 p-0">
               <ul id="myTab" class="nav nav-tabs" role="tablist">
                  <li class="nav-item darken active m-0" href="#bachelors_this" data-target=".bachelors_this, .bachelors_that" data-toggle="tab">
                     <a class="nav-link text-center nav-item" >
                     <?php the_field('program_1_tab_title', 4549) ?>
                     </a>
                  </li>
                  <li class="nav-item darken m-0" href="#masters_this" data-target=".masters_this, .masters_that" data-toggle="tab">
                     <a class="nav-link text-center nav-item" >
                     <?php the_field('program_2_tab_title', 4549) ?></a>  
                  </li>
                  </a>
               </ul>
            </div>
         </div>
         <div class="row program-text mr-md-0">
            <div class="col-xl-9 col-lg-11 col-11 m-auto pl-md-0">
               <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade show bachelors_this">
                     <h3>  <?php the_field('program_1_title', 4549) ?>
                     </h3>
                     <p> <?php the_field('program_1_details', 4549) ?></p>
                     <a class="mt-5 btn btn-primary bronze"  href="<?php echo get_field('program_1_link', 4549)['url'] ?>" role="button"><?php echo get_field('program_1_link', 4549)['title'] ?></a>
                  </div>
                  <div class="tab-pane fade hide masters_this">
                     <h3><?php the_field('program_2_title', 4549) ?></h3>
                     <p><?php the_field('program_2_details', 4549) ?></p>
                     <?php if (get_field('program_2_link', 4549)) : ?><a class="mt-5 btn btn-primary bronze"  href="<?php echo get_field('program_2_link', 4549)['url'] ?>" role="button"><?php echo get_field('program_2_link', 4549)['title'] ?></a><?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-12  p-0">
         <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade show bachelors_that">
               <img src="<?php echo get_field('program_1_image', 4549)['url']; ?>" alt="<?php echo get_field('program_1_image', 4549)['alt']; ?>"  />
            </div>
            <div class="tab-pane fade hide  masters_that">
            <img src="<?php echo get_field('program_1_image', 4549)['url']; ?>" alt="<?php echo get_field('program_1_image', 4549)['alt']; ?>"  />
               <div class="row p-0 m-0">
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class="masters-int-title accordion_title accordion_title_black_text coloured"><?php the_field('program_2_block_1_title', 4549) ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-int-text accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_1_text', 4549) ?></p>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_11', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_11', 4549)['title'] ?></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11  col-12 m-auto px-0 py-md-3">
                        <h6 class=" business-masters-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_2_title', 4549) ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg business-masters-text  accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_2_text', 4549) ?></p>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_22', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_22', 4549)['title'] ?></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row p-0 m-0">
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class=" masters-hosp-title accordion_title accordion_title_black_text coloured"><?php the_field('program_2_block_3_title', 4549) ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-hosp-text  accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_3_text', 4549) ?></p>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_33', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_33', 4549)['title'] ?></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class=" masters-lux-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_4_title', 4549) ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-lux-text  accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_4_text', 4549) ?></p>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_44', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_44', 4549)['title'] ?></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
			</div>