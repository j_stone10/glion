<?php
   /**
    * Block Name: Callout-02 Stats
    */
   ?>
<div class="block-callout-02-stats">
   <div class=" row stats <?php the_field('background_colour_stats')?> ">
      <div class="scrollElement col-12 my-5">
         <div class="counter  no-background text-center">
            <div class="row">
               <div class="col-sm-8 col-10 m-auto">
                  <h3 class="my-5"><?php the_field('stats_title') ?></h3>
               </div>
            </div>
            <div class="row wrapper">
               <div class="col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <p class="col-12" <?php if ( !the_field('stat_1_intro') ) { echo "style='margin-top: 65px'"; } ?>> <?php the_field('stat_1_intro'); ?></p>
                        <div class="font scrollElement"> <span class="value "><?php the_field('stat_1_figure') ?></span><?php the_field('stat_1_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_1_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 mr-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <p class="col-12" <?php if ( !the_field('stat_2_intro') ) { echo "style='margin-top: 65px'"; } ?>><?php the_field('stat_2_intro') ?></p>
                        <div class="font scrollElementOffset"><span class="value "><?php the_field('stat_2_figure') ?></span><?php the_field('stat_2_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_2_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="d-none col-md-4 mr-auto col-12 <?php if ( get_field('number_of_stats') == 'Three Stats')  { echo 'd-block'; } ?>">
                  <div class="row">
                     <div class="col-12  my-5">
                        <p class="col-12" <?php if ( !the_field('stat_3_intro') ) { echo "style='margin-top: 65px'"; } ?>><?php the_field('stat_3_intro') ?></p>
                        <div class="font scrollElementOffsetThird"><span class="value "><?php the_field('stat_3_figure') ?></span><?php the_field('stat_3_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_3_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row wrapper <?php if ( ( get_field('number_of_stats') == 'Two Stats' ) || ( get_field('number_of_stats') == 'Three Stats' ) )  { echo 'd-none'; } ?>">
               <div class="col-md-4 col-12 ml-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <p class="col-12" <?php if ( !the_field('stat_3_intro') ) { echo "style='margin-top: 65px'"; } ?>><?php the_field('stat_3_intro') ?></p>
                        <div class="font scrollElement"><span class="value "><?php the_field('stat_3_figure') ?></span><?php the_field('stat_3_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_3_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <p class="col-12" <?php if ( !the_field('stat_4_intro') ) { echo "style='margin-top: 65px'"; } ?>><?php the_field('stat_4_intro') ?></p>
                        <div class="font scrollElementOffset"><span class="value "><?php the_field('stat_4_figure') ?></span><?php the_field('stat_4_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_4_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 <?php if ( get_field('number_of_stats') == 'Four Stats')  { echo 'd-none'; } ?>">
                  <div class="row">
                     <div class="col-12  my-5">
                        <p class="col-12" <?php if ( !the_field('stat_5_intro') ) { echo "style='margin-top: 65px'"; } ?>><?php the_field('stat_5_intro') ?></p>
                        <div class="font scrollElementOffsetThird"><span class="value "><?php the_field('stat_5_figure') ?></span><?php the_field('stat_5_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_5_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   // jQuery(document).ready(function ($) {
   
   // /***************************************************
   // scrolling number js
   // ***************************************************/
   
   // $(window).scroll(testScroll);
   // var viewed = false;
   
   // function isScrolledIntoView(elem) {
   //  var docViewTop = $(window).scrollTop();
   //  var docViewBottom = docViewTop + $(window).height();
   
   //  var elemTop = $(elem).offset().top;
   //  var elemBottom = elemTop + $(elem).height();
   
   //  return (elemTop <= docViewBottom)  //&&  (elemTop >= docViewTop) ;
   // }
   
   // function testScroll() {
   //  if (isScrolledIntoView($(".counter")) && !viewed) {
   //    viewed = true;
   //    $('.value').each(function () {
   // 	 $(this).prop('Counter', 0).animate({
   // 	   Counter: $(this).text()
   // 	 }, {
   // 	   duration: 2000,
   // 	   easing: 'swing',
   // 	   step: function (now) {
   // 		 $(this).text(Math.ceil(now));
   // 	   }
   // 	 });
   //    });
   //  }
   // }
   // });
   
</script>