<?php
/**
 * Block Name: FAQ-01
 */
?>
<div class="block-faq-01 container-fluid py-4 py-md-5 faqs">
	<div class="wrapper row">
		<div class="scrollElement col">
			<div class="container">
                <div class="row">
                    <div class="col py-4 text-center">
                        <h5><?php the_field('title'); ?></h5>					
                    </div>
                </div>
				<div class="row">
					<div class="col-12 col-md-10 mx-auto mb-5 campus-accordion-module">
						<?php if( have_rows('accordion_row') ): $accordion++; ?>		
							<div class="accordion md-accordion" role="tablist" aria-multiselectable="true" id="accordion<?php echo $accordion; ?>">
								<?php while ( have_rows('accordion_row') ) : the_row(); $collapse++; ?>
									<div class="card">
										<div class="card-header" id="heading<?php echo $collapse; ?>">
											<a data-toggle="collapse" data-target="#collapse<?php echo $collapse; ?>" aria-expanded="true" aria-controls="collapse<?php echo $collapse; ?>" class="collapsed">
											<h6 class="mb-0">
												<?php the_sub_field('question'); ?> <i class="plus float-right"></i>
											</h6>
											</a>
										</div>
										<div id="collapse<?php echo $collapse; ?>" class="collapse" aria-labelledby="heading<?php echo $collapse; ?>" data-parent="#accordion<?php echo $accordion; ?>">
											<div class="card-body mb-2 mb-md-5">
												<div class="row">
													<div class="col-12 mb-4 mb-md-0 accordion-copy">
														<?php the_sub_field('answer'); ?>
													</div>
												</div>
											</div>
										</div>
									</div>								
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>