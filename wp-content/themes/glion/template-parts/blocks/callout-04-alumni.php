<?php
/**
 * Block Name: Callout-04 Alumni
 */
?>
<div class="block-callout-04-alumni py-5 program-alumni">
	<div class="wrapper row left_img_right_text">
      <div class=" col-md-6 col-12 p-0  d-flex justify-content-center align-items-center">
         <div class="overlay"></div>
		 <img class="img-fluid"  src="<?php echo get_field('quote_image')['url']; ?>"  alt="<?php echo get_field('quote_image')['alt']; ?>" />
	
         <div class="scrollElementOffset absolute col-9 m-auto text-center white-text">
            <h3><?php the_field('quote_image_text'); ?></h3>
         </div>
      </div>
      <div class=" col-md-6 col-12  py-5  d-flex justify-content-center align-items-center beige-bg">
         <div class="scrollElementOffsetThird col-9 col-sm-11  m-auto  text-center">
            <h3 class="mb-4">“<?php the_field('main_quote'); ?>”
            </h3>
            <p><?php the_field('cite'); ?></p>
			<a class="mt-5 btn btn-primary transparent" href="<?php echo get_field('quote_link')['url'] ?>"><?php echo get_field('quote_link')['title']?></a>
         </div>
      </div>
   </div>
</div>