	<?php
/*
Template Name: Modules
*/

get_header(); ?>
<div class="container-fluid">

	
            <div class="container breacrumb-trail-container d-none d-md-block absolute">
				<div class="row">
					<div class="col">
						<?php the_breadcrumb(); ?>
					</div>
				</div>
			</div>
            <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'modules' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
			?>	

	</div>	
<?php get_footer();
