<!-- Template for the all main Program pages  -->
<?php
/*
   Template Name: Campus Individual
   */

get_header(); ?>
<div class="container-fluid">
   <div class="row campus hero">
      <div class="background_image">
         <img src="<?php echo get_field('campus_hero')['url']; ?>" alt="<?php echo get_field('campus_hero')['alt']; ?>"" />
      </div>
      <div class=" scrollElementStart foreground_text col-md-9 col-11 m-auto">
         <h1 class="mb-3"><?php the_field('campus_hero_title'); ?></h1>
      </div>
      <div class="secondary_text col-lg-3 col-md-5 ">
         <?php if (get_field('campus_intro_button_1')) : ?>
            <a class="btn btn-primary bronze" href="<?php echo get_field('campus_intro_button_1')['url'] ?>" role="button"><?php echo get_field('campus_intro_button_1')['title'] ?></a>
         <?php endif; ?>
         <?php if (get_field('campus_intro_button_2')) : ?>
            <a class="btn btn-primary beige" href="<?php echo get_field('campus_intro_button_2')['url'] ?>" role="button"><?php echo get_field('campus_intro_button_2')['title'] ?></a>
         <?php endif; ?>
      </div>
      <div class="campus_share offset-lg-3 offset-md-5 col">
         <div class="col">
            <a href="#">
               <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                  <svg class="social_share_toggle" width="47" height="46" viewBox="0 0 47 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                     <path d="M2.2207 40.334C2.27539 40.7285 2.38281 41.0234 2.54297 41.2188C2.83594 41.5742 3.33789 41.752 4.04883 41.752C4.47461 41.752 4.82031 41.7051 5.08594 41.6113C5.58984 41.4316 5.8418 41.0977 5.8418 40.6094C5.8418 40.3242 5.7168 40.1035 5.4668 39.9473C5.2168 39.7949 4.82422 39.6602 4.28906 39.543L3.375 39.3379C2.47656 39.1348 1.85547 38.9141 1.51172 38.6758C0.929688 38.2773 0.638672 37.6543 0.638672 36.8066C0.638672 36.0332 0.919922 35.3906 1.48242 34.8789C2.04492 34.3672 2.87109 34.1113 3.96094 34.1113C4.87109 34.1113 5.64648 34.3535 6.28711 34.8379C6.93164 35.3184 7.26953 36.0176 7.30078 36.9355H5.56641C5.53516 36.416 5.30859 36.0469 4.88672 35.8281C4.60547 35.6836 4.25586 35.6113 3.83789 35.6113C3.37305 35.6113 3.00195 35.7051 2.72461 35.8926C2.44727 36.0801 2.30859 36.3418 2.30859 36.6777C2.30859 36.9863 2.44531 37.2168 2.71875 37.3691C2.89453 37.4707 3.26953 37.5898 3.84375 37.7266L5.33203 38.084C5.98438 38.2402 6.47656 38.4492 6.80859 38.7109C7.32422 39.1172 7.58203 39.7051 7.58203 40.4746C7.58203 41.2637 7.2793 41.9199 6.67383 42.4434C6.07227 42.9629 5.2207 43.2227 4.11914 43.2227C2.99414 43.2227 2.10938 42.9668 1.46484 42.4551C0.820312 41.9395 0.498047 41.2324 0.498047 40.334H2.2207ZM10.1062 43V34.3633H11.8934V37.6562H15.2684V34.3633H17.0613V43H15.2684V39.1445H11.8934V43H10.1062ZM22.3512 39.7363H24.5426L23.4645 36.3379L22.3512 39.7363ZM22.4625 34.3633H24.5016L27.5602 43H25.6031L25.0465 41.2246H21.8648L21.2672 43H19.3805L22.4625 34.3633ZM31.6664 35.8633V38.1836H33.7113C34.1176 38.1836 34.4223 38.1367 34.6254 38.043C34.9848 37.8789 35.1645 37.5547 35.1645 37.0703C35.1645 36.5469 34.9906 36.1953 34.643 36.0156C34.4477 35.9141 34.1547 35.8633 33.7641 35.8633H31.6664ZM34.1391 34.3633C34.7445 34.375 35.2094 34.4492 35.5336 34.5859C35.8617 34.7227 36.1391 34.9238 36.3656 35.1895C36.5531 35.4082 36.7016 35.6504 36.8109 35.916C36.9203 36.1816 36.975 36.4844 36.975 36.8242C36.975 37.2344 36.8715 37.6387 36.6645 38.0371C36.4574 38.4316 36.1156 38.7109 35.6391 38.875C36.0375 39.0352 36.3188 39.2637 36.4828 39.5605C36.6508 39.8535 36.7348 40.3027 36.7348 40.9082V41.4883C36.7348 41.8828 36.7504 42.1504 36.7816 42.291C36.8285 42.5137 36.9379 42.6777 37.1098 42.7832V43H35.1234C35.0688 42.8086 35.0297 42.6543 35.0063 42.5371C34.9594 42.2949 34.934 42.0469 34.9301 41.793L34.9184 40.9902C34.9105 40.4395 34.809 40.0723 34.6137 39.8887C34.4223 39.7051 34.0609 39.6133 33.5297 39.6133H31.6664V43H29.9027V34.3633H34.1391ZM46.1203 35.8926H41.55V37.7266H45.7453V39.2266H41.55V41.4473H46.3312V43H39.7863V34.3633H46.1203V35.8926Z" fill="#fff" />
                     <path d="M14.5 13.5V22.5C14.5 23.0967 14.7371 23.669 15.159 24.091C15.581 24.5129 16.1533 24.75 16.75 24.75H30.25C30.8467 24.75 31.419 24.5129 31.841 24.091C32.2629 23.669 32.5 23.0967 32.5 22.5V13.5" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" />
                     <path d="M28 6.75L23.5 2.25L19 6.75" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" />
                     <path d="M23.5 2.25V16.875" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" />
                     <rect x="10" width="27" height="27" />
                  </svg>
               <?php else : ?>
                  <img class="social_share_toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Share_Button_WHITE_FR.svg" />
               <?php endif; ?>
            </a>
         </div>
         <div class="col-9  social_view pt-1">
            <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            echo "<a  data-toggle='tooltip; data-placement='top' title='Share on Wattsap' target='_blank' href='https://api.whatsapp.com/send?text=" . urlencode($actual_link) . "' data-action='share/whatsapp/share'>"; ?>
            <svg width="22" height="22" viewBox="0 0 22 22" fill="#fff" xmlns="http://www.w3.org/2000/svg">
               <path d="M11.0027 0H10.9973C4.93213 0 0 4.72563 0 10.5365C0 12.8414 0.7755 14.9777 2.09413 16.7123L0.72325 20.6266L4.95138 19.3319C6.69075 20.4356 8.76562 21.0731 11.0027 21.0731C17.0679 21.0731 22 16.3461 22 10.5365C22 4.72695 17.0679 0 11.0027 0ZM17.4034 14.8789C17.138 15.5967 16.0847 16.192 15.2446 16.3659C14.6699 16.4831 13.9191 16.5766 11.3919 15.573C8.15925 14.2902 6.0775 11.1437 5.91525 10.9396C5.75988 10.7354 4.609 9.27346 4.609 7.76147C4.609 6.24948 5.41062 5.51324 5.73375 5.19714C5.99913 4.93768 6.43775 4.81915 6.8585 4.81915C6.99463 4.81915 7.117 4.82573 7.227 4.831C7.55013 4.84417 7.71237 4.86261 7.9255 5.35124C8.19088 5.96368 8.83712 7.47567 8.91412 7.63108C8.9925 7.7865 9.07088 7.99723 8.96088 8.20137C8.85775 8.4121 8.767 8.50561 8.60475 8.68473C8.4425 8.86386 8.2885 9.00083 8.12625 9.19312C7.97775 9.36039 7.81 9.53951 7.997 9.84902C8.184 10.1519 8.83025 11.1621 9.78175 11.9734C11.0096 13.0205 12.0051 13.3551 12.3612 13.4973C12.6266 13.6027 12.9429 13.5776 13.1368 13.3801C13.3829 13.1259 13.6867 12.7044 13.9961 12.2895C14.2161 11.9919 14.4939 11.955 14.7854 12.0604C15.0824 12.1592 16.654 12.9033 16.9771 13.0574C17.3003 13.2128 17.5134 13.2866 17.5917 13.417C17.6687 13.5473 17.6687 14.1598 17.4034 14.8789Z" fill="#fff" />
            </svg>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" class='social-share facebook' href="#">
               <svg width="22" height="22" viewBox="0 0 22 22" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                  <g clip-path="url(#clip0)">
                     <path d="M11 0C4.92525 0 0 4.36739 0 9.75551C0 12.8256 1.59913 15.5638 4.09888 17.3523V21.0731L7.84437 19.104C8.844 19.3688 9.90275 19.5123 11 19.5123C17.0748 19.5123 22 15.1449 22 9.75683C22 4.36871 17.0748 0 11 0ZM12.0931 13.1377L9.29225 10.2758L3.82662 13.1377L9.8395 7.02391L12.7091 9.8859L18.106 7.02391L12.0931 13.1377Z" fill="#fff" />
                  </g>
                  <defs>
                     <clipPath id="clip0">
                        <rect width="22" height="21.0731" fill="white" />
                     </clipPath>
                  </defs>
               </svg>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" class='social-share copy' href="#">
               <svg width="25" height="25" viewBox="0 0 25 25" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                  <path d="M20.3982 9.82129H11.7444C10.6823 9.82129 9.82129 10.6823 9.82129 11.7444V20.3982C9.82129 21.4603 10.6823 22.3213 11.7444 22.3213H20.3982C21.4603 22.3213 22.3213 21.4603 22.3213 20.3982V11.7444C22.3213 10.6823 21.4603 9.82129 20.3982 9.82129Z" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                  <path d="M5.56333 15.1787H4.60179C4.09176 15.1787 3.60261 14.9761 3.24197 14.6155C2.88132 14.2548 2.67871 13.7657 2.67871 13.2556V4.60179C2.67871 4.09176 2.88132 3.60261 3.24197 3.24197C3.60261 2.88132 4.09176 2.67871 4.60179 2.67871H13.2556C13.7657 2.67871 14.2548 2.88132 14.6155 3.24197C14.9761 3.60261 15.1787 4.09176 15.1787 4.60179V5.56333" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
               </svg>
            </a>
            <a data-toggle="tooltip" data-placement="top" class='social-share email' title="Share via Email" href="#">
               <svg width="25" height="25" viewBox="0 0 25 25" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                  <path d="M22.8027 2.97852H2.19727C0.987207 2.97852 0 3.96465 0 5.17578V19.8242C0 21.0312 0.982813 22.0215 2.19727 22.0215H22.8027C24.0098 22.0215 25 21.0387 25 19.8242V5.17578C25 3.96875 24.0172 2.97852 22.8027 2.97852ZM22.4993 4.44336L12.5466 14.3961L2.50776 4.44336H22.4993ZM1.46484 19.5209V5.47212L8.51948 12.4663L1.46484 19.5209ZM2.50063 20.5566L9.55972 13.4976L12.0332 15.9498C12.3195 16.2337 12.7816 16.2328 13.0667 15.9476L15.4785 13.5358L22.4994 20.5566H2.50063ZM23.5352 19.5208L16.5143 12.5L23.5352 5.4791V19.5208Z" fill="#fff" />
               </svg>
            </a>
            <a data-toggle="tooltip" data-placement="top" class='social-share linkedin' href="#">
               <svg width="19" height="19" viewBox="0 0 19 19" fill="#fff" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <rect width="19" height="19" fill="url(#pattern0)" />
                  <defs>
                     <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                        <use xlink:href="#image0" transform="scale(0.00195312)" />
                     </pattern>
                     <image id="image0" width="512" height="512" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAACXBIWXMAAA3XAAAN1wFCKJt4AAAFGmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDIgNzkuMTY0MzYwLCAyMDIwLzAyLzEzLTAxOjA3OjIyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgMjEuMSAoTWFjaW50b3NoKSIgeG1wOkNyZWF0ZURhdGU9IjIwMjAtMDYtMTFUMTA6NDg6NTkrMDI6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDIwLTA2LTExVDEwOjQ5OjE4KzAyOjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDIwLTA2LTExVDEwOjQ5OjE4KzAyOjAwIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9InNSR0IgSUVDNjE5NjYtMi4xIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjIzZDM1MDU3LTU0NTgtNGJjMC1hNTM1LTEyYmNkZWQyMDM5OSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoyM2QzNTA1Ny01NDU4LTRiYzAtYTUzNS0xMmJjZGVkMjAzOTkiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoyM2QzNTA1Ny01NDU4LTRiYzAtYTUzNS0xMmJjZGVkMjAzOTkiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjIzZDM1MDU3LTU0NTgtNGJjMC1hNTM1LTEyYmNkZWQyMDM5OSIgc3RFdnQ6d2hlbj0iMjAyMC0wNi0xMVQxMDo0ODo1OSswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjEgKE1hY2ludG9zaCkiLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+dpIx7wAAF7NJREFUeNrt3XvwZnVh3/H3LmAXZYEGgmWliYIVHJPoBIGaGqJgo9UB73hBqlaNkrYTJ2qCEtImEhsaoqSN49hIo1EwGivqjuaKNUq8gFMH4gVQ6KjpEglRuS9B9tc/zlmyrix7+/12n+c5r9fMM78Vlt9zzuc84/fzfM/3nLNqaWmpOXBQdfT4Omb8ua46YHytHX+uCQBWzsbqturW8edt1Ybqmurq8ec11c2zviOrZrQAHFqdVJ08vo7ymQNgjlxXXTq+Pl7dpABs25HVi6pnVo+uVvn8ALAAlqorq0uq91TXKwB1cHVadUb1eJ8RACbgsurd1fur706tABxYnVW9utrfZwGACbqzuqD6zeqWRS8A+1VnVuc0nOcHgKm7qXpj9bbq7kUsAKdVb8qCPgC4L9dVb2g4NbAQBeDE6req4x1bANiuy6vXVZ+c1wLwiOr86hTHEgB22vrqtdW181QATqkuarhBDwCwa26tTh/LwLJavQIbe1b1IYM/AOy2teOYetYszwCsqS6sXuh4AcCyu7h6WcPtiGemAKwbG8pxjg8ArJgrqmc0PH9gtyzHKYDjxg0y+APAylq2MXd3C8DpDZcprHNMAGCPWDeOvafvzi/ZnVMApzc81AAA2Dte1HDV3R4rAMeN7WON7AFgr9nYcMO9K/ZEAVg3vpFpfwDY+zaMX8x3amHgzq4BWNOw2t/gDwCzYfOVeDs1K7+zBeDCrPYHgFlz3DhGr0gBOCs3+QGAWfXCduKOgTu6BuCUhumF1fIFgJm1qeFGQdt9dsCOFIBHVJ/Pvf0BYB7cWj227TxFcEe+0Z9v8AeAubF2HLt3awbgxOovZQkAc+dnGu7Zs0sF4HPV8TIEgLlzeXXCtv7l/Z0COM3gDwBz6/hxLN+pGYD9qq9UR8kPAObWddUjq7t3dAbgTIM/AMy9o8YxfYdmAA4cG8OhcgOAuXfTWARu2d4MwFkGfwBYGId2H3cI3HoG4OCGpwntLy8AWBh3Njw06LvbmgE4zeAPAAtn/7a6ImDrAnCGjABgIX3fGL/lKYAjGxb/AQCL6ajq+q1nAF4kFwBYaPeO9VsWgGfKBQAW2r1j/eZTAIdWN1arZAMAC2upOqy6afMMwEkGfwBYeKvGMf/eUwAnywQAJuFkBQAAJloAVi0tLR3UFncGAgAW3sGrq6PlAACTcrQCAAATLQDHyAEAJuUYMwAAMNEZgHVyAIBJWbe6OkAOADApBygAADDRArBWDgAwKWvNAADABGcAVi2NjwMEAKZjtQgAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAACg9hXB3LinurHasMXrhq3+953V4dW68XVff36gKAFYtbS0tCSGmbRUXVF9ZHx9eSwBu+uHqydXp1ZPqdaKGkABYO/aWF06Dvjrx2/4K+kB1ROrp1enVEc4BAAKAHvGXdUfVh+u/qy6fS9uy7HjzMAZ1cMcGgAFgOW3qXpPdU71jRnbtv2qV47bdphDBaAAsDw+Vp1V/fWMb+cB1Wur14x/BkABYBdcXv1y9Yk52+7DxtmAV46zAwAoAOyAr1ZvqD4w5/txZHVu9fxqlcMKoACwbe+rXtpwjf6ieE71rtxTAGBuuRPgyllqmDZ//oIN/jXMZDy++qbDDGAGgH90e8OldJcs+H4+eNzHxznkAGYApu7r1U9NYPCv+lbDjYTe5bADKABTdll1XHXVhPb5ruolDZcLbvIRAJgPTgEsn/eOA+E/TDiDp1UfbLjFMAAKwCS++Z888cF/s5dV7xADwGxzCmD3fb16lsH/XhdWvyMGADMAi+z2hgV/V4ni++xT/XH1r0UBYAZg0Sw1XOpn8P9B91TPa7gDIgAKwEL51aZxqd+u+k7Do4VvEQXA7HEKYNe8r+EOf2zfU6v1yiaAGYB599WGe/uzYz5WvUkMAGYA5t1zm/+n+u1pD6q+Vv0zUQCYAZhHlxv8d8nt1a+JAcAMwLx6YvUJMeySfasvVkeLAsAMwDz5mMF/t3yver0YAMwAzJNN1WOqvxbFbvurhpsnAWAGYOa9x+C/bH5JBABmAObBXdUjqm+IYtlcUj1DDABmAGbZHxr8l915IgBQAGbdh0Ww7D5X/a0YABSAWbWx+jMxLLulhtsDA6AAzKRLG25iw/L7iAgAFACD1DTL1R1iAFAAZo1p6pV1Z/XnYgBQAGbNFdUNYlhRFlgCKAAzx/T/yvtow10WAVAAFIAJubH6rBgAFIBZcU/1ZTHsEVeJAEABmKVvpveIYY+wzgJAAZgZG0QgawAFwKCErAEUAIMSsgZQABaR89KyBlAAfCtlBd1YfU8MAAqAAjAtS3k0MIACoADIGwAFYG+5UwTyBlAApudwEcgbQAGYnnUikDeAAmBAYuWsrQ4QA4ACMAtMScsaQAEwA4CsARQAgxKyBlAAFpJpaVkDKAC+lSJrAAVgCh5Y/bAY9oiHigBAAZglTxbBitu3OkkMAArALDlVBCvuxOpgMQAoALPkKdUDxLCini4CAAVg1qytniiGFWWWBUAB8A11Yn4iCwABFIAZdYoIfPsHUACm54jqWDGsCLMrAAqAb6oT8xDFCkABmHVnVPuJYVm9tFolBgAFYJY9rHqlGJbNodXrxACgAMyDc6oDxLAsfqU6UAwACsA8OKx6rRh228OqM8UAsPetWlpaWhLDDrmtOqq6URS77OLqBWIAMAMwTw5oOBXArvnJ6vliADADMI/uro6prhfFTvvz6kliADADMI/2q84Vw077WYM/gBmAebdUnVZ9QBQ75JDqioYFgAAoAHPtjurx1RdEcb/2bZj6f4IoAGaLUwC75oHVh6sHi+J+/TeDP4ACsGj+eXVJ9U9EcZ/OzDX/ADPLKYDd967qJWL4Pk9omPrfVxQAZgAW1Yur14jhXg9rWCBp8AcwA7DwNjU8NvijE89hbfXp6sd8JADMAEwlxw9WL5v4N3+DP4ACMDkPqN5RXVDtM7F9f0LDtf4GfwAFYLJ+ofrj6p9OZH/PbFjwd4hDDzA/rAFYOV9tWBdw9YLu374N1/m71A9AAWArtzQ8/vZjC7ZfhzSs9H+CQwwwn5wCWFkHVuurN1YPWpB9+tmG8/0GfwAFgO1k/CvV16pXNb/Xx/9kw7n+P82DfQDmnlMAe9411esbbiM8Dx5W/Ub1/GqVwwegALB7Pl39UvVXM7p9hzbMXJzZcIkjAAoAy+hD1XnV56pZOBYPqV5ava5hDQMACgAr6G8bFgx+pLq0unMPvvdPNFyy+PTq2Ez1AygA7BV3NCy4+3DD8wVuXObfv2914jjgn1o9VOQACgCzZVP12eqq6oZqwxavG8ZycF/HcG11eLVufG3+80Ork6qDRQugADC/vtdw+mBDw2mDzQP9AaIBQAEAAO7lRkAAoAAAAAoAAKAAAAAKAACgAAAACgAAoAAAAAoAAKAAAAAKAACgAAAACgAAoAAAAAoAAKAAAAAKAACgAACAAgAAKAAAgAIAACgAAIACAAAoAACAAgAAKAAAgAIAACgAAIACAAAoAACAAgAAKAAAgAIAACgAAIACAAAKAAAwHfuKAGBZLVXfqW4aX9+p1lRrt3gdWB3gSxgKAMB8+Ub16eqz459v2uL17eqeHfw9DxrLwEHVUdUxW70OFTUrZdXS0tKSGAC26a7q/1SfGQf9z1Qb9tB7H7JFGfjx6onjz1UOCwoAwPL7YvXe6n+Pg/9dM7Rth1UnVU+qTq4e6nChAADsum+Mg/7F1VVztN1HjUXgSdVTGtYYgAKwAjZW14hhpxxSHeH4O/4z6NvVH42D/qcaFvDNswdWz65e0nC6wKkCFIBldGX1GDHslFdVb3P8Hf8Zsan6cPX71Z9Udy9o7j9S/dvqxdXDfQzZmktQgKm4p3pP9WPVs6r1Czz413BK49zqX1Q/XV1Y3eZjgAIATMU/VO+ojq7OqL4ywQwuq17esGDw3OpmHwsUAGBRbax+t2H6+xXVdSLp76tzqh+tfrVhDQQKAMBCuL06f/y2+x+rb4rkB9xcvXHM6Kzq70SiAADMs/XVI6vXVd8Sx3bdWp03FoFfHGcIUAAA5sYN1XOrU33j3yV3VG9puOPgO8WhAADMuqXq7eO3/g+IY7fdVL20+pmmuVhSAQCYA1+pTmy4z4BV7cvrk9Wjq7OrO8WhAADMgruq/9RwQ6bLxLFi7q7e1HDfhD8RhwIAsDddWx1b/XrD9f2svOurf9NwV0E3ElIAAPa49dXx1ZdEsVe8u3ps8/WgJBQAYI4tVb9WPT3n+ve2a6oTqt8ThQIAsJJuHgf+/9z8P6lvUWysfq56YcN9BFAAAJbVVxqm/NeLYia9t+GUwJWiUAAAlsslDVPN14pipl1b/cuG9QEoAAC75fzq2ZlenhcbqxdXbxaFAgCwq97ScB9/5/vny1L1moYHC6EAAOyUtzY8kIb5dV718uoeUSgAADvifzQ8upf5d2H1nIZTAygAANv0+w338zftvzg+1HD3wFtEoQAA3JeLGqaMDf6L5xPVyVnMqQAAbOX9DavHN4liYX2+ekbDA5xQAAD6THV6FotNwccb7hqo6CkAwMR9p3pB9T1RTMYHG9Z5oAAAE/bvqq+LYXJ+rzpbDAoAME2/27BCnGl6U3WBGBQAYFq+UL1WDJP3iw0PEkIBACbgtup5WQ3OcMnny6sviUIBABb///BfVX1VFIzuqE6rbheFAgAsrvUNN/yBLX25+nkxKADA4togArbhD6r/KQYFAIDp+Q/VF8WgAAAwLXdWz816AAUAgMm5uvr3YlAAAJied1WXikEBAGB6fj73ilAAAJica6vzxKAAADA9/6W6TgwKAADTsjELAhUAACbpT6s/EoMCAMD0vLq6VQwKAADTsqF6ixgUAACm53fMAigAAEzPt6u3ikEBAGB63lzdIQYFAIBp+bvq7WJQAACYnt9quD8ACgAAE3JDdaEYFAAApue86m4xKAAATMs3q4+JQQEAYHreKQIFAIDp+Wh1kxgUAACm5e7qIjEoAABMz7tEoAAAMD1fqK4Sw+7bVwTAAltb/Wh1RPWQrX4eMf6dvxlf/2+rn1/Pg2hm1TsbbhGMAgBwr3XVM6vnVD9d7bOdv//j2/jn91Sfqj5QXdLweFpmw0XVfzWGKQAAP1I9e3z9VLVqGX7nPtUTxtd/rz5d/a/x9Q2R71U3Vp8ZCx67yBoAYJ6dPA7MX2+YEv5XyzT4b23V+LvfPL7Xp8f3Zu/5CxEoAMD0PKrhmvC/qB63F97/ceN7f3TcFhQABQBgBT244dGwV1ZPnYHteeq4LW8ft4095/Is0lQAgIW3f3VO9bXq59r+wr49aZ9xm742buP+Dtce8b3qL8WgAACL6yENq/F/vTpghrfzgHEbPzVuMyvvUhEoAMBiOr66ojp2jrb52HGbj3f4Vpx1AAoAsIBe0DDFe/gcbvvh47a/wGFcUV+sviUGBQBYDKuqc6uLqzVzvB9rxn04t5W5NJGBdQAKALAA9mu4897ZC7RPZ4/7tJ/DuyK+JAIFAJh/b62etYD79axx31h+V4tAAQDm2y9Ur1jg/XvFuI8oAAoAwOjJ1W9PYD9/e9xXls+11SYxKADA/Dmmel+zdXOflbLPuK/HOOzLZmPD8xlQAIA58kPV+uqgCe3zQeM+/5DDv2ycBlAAgDnzB9XDJ7jfDx/3HQVAAQAm5ynV0ya8/08bM0ABUACAydinOl8Mnd801j6stP8rAgUAmA8vqx4lhh41ZsHuuUUECgAw+zY/NY/BrD/lcB7cKgIFAJh9v1w9WAz3evCYCWYAFABgYT2keo0YfsBrxmwwA6AAAAvpJdX+YvgB+4/ZoAAoAMBCeo4IZLMCNlW3i0EBAGbTkdVjxLBNjxkzwiyAAgAslGeLQEYKgAIAGNyQ0XK6QwQKADB7jqiOF8N2HT9mBQoAsBCeVa0Sw3atGrMCBQBYCCeIQFYoAMD0mNaWFQoAYFBDVigAwBSsE4GsUACAaTm0WiOGHbZmzAwUAGCueciNzFAAgAlyTltmKADABB0uApmhAADTs68IZIYCAAAoAACAAgAAKAAAgAIAACgAAIACAAAoAACAAgAAKAAAoAAAAAoAAKAAAAAKAACgAAAACgAAoAAAAAoAAKAAAAAKAACgAAAACgAAoAAAAAoAAKAAAAAKAACgAACAAiACAFAAAAAFAABQAAAABQAAUAAAAAUAAFAAAAAFAABQAAAABQAAUAAAAAUAAFAAAAAFAABQAAAABQAAUAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAABQAAUAAAAAUAAFAAAAAFAABQAAAABQAAUAAAAAUAAFAAAAAFAABQAAAABQAAUAAAAAUAAFAAAAAFAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAFAAAAAFAABQAAAABQAAUAAAAAUAAFAAAAAFAABQAAAABQAAUAAAAAUAAFAAAAAFAABQAAAABQAAUAAAQAEQAQAoAACAAgAAKAAAgAIAACgAAIACAAAoAACAAgAAKAAAgAIAACgAAIACAAAoAACAAgAAKAAAgAIAACgAAKAAAAAKAACgAAAAi2LV0tLSkhh2ysbqGjHslEOqIxx/x58d9jfV34thpxxdrRGDAgAA3A+nAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAUAABAAQAAFAAAQAEAABQAAEABAAAFAABQAAAABQAAUAAAAAUAAFAAAIBZLwAbxQAAk7JxdXWbHABgUm5bXd0qBwCYlFvNAADARGcAFAAAmGAB2CAHAJiUDaura+QAAJNyzerqajkAwKRcbQYAACY4A7BqaWnpoOq7sgCAyTh4dXVzdZ0sAGASrqtu3vwsgEvlAQCTcGn948OAFAAAmFABWLW0tFR1aHVjtUouALCwlqrDqps2zwDcVF0pFwBYaFeOY/69pwCqLpELACy0e8f6zacAqo7M1QAAsMiOqq7fegbg+uoy2QDAQrps8+C/dQGoerd8AGAhfd8Yv+UpgKqDG54OuL+cAGBh3Fmta4s7/249A/Dd6gI5AcBCuaCtbvu/9QxA1YENiwEPlRcAzL2bGhb/3bLlP1x9H3/xluqN8gKAhfDGrQf/bc0AVO1XfWVsDADAfLquemR199b/YvU2/oO7qzfIDQDm2hvua/C/vxmAzT5XHS8/AJg7l1cnbOtfrt7Of/w6+QHAXLrfMXx7BeCT1XoZAsBcWT+O4du0vVMAVY+oPl+tlScAzLxbq8dW1+7ODEDjLzi92iRTAJhpm8Yx+9rt/cXVO/gL11dnyxUAZtrZ7eCp+x05BbCli6oXyhcAZs7F47f/HbKzBWBNw6KC4+QMADPjiurEauNKFYAaniZ0xfgTANi7NoxfzDfszH+0ehff6Bk70zIAgBWxcRyTN+zsf7h6F9/wiurlcgeAverl45i801bvxpteVL3ITAAA7JVv/i8ax+JdsitrALZ2XPWhrAkAgD1h86n4K3bnl6xehg25YiwBVzgmALCilm3MXb1MG7Sh4fKDix0bAFgRF49j7Ybl+GWrl3HDNjbcgOD1uW0wACyXTePYenrLuO5uOdYA3JdTGhYmeIAQAOy6W8eBf9mfzLt6hTZ4fcOTiDxKGABmcCxdvYIbfm11avUz1eWOIwDskMvHsfPUduCpfrNYADb7ZHVC9bzqOscVAO7TdeNYecI4dq6olVoDsC37VWdW51SHOtYA0E3VG6u3VXfvqTfd0wVgswOrs6pXV/s79gBM0J3VBdVvVrfs6TffWwVgs4Or06ozqsf7LAAwAZdV767eX313b23E3i4AWzqy4b7Gz6weXa3yGQFgASxVV1aXVO+prp+FjZqlArClQ6uTqpPH11E+PwDMkeuqS8fXxxvO88+UWS0AWzuoOnp8HTP+XFcdML7Wjj/X+MwBsII2Vrc13KDntvG1obqmunr8eU1186zvyP8HGkFW9uzmbioAAAAASUVORK5CYII=" />
                  </defs>
               </svg>
            </a>
         </div>
      </div>
   </div>
   <div class="row white-strip <?php if (get_field('hide_open_day_section')) {
                                    echo ' d-none';
                                 } ?>">
      <div class="pl-5 col-lg-3 col-md-5">
         <p class="mt-1 open_day_intro">NEXT OPEN DAY: <?php the_field('open_day_date'); ?></p>
      </div>
   </div>
   <div class="wrapper row white-strip mt-5 mb-5 justify-content-center align-items-center">
      <div class="scrollElement col-10 col-md-9 m-auto text-center">
         <h2 style="font-weight: 300"><?php the_field('campus_intro_title'); ?></h2>
      </div>
   </div>
   <div class="row line-after">
      <div class="">
         <div class="row">
            <a data-toggle="modal" data-target="#campusGallery1" class="campus_gallery">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_1']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_1']['alt']; ?>" />
            </a>
            <a data-toggle="modal" data-target="#campusGallery2" class="campus_gallery">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_2']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_2']['alt']; ?>" />
            </a>
            <a data-toggle="modal" data-target="#campusGallery3" class="campus_gallery">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_3']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_3']['alt']; ?>" />
            </a>
            <a data-toggle="modal" data-target="#campusGallery4" class="campus_gallery">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_4']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_4']['alt']; ?>" />
            </a>
            <a data-toggle="modal" data-target="#campusGallery5" class="campus_gallery">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_5']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_5']['alt']; ?>" />
            </a>
         </div>
      </div>
   </div>
   <div class="modal fade" id="campusGallery1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog campusGallery" role="document">
         <div class="modal-content campusGallery">
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_1']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_1']['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="campusGallery2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog campusGallery" role="document">
         <div class="modal-content campusGallery">
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_2']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_2']['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="campusGallery3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog campusGallery" role="document">
         <div class="modal-content campusGallery">
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_3']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_3']['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="campusGallery4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog campusGallery" role="document">
         <div class="modal-content campusGallery">
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_4']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_4']['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="campusGallery5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog campusGallery" role="document">
         <div class="modal-content campusGallery">
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
               <img src="<?php echo get_field('campus_gallery')['gallery_image_5']['url']; ?>" alt="<?php echo get_field('campus_gallery')['gallery_image_5']['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row white-strip mt-5 justify-content-center align-items-center">
      <div class="scrollElement col-10 col-md-9  m-auto text-center">
         <?php if (get_field('campus_hero_title') == "London") : ?>
            <h2 class="mb-5">Introducing London Campus</h2>
         <?php endif; ?>
         <?php if (get_field('campus_hero_title') == "Switzerland") : ?>
            <h2 class="mb-5">Switzerland Campus Locations</h2>
         <?php endif; ?>
         <?php if (get_field('campus_hero_title') == "Londres") : ?>
            <h2 class="mb-5">Le campus de Londres</h2>
         <?php endif; ?>
         <?php if (get_field('campus_hero_title') == "Suisse") : ?>
            <h2 class="mb-5">
               Campus Suisse
            </h2>
         <?php endif; ?>
      </div>
   </div>
   <?php if ((get_field('campus_hero_title') == "Suisse") || (get_field('campus_hero_title') == "Switzerland")) : ?>
      <div class=" row block-tabarea-02-2tabs pb-5">
         <ul class=" nav nav-tabs m-auto home-programmes two-tabs d-flex" role="tablist">
            <li class="nav-item col ml-auto">
               <!-- <button class="mt-5  btn btn-primary active mr-5" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab"><?php the_field('tab_1'); ?></button> -->
               <button class="mt-5  btn btn-primary active mr-5" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab">Glion</button>
            </li>
            <li class="nav-item col mr-auto">
               <!-- <button class="mt-5 btn btn-primary" href="#tabarea-02-2tabs-tab2" role="tab" data-toggle="tab"><?php the_field('tab_2'); ?></button> -->
               <button class="mt-5 btn btn-primary" href="#tabarea-02-2tabs-tab2" role="tab" data-toggle="tab">Bulle</button>
            </li>
         </ul>
      </div>
   <?php endif; ?>
   <?php
   $tab1 = get_field('campus_tabs')['campus_tab_1'];
   $tab2 = get_field('campus_tabs')['campus_tab_2'];
   ?>
   <div class="row">
      <div class=" tab-content module-tabs-content" style="overflow: hidden">
         <!-- TAB 1 -->
         <div role="tabpanel" class=" tab-pane flex-tabs w-100 pb-5 active" id="tabarea-02-2tabs-tab1">
            <div class="wrapper row">
               <div class=" col-md-6 col-12 pr-md-0 program-content ind order-2 order-md-1">
                  <div class="scrollElement row program-text individual mr-md-0">
                     <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                        <?php echo $tab1['left_content']; ?>
                     </div>
                  </div>
               </div>
               <div class=" col-md-6 col-12 order-1 order-md-2 image fill p-0">
                  <?php if ($tab1['media_type'] == 'video') : ?>
                     <div class="video_wrapper1">
                        <a data-toggle="modal" data-target="#content1">
                           <div class="video">
                              <img style="min-width: 100%" src="<?php echo $tab1['right_image_test']['url']; ?>" alt="<?php echo $tab1['right_image_test']['alt']; ?>" />
                              <!-- <iframe style="min-width: 100%" width="100%" height="100%"
                           src="<?php //echo $tab1['image_video_content_video']; 
                                 ?>">
                           </iframe> -->
                           </div>
                           <div class="play_icon">
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
                           </div>
                        </a>
                     </div>
                  <?php else : ?>
                     <img style="min-width: 100%" src="<?php echo $tab1['right_image_test']['url']; ?>" alt="<?php echo $tab1['right_image_test']['alt']; ?>" />
                  <?php endif; ?>
               </div>
            </div>
            <?php if ($tab1['media_type'] == 'video') : ?>
               <div class="modal fade" id="content1" tabindex="-1" role="dialog" aria-labelledby="content1" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
                        </div>
                        <div class="modal-body">
                           <iframe class="video_id" width="100%" height="500px" src="<?php echo $tab1['image_video_content_video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            <?php endif; ?>
            <div class="wrapper row  white-strip mt-5 justify-content-center align-items-center">
               <div class="scrollElement col-10 col-md-9  m-auto text-center">
                  <h2 class="mb-5"><?php echo $tab1['programs_title']; ?></h2>
               </div>
            </div>
            <div class="container">
               <div class="row my-5 alumni">
                  <div id="alumni-carousel" class="wrapper carousel slide w-100 glion-alumni-carousel" data-ride="carousel" data-interval="false">
                     <ol class="carousel-indicators d-flex d-md-none">
                        <li data-target="#alumni-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#alumni-carousel" data-slide-to="1"></li>
                        <li data-target="#alumni-carousel" data-slide-to="2"></li>
                        <!-- <li data-target="#alumni-carousel" data-slide-to="3"></li> -->
                     </ol>
                     <div class="carousel-inner row w-100 mx-auto" role="listbox">
                        <div class="scrollElement carousel-item item col-12 col-md-4 p-0  active">
                           <div class="d-flex" style='flex-direction: column'>
                              <div class="row">
                                 <div class="col-12">
                                    <img style="min-width:100%" src="<?php echo $tab1['callout_image_1_test']['url']; ?>" alt="<?php echo $tab1['callout_image_1_test']['alt']; ?>" />
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-10 py-5 m-auto text-center">
                                    <?php echo $tab1['callout_content_1']; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="scrollElementOffset carousel-item item col-12 col-md-4  p-0 lavender-bg">
                           <div class="d-flex" style='flex-direction: column'>
                              <div class="row order-1 order-md-2">
                                 <div class="col-12">
                                    <img style="min-width:100%" src="<?php echo $tab1['callout_image_2_test']['url']; ?>" alt="<?php echo $tab1['callout_image_2_test']['alt']; ?>" />
                                 </div>
                              </div>
                              <div class="row order-2 order-md-1">
                                 <div class="col-10 py-5 m-auto text-center">
                                    <?php echo $tab1['callout_content_2']; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="scrollElementOffsetThird carousel-item item col-12 col-md-4  p-0 lavender-bg">
                           <div class="d-flex" style='flex-direction: column'>
                              <div class="row ">
                                 <div class="col-12">
                                    <img style="min-width:100%" src="<?php echo $tab1['callout_image_3_test']['url']; ?>" alt="<?php echo $tab1['callout_image_3_test']['alt']; ?>" />
                                 </div>
                              </div>
                              <div class="row ">
                                 <div class="col-10 py-5 m-auto text-center">
                                    <?php echo $tab1['callout_content_3']; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <a class="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <!-- TAB 2 -->
         <?php if ((get_field('campus_hero_title') == "Suisse") || (get_field('campus_hero_title') == "Switzerland")) : ?>
            <div role="tabpanel" class="tab-pane flex-tabs w-100 pb-5" id="tabarea-02-2tabs-tab2">
               <div class="wrapper row ">
                  <div class=" col-md-6 col-12 pr-md-0 program-content ind order-2 order-md-1">
                     <div class="scrollElement row program-text individual mr-md-0">
                        <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                           <?php echo $tab2['left_content']; ?>
                        </div>
                     </div>
                  </div>
                  <div class=" col-md-6 col-12 order-1 order-md-2 image fill p-0">
                     <?php if ($tab2['media_type'] == 'video') : ?>
                        <div class="video_wrapper1">
                           <a data-toggle="modal" data-target="#content2">
                              <div class="video">
                                 <img style="min-width: 100%" src="<?php echo $tab2['right_image_test']['url']; ?>" alt="<?php echo $tab2['right_image_test']['alt']; ?>" />
                              </div>
                              <div class="play_icon">
                                 <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
                              </div>
                           </a>
                        </div>
                     <?php else : ?>
                        <img style="min-width: 100%" src="<?php echo $tab2['right_image_test']['url']; ?>" alt="<?php echo $tab2['right_image_test']['alt']; ?>" />
                     <?php endif; ?>
                  </div>
               </div>
               <?php if ($tab2['media_type'] == 'video') : ?>
                  <div class="modal fade" id="content2" tabindex="-1" role="dialog" aria-labelledby="content2" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
                           </div>
                           <div class="modal-body">
                              <iframe class="video_id" width="100%" height="500px" src="<?php echo $tab2['image_video_content_video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>
                  </div>
               <?php endif; ?>
               <div class="wrapper row white-strip mt-5 justify-content-center align-items-center">
                  <div class="scrollElement col-10 col-md-9  m-auto text-center">
                     <h2 class="mb-5"><?php echo $tab2['programs_title']; ?></h2>
                  </div>
               </div>
               <div class="container">
                  <div class="row my-5 alumni">
                     <div id="alumni-carousel1" class="wrapper carousel slide w-100 glion-alumni-carousel" data-ride="carousel" data-interval="false">
                        <ol class="carousel-indicators d-flex d-md-none">
                           <li data-target="#alumni-carousel1" data-slide-to="0" class="active"></li>
                           <li data-target="#alumni-carousel1" data-slide-to="1"></li>
                           <li data-target="#alumni-carousel1" data-slide-to="2"></li>
                           <li data-target="#alumni-carousel1" data-slide-to="3"></li>
                        </ol>
                        <div class="carousel-inner row w-100 mx-auto" role="listbox">
                           <div class="scrollElement carousel-item item col-12 col-md-3 p-0  active">
                              <div class="d-flex" style='flex-direction: column'>
                                 <div class="row">
                                    <div class="col-12">
                                       <img style="min-width:100%" src="<?php echo $tab2['callout_image_1_test']['url']; ?>" alt="<?php echo $tab2['callout_image_1_test']['alt']; ?>" />
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-10 py-5 m-auto text-center">
                                       <?php echo $tab2['callout_content_1']; ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="scrollElementOffset carousel-item item col-12 col-md-3  p-0  lavender-bg">
                              <div class="d-flex" style='flex-direction: column'>
                                 <div class="row order-1 order-md-2">
                                    <div class="col-12">
                                       <img style="min-width:100%" src="<?php echo $tab2['callout_image_2_test']['url']; ?>" alt="<?php echo $tab2['callout_image_2_test']['alt']; ?>" />
                                    </div>
                                 </div>
                                 <div class="row order-2 order-md-1">
                                    <div class="col-10 py-5 m-auto text-center">
                                       <?php echo $tab2['callout_content_2']; ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="scrollElementOffsetThird carousel-item item col-12 col-md-3  p-0 lavender-bg">
                              <div class="d-flex" style='flex-direction: column'>
                                 <div class="row ">
                                    <div class="col-12">
                                       <img style="min-width:100%" src="<?php echo $tab2['callout_image_3']['url']; ?>" alt="<?php echo $tab2['callout_image_3']['alt']; ?>" />
                                    </div>
                                 </div>
                                 <div class="row ">
                                    <div class="col-10 py-5 m-auto text-center">
                                       <?php echo $tab2['callout_content_3']; ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="scrollElementOffsetLast carousel-item item col-12 col-md-3  p-0 lavender-bg">
                              <div class="d-flex" style='flex-direction: column'>
                                 <div class="row order-1 order-md-2">
                                    <div class="col-12">
                                       <img style="min-width:100%" src="<?php echo $tab2['callout_image_4']['url']; ?>" alt="<?php echo $tab2['callout_image_4']['alt']; ?>" />
                                    </div>
                                 </div>
                                 <div class="row order-2 order-md-1">
                                    <div class="col-10 py-5 m-auto text-center">
                                       <?php echo $tab2['callout_content_4']; ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <a class="carousel-control-prev d-flex d-md-none" href="#alumni-carousel1" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next d-flex d-md-none" href="#alumni-carousel1" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         <?php endif; ?>
      </div>
   </div>
   <div class="wrapper row white-strip mt-5 justify-content-center align-items-center">
      <div class="scrollElement col-10 col-md-9  m-auto text-center">
         <h2 class="mb-5"><?php the_field('student_life_title'); ?></h2>
      </div>
   </div>
   <div class="wrapper row left_img_right_text">
      <div class=" col-md-6 col-12 p-0  d-flex justify-content-center align-items-center">
         <div class="overlay"></div>
         <img class="img-fluid" src="<?php echo get_field('campus_right_left_image')['url']; ?>" alt="<?php echo get_field('campus_right_left_image')['alt']; ?>" />
      </div>
      <div class=" col-md-6 col-12  py-5  d-flex justify-content-center align-items-center blue-bg">
         <div class="scrollElement col-9 col-sm-11  m-auto  text-center white-text">
            <?php echo get_field('campus_right_left_content') ?>
         </div>
      </div>
   </div>
   <div class="wrapper row">
      <?php if (have_rows('campus_accordions_repeater')) : $accordion++; ?>
         <?php while (have_rows('campus_accordions_repeater')) : the_row(); ?>
            <div class=" col-12 p-0 accordion">
               <h6 class="accordion_title_black_text"><?php the_sub_field('title'); ?></h6>
               <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
                  <?php if (get_sub_field('number_acc_columns') == 'Single column') : ?>
                     <div class="row ">
                        <div class="col-12">
                           <?php the_sub_field('content_left'); ?>
                        </div>
                     </div>
                  <?php else : ?>
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <?php the_sub_field('content_left'); ?>
                        </div>
                        <div class="col-12 col-md-6">
                           <?php the_sub_field('content_right'); ?>
                        </div>
                     </div>
                  <?php endif; ?>
               </div>
            </div>
         <?php endwhile; ?>
      <?php endif; ?>
   </div>
   <div class="wrapper row my-5">
      <div class="scrollElement col-12 text-center">
         <h2>#GlionSpirit</h2>
         <p class="follow-us">FOLLOW US</p>
         <div class="row social_view">
            <div class="col-12">
               <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" /></a>
               <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/youtube.png" /></a>
               <a target="_blank" href="https://www.facebook.com/glionswitzerland/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" /></a>
               <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram.png" /></a>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row my-5">
      <div class=" col-12 p-0">
         <div>
            <div id="curator-feed-default-layout" class="">
               <?php the_field('social_feed_code') ?>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row hero  <?php if (get_field('hide_open_day_section')) {
                                    echo ' d-none';
                                 } ?>">
      <div class="background_image">
         <img src="<?php echo get_field('open_day_image')['url']; ?>" alt="<?php echo get_field('open_day_image')['alt']; ?>" />
      </div>
      <div class="scrollElement foreground_text col-md-9 col-11 m-auto about-us">
         <h2>
            <?php the_field('open_day_intro') ?>
            <br />
            <?php the_field('open_day_date') ?>
         </h2>
         <p class="text-center">
            <a class="mt-5  btn btn-primary bronze" href="<?php echo get_field('open_day_button')['url'] ?>"><?php echo get_field('open_day_button')['title'] ?></a>
         </p>
      </div>
   </div>
   <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
      <div class="wrapper row my-5">
         <div class="scrollElement col-12 text-center">
            <h2>News from campus</h2>
         </div>
      </div>
      <div class="wrapper row mb-5">
         <div class="splide">
            <div class="splide__track">
               <ul class="splide__list">
                  <?php
                  $category_select = get_field('category_select');
                  // $tag_select = get_field('tag_select');
                  $category_name = get_cat_name($category_select[0]);
                  // echo  $category_name;
                  // var_dump($category_select);

                  // $category_selected = echo $featured_post[0]

                  $args = array(
                     // 'category__not_in' => array( 52, 54 ),
                     // 'category__in' => array( $category_id ),
                     'category__in' => $category_select,
                     // 'tag_id' => $tag_select,
                     'posts_per_page' => 5,

                  );
                  $query = new WP_Query($args);
                  $max_num_pages = $query->max_num_pages;
                  if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <li class="splide__slide">
                           <div class="overlay"> </div>
                           <div class="image">
                              <?php the_post_thumbnail(); ?>
                           </div>
                           <div class="title">
                              <p class="text-center"><?php echo $category_name; ?></p>
                              <h6 class="text-center"><?php the_title(); ?></h6>
                              <div class="learn_more_container mx-auto pt-5 text-center">
                                 <a class="learn_more white" href="<?php the_permalink() ?>">Learn More </a>
                              </div>
                           </div>
                        </li>
                  <?php endwhile;
                  endif;
                  wp_reset_postdata();
                  ?>
               </ul>
            </div>
         </div>
      </div>
   <?php endif;  ?>
   <div id='DownloadBrochure' class=" row mt-1 blue-bg  py-5">
      <div class="  col-md-5 col-12 p-5">
         <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
            <h2 class="text-center text-md-left">Download <br>
               a brochure
            </h2>
         <?php else : ?>
            <h2 class="text-center text-md-left">
               Télécharger <br> une brochure
            </h2>
         <?php endif; ?>
      </div>
      <div class=" col-md-7 col-12 p-5">
         <?php the_field('form_code'); ?>
      </div>
   </div>
   <div class="wrapper row my-5">
      <div id="campus-carousel" class="scrollElement carousel slide mb-5 col-12" data-ride="carousel" data-interval="false">
         <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <div class="carousel-item col-12 col-md-6 campus-rollover p-0 px-md-3 active <?php if ((get_field('campus_hero_title') == "Suisse") || (get_field('campus_hero_title') == "Switzerland")) {
                                                                                             echo "col-md-12 px-0";
                                                                                          }
                                                                                          elseif ((get_field('campus_hero_title') == "London") || (get_field('campus_hero_title') == "Londres")) {
                                                                                             echo "d-none";
                                                                                          }
                                                                                          else{
                                                                                             echo "px-md-3";
                                                                                          }  ?>">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <?php if ((get_field('campus_hero_title') == "Suisse") || (get_field('campus_hero_title') == "Switzerland")) : ?><img class="img-fluid" src="<?php echo get_field('campus_2_image_wide', 23)['url']; ?>" alt="<?php echo get_field('campus_2_image_wide', 23)['alt']; ?>" /><?php else : ?><img class="img-fluid" src="<?php echo get_field('campus_2_image', 23)['url']; ?>" alt="<?php echo get_field('campus_2_image', 23)['alt']; ?>" /><?php endif; ?>
                  </div>
                  <div class="absolute col-9 m-auto text-center">
                     <h2><?php the_field('campus_text') ?></h2>
                  </div>
                  <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                     <button class="btn btn-primary blue btnAdjust"><?php echo get_field('campus_2_link_homee', 23)['title'] ?></button>
                     <a href="/locations/london/"></a>
                  <?php else : ?>
                     <button class="btn btn-primary blue btnAdjust">EN SAVOIR PLUS</button>
                     <a href="/fr/lieux/londres/"></a>
                  <?php endif; ?>
               </div>
            </div>
            <div class="carousel-item col-12 col-md-6 campus-rollover p-0 px-md-3 <?php if ((get_field('campus_hero_title') == "London") || (get_field('campus_hero_title') == "Londres")) {
                                                                                       echo "col-md-12 d-block px-0";
                                                                                    }
                                                                                    elseif ((get_field('campus_hero_title') == "Suisse") || (get_field('campus_hero_title') == "Switzerland")) {
                                                                                       echo "d-none col-md-12";
                                                                                    }
                                                                                    else{
                                                                                       echo "px-md-3";
                                                                                    }   ?>">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <?php if ((get_field('campus_hero_title') == "London") || (get_field('campus_hero_title') == "Londres")) : ?><img class="img-fluid" src="<?php echo get_field('campus_1_image_wide', 23)['url']; ?>" alt="<?php echo get_field('campus_1_image_wide', 23)['alt']; ?>" /><?php else : ?><img class="img-fluid" src="<?php echo get_field('campus_1_image', 23)['url']; ?>" alt="<?php echo get_field('campus_1_image', 23)['alt']; ?>" /><?php endif; ?>
                  </div>
                  <div class="absolute col-9 m-auto  text-center">
                     <h2><?php the_field('campus_text') ?></h2>
                  </div>
                  <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                     <button class="btn btn-primary blue btnAdjust"><?php echo get_field('campus_1_link_homee', 23)['title'] ?></button>
                     <a href="/locations/switzerland/"></a>
                  <?php endif; ?>
                  <?php if (ICL_LANGUAGE_CODE == 'fr') : ?>
                     <button class="btn btn-primary blue btnAdjust">EN SAVOIR PLUS</button>
                     <a href="/fr/lieux/suisse/"></a>
                  <?php endif; ?>
               </div>
            </div>
         </div>

      </div>
   </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
<script>
   document.addEventListener('DOMContentLoaded', function() {
      // new Splide( '.splide' ).mount();
      if ($(window).width() >= 992) {
         new Splide('.splide', {
            // rewind : true,

            perPage: 3.5,
            perMove: 1,
            width: '100%',
            type: 'loop',
            //  autoplay: true,
            gap: '1rem',
            pagination: false,
            isNavigation: false,
            arrows: false,
            heightRatio: 0.3,
         }).mount();
      }
      if ($(window).width() >= 768 && $(window).width() <= 991) {
         new Splide('.splide', {
            // rewind : true,

            perPage: 2.5,
            perMove: 1,
            width: '100%',
            type: 'loop',
            //  autoplay: true,
            gap: '1rem',
            pagination: false,
            isNavigation: false,
            arrows: false,
            heightRatio: 0.4,
         }).mount();
      }
      if ($(window).width() <= 767) {
         new Splide('.splide', {
            // rewind : true,

            perPage: 1.2,
            perMove: 1,
            width: '100%',
            type: 'loop',
            //  autoplay: true,
            gap: '1rem',
            pagination: false,
            isNavigation: false,
            arrows: false,
            heightRatio: 0.6,
         }).mount();
      }
   });
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer();
