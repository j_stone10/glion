    <!-- Template for the all main Program pages  -->
<?php
/*
   Template Name: Program Individual
   */

get_header(); ?>
<!-- <div class="fixed-ctas d-sm-block">
   <?php // wp_nav_menu( array( 'theme_location' => 'header actions' ) ); 
    ?>
   <div class="d-block d-sm-none">
      <div class="fixed_open">
         <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" />
      </div>
   </div>
   </div> -->
<div class="container-fluid">
    <!-- <div id='overlay-slide'></div> -->
    <div class="row" style="background: var(--grey);">
        <div class="col-md-6 col-12 pr-md-0 program-content ind order-2 order-md-1 <?php if (get_field('lavender_background')) {
                                                                                        echo "lavender-bg";
                                                                                    } ?>">
            <div class="breadcrumb-container d-none d-md-block">
                <?php the_breadcrumb() ?>
            </div>
            <div class="wrapper row program-text individual mr-md-0">
                <div class="scrollElementStart col-xl-8 col-lg-11 col-11 m-auto p-md-3">
                    <h1><?php the_title(); ?> </h1>
                    <?php the_field('program_overview'); ?>
                    <div class="row <?php if (the_field('show_remote_learning') === null) {
                                        echo 'd-none';
                                    } else {
                                        echo 'd-block';
                                    } ?>">
                        <div class="d-inline pl-3">
                            <p class="remote_learning_text remote_learning_text_program"><?php the_field('remote_learning_text'); ?><a href="#remote_learning_option"><?php the_field('learn_more_button_text') ?></a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-inline pl-3">
                            <a class="mt-5  btn btn-primary beige" href="<?php echo get_field('program_overview_button_1')['url'] ?>"><?php echo get_field('program_overview_button_1')['title'] ?></a>
                        </div>
                        <div class="d-inline pl-3"><a class="mt-5 btn btn-primary bronze" href="#DownloadBrochure"><?php echo get_field('program_overview_button_2')['title'] ?></a> </div>
                    </div>
                    <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                        <div class="row">
                            <div class="d-inline pl-3"><a target="_blank" class="mt-0 btn btn-primary blue" href="https://api.whatsapp.com/send?phone=41219892677"><svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill="#fff" d="M11.0027 0H10.9973C4.93213 0 0 4.72563 0 10.5365C0 12.8414 0.7755 14.9777 2.09413 16.7123L0.72325 20.6266L4.95138 19.3319C6.69075 20.4356 8.76562 21.0731 11.0027 21.0731C17.0679 21.0731 22 16.3461 22 10.5365C22 4.72695 17.0679 0 11.0027 0ZM17.4034 14.8789C17.138 15.5967 16.0847 16.192 15.2446 16.3659C14.6699 16.4831 13.9191 16.5766 11.3919 15.573C8.15925 14.2902 6.0775 11.1437 5.91525 10.9396C5.75988 10.7354 4.609 9.27346 4.609 7.76147C4.609 6.24948 5.41062 5.51324 5.73375 5.19714C5.99913 4.93768 6.43775 4.81915 6.8585 4.81915C6.99463 4.81915 7.117 4.82573 7.227 4.831C7.55013 4.84417 7.71237 4.86261 7.9255 5.35124C8.19088 5.96368 8.83712 7.47567 8.91412 7.63108C8.9925 7.7865 9.07088 7.99723 8.96088 8.20137C8.85775 8.4121 8.767 8.50561 8.60475 8.68473C8.4425 8.86386 8.2885 9.00083 8.12625 9.19312C7.97775 9.36039 7.81 9.53951 7.997 9.84902C8.184 10.1519 8.83025 11.1621 9.78175 11.9734C11.0096 13.0205 12.0051 13.3551 12.3612 13.4973C12.6266 13.6027 12.9429 13.5776 13.1368 13.3801C13.3829 13.1259 13.6867 12.7044 13.9961 12.2895C14.2161 11.9919 14.4939 11.955 14.7854 12.0604C15.0824 12.1592 16.654 12.9033 16.9771 13.0574C17.3003 13.2128 17.5134 13.2866 17.5917 13.417C17.6687 13.5473 17.6687 14.1598 17.4034 14.8789Z" fill="#243646" />
                                    </svg>
                                    &nbsp; Chat on WhatsApp</a> </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <p class="pl-3 intake mt-1"><?php the_field('next_intake'); ?></p>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a href="">
                                <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                    <img class="social_share_toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/share.svg" />
                                <?php else : ?>
                                    <img class="social_share_toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Share_Button_FR.svg" />
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="col-9  social_view pt-1">
                            <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            echo "<a  data-toggle='tooltip; data-placement='top' title='Share on Wattsap' target='_blank' href='https://api.whatsapp.com/send?text=" . urlencode($actual_link) . "' data-action='share/whatsapp/share'>"; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/email.svg" /></a>
                            <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" class='social-share facebook' href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/messenger.svg" /></a>
                            <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" class='social-share copy' href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/whatson.svg" /></a>
                            <a data-toggle="tooltip" data-placement="top" class='social-share email' title="Share via Email" href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mail.svg" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12  p-0 order-1 order-md-2 image fill">
            <img src="<?php echo get_field('program_header_image')['url']; ?>" alt="<?php echo get_field('program_header_image')['alt']; ?>" />
        </div>
    </div>
    <div class="row ">
        <div class="pr-4 pt-3 m-auto m-md-0 ml-md-auto">
            <?php if (ICL_LANGUAGE_CODE == 'en') : ?> Accredited by: <?php else : ?> Accréditation: <?php endif; ?> <span class="d-block d-md-none">&nbsp; &nbsp;</span> <br class="d-block d-md-none" /> <br class="d-block d-md-none" /> <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAgAGcDAREAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+Pj9nf/gqb/wUZ/ZQezT9n/8AbR/aG+H+k2DQtbeEo/iNrniTwAptzIYRL8OvGE/iDwJcqnmyDZc+HZkKuyspU4oA/rH+FX/BxB/wU91r/gh9+0f+11rnxO+HesftF/DP9sz4Tfs++FviTc/B/wAC2skHgnxx8P7vxRrF9eeEdK03TvAN54ki1G0VLC+k8KDT4bWWWKbSbmTypogD+WL9on/gsH/wU/8A2rFvLb45ftxftCeJ9G1Hd9v8I6B43uvhp4BvN0csR+0/Dz4XR+DPA037qaaJfM8PNtilljXCSMpAP9D7/gqt408XeAP+DV3QvH/gfxPr3hHxx4R/Ze/4JfeKPC3i/wAOare6P4k8PeJNH+NP7IepaVrujazYTQX+natp1/BDeWd/azx3MFzGk0ciuoNAH3P/AMEjv+CknxA/aTtvHX7Gn7a/h21+E/8AwUn/AGTtN0PS/jh4Kmk0+10341+DrjT9PPhn9pj4VQ2kVlZap4N8fWV7peqa/BoNlHpfh3V9b0+W1ttM0LxH4esYgD4n/Yv1/XZ/+Cl//B0tZz61q01poGg/sbvoNrLqN5Jb6I9x+zD8fridtIgeYxaa088MM8zWawmSaKORyzorAA+N/wDgjl/wTc/4JbftO/8ABNP9lX47ftReOPFmqfHv4g+E/Fl/8R7/AFD9vj9oTwFe3OqaX8TfG/h7T2uPCOh/Hnw/pWiSLoWkaVGbez0awjmCC7aJpbh5ZAD7w+PPiH4wftO/tYfD/wD4Iif8E+/jT46/Zd/Zk/ZS/Zy+GPjj9tb9p74f+K9S8T/G7Tvhp4j0PSNO+Bn7OPwf+KPiXUvEXiLRvH3j7wS9l4y1f4q6pqN/4huNBmGtW+s3v9h6z4a8eAHtkv8AwbTf8Eq7O2XWvBngj9ob4a/GyNZLqD9p3wL+11+0fY/H6DxHLBsbxgniTWPiLrnhR/EQvQmrxGfwTNo0Gpq8sOjx291e210AfKni258Zfsl/8Fn/APgkh8J/2hf2s9a+JFh4S/YZ/a70bxj8Z/i14i0zwHF8Sp7TXdbTwNrXxCt5dVtfDGoeNrbQJNE0e81i5eS81vWrC41dSLu/kUAH66ft2/tWfs5XH7EH7ZMHhv8AaS+Cc/iKf9lT9oaHQIdD+MPgWTWptak+EXi9NKi0eOw8RNfSapJfNAmnpZA3bXZhW2BmKCgD+aP9qPVLnxZ/wb3f8EE9P8b+NPFVrofxP/bC/YB8GfFfX4fHviXwnrGt+BPGXh74waZ43ttZ8caPrWk+ILK1v9HmuWv9Uj1q0ubUxpqEd5b3FrDcRAH7M+Av+CNX/BGu28WaVP8AD/W/GGpeLk+3f2RZaX/wUL/aW8S3827TbxL/AMjRZf2gtTS98vTGvZZd1jP9mhjkux5ZgE0YB/j60Afv78Gv+Va/9s//ALSf/s6f+qb1WgD8AqAP9Tv/AILC/wDKpdL/ANmgf8E0P/VufskUAfoP/wAFMv8Agm54y/ag8I/A39rz9jjxBo/we/4KXfsi6Xovin9nL4szLHY2HxC0K00+a41/9nT4sXKvDDrPw0+INrqOsaZBBrhuNN0O/wBc1SKY23hzxT4yg1EA/GD/AIIe/tGeMP2sv2g/+Dkb4+fEX4Ua58C/iR418B/slWPxL+EHiOO6i1f4efErwN8CP2ovh58QvDE8OoQ22pRW+n+MvCmtpp0WqW1tqsWmtaJqlvBqC3MSAH0N/wAEIf8Agkp/wTT/AGhv+CS37GXxk+N37FPwC+JvxT8ceCfGl74v8deLPBNpqPiHxDeaf8W/iDollc6pfM6tczW+k6ZYWEcjLu8i1iUklSSAekalqfgD/gjd/wAFn/iV8XPixpdr8Lv2Cv8Agpb8Bv2aPg/4M+Nj2gtfhX+z78f/ANkr4exfCf4efBnxvrCwvH4F8K+LPhZoEd54c1zWLqDS7nVLiOOWX+yfDXibVfDgB/SjrnxL+HPhnwTc/EvxJ4/8E+H/AIcWWmjWbz4ga54q0LSfBNppDAEarc+K7+/t9Cg00ggi+lv0tSCCJcUAfysftW6N+xx/wU4/4LYf8EprvW/D3w//AGov2XPif+xX+2Dr2hR+I9FudT8EeMJPBPirXNMsvEFhZ6rbWM15Z2niHQru40TVUhFpqEEUGqadNdafd21xMAfb/wC27/wRX/4JQ+BP2L/2u/HHg79gj9nDw34u8G/swfH3xX4V8RaT4DtbTVdB8SeHvhT4s1fQ9a0y6SXfbahpep2drfWdwnzQ3MEci8qKAPxg/aN8KeG/Hf8Awb3/APBvH4H8Y6Lp/iTwj4y/bf8A+CcfhTxV4d1a3W70rXvDfiHSPi7pGuaLqdq/yXOn6ppl5dWN5bv8s1tPJG3DGgD+n74W/wDBIH/gmL8EvHehfE/4SfsQ/AD4e/ELwz/af/CP+MPC3gu30vXdI/trR9Q8Par9hvoZhLB/aGiatqWl3W0/vbO9uIT8shoA/wASagD9/fg1/wAq1/7Z/wD2k/8A2dP/AFTeq0AfgFQB/tLfD74YfsyfHD/gkX+yH8FP2wPC2n+NfgF8UP2WP2PNE8YeGdYk8V2uh39zoHw9+G/j/wAO3mvar4Nu9P1jQND8P+IPB2m+JtW8RTalpmg6FYaNPqfifUbPw5bapOgB97eD/j/8CvE82k+HvCnxM8F3Wp3V5B4e0jw3HrVnDrU+pQ6fcXT6PY6RPKl/d3mmW2m6jDqkFrDOdJu9H1iw1Fre90nUYLYA+btEtP2D9A8e/tR/HK1s/BPgzxz+0KujfBT9pfxNLqt3pNz8Th+z9rvxY+Efh621/SdO1aS2t9e0q81f4p+E9P1yPTdH8beKvDfh2SaafVfC3gbQrzRgD8xLL/gix/wbuXK6JHd/sh/C3RdT1/wrpnjSy0LW/Hvx5sNZTw7q/h/RfEdlqM1ofiVIhtZrTxDpGnxXlvNcWN34ju08L2N1deIQ2mgA/T/wF8Pf2H4P2W/B37Lmh/Brwjqv7J9j8Hvhzf2nwg8deFovG/gbQ/h18UptUufht4e1/wAP+P7nxFqfiDWvEmr2t5ZaV4dt4fFHiO31qXTUngtDqWizXYB+ZWk/8ET/APg3i8PeNLLxpY/s2/CzUr1fGlloGleFdQ+Lnx58bfDm/wDHlxvltdN0v4U6t8Std+H+v6heWzW//EntfDOo6VNY6hpcw0/y/ENlNqYB+jK6J/wTmsPin8JP2gdM0v4O6T8Sv2e/Avin4P8Awi8R+E9b0/SNF+Fnwu1rVPDuh+KtC0jRfC+u2vw20PwzHbeLfCt9dwX2npqWiaL4i0SOG20+61Kz06UA+rb7W/gt+0LoPxV+C97qWj+O9F1Dw3rXw/8Aix4LE+pWc6aF4y0nUfD/AIh8Ma15J07UtOvLjTbi/wBL1e0t7i11nRJphDef2bfPACAfPXxR/wCCZn7C/wAaf2X/AIYfsYfE/wDZ88O+K/2YvgzfeG9S+GfwmufEXjux0fwrfeEdF17w74cuLbVtJ8Vaf4nvW0zRvE+vWca6rrl/HONRknuknuYreaEA+Lv+Ib3/AIIlf9GEfD//AMOD8cP/AJ6FAHxX/wAFRP8Ag1Z/Yd/bgl8SfFT9nGGz/Yz/AGi9Sje7e7+H2g2K/Abxtq5YyTXXjb4S6fb2UGh6pqB4ufEnw5vfDTy3c11rXiHQPFuqTySOAfht4U/4IB/8FNfAf/BJT9qT/gn1d/Bzw7rvxv8AHn/BRX4EfEXwRrnh74heF5/hb4m+FOhfC7XNI134oW3jLVbvSJtJ8M6PfW00F/pXibRtC8dpMbO1i8HTXur6NbaiAfq1/wAEuf8Ag0b/AGUf2X/+ET+LX7deraX+138dNNkt9WX4bx2s8H7MXhDU4sPFav4b1Wytdd+MclnKCzXvj2DSfCGoxyCC7+GRe2S+nAP6qfHt9Y/DzwhoSeH/AIV3HjHS9C83TNI8K+EtEs3j8N6Vpng3xCttHpOj2tlP9kt7nT7IeBNHstLsFtluvE2nadfy6T4bm1bVNPAPluTWvGPh27+ENzbfsleDxbzeF/CvibV9X0PwRBHcfCvxB4u8H/F3xj4o0jTLV7Gyube80jxT8MvhFoGr3CTaTImpeJ9Pu9T8q4GhwwAHkWg+NdC+OHge3v7z9iBp/BPxK+J3iDX7WXwjoiaXrVtc+CIn8c33ivxhqa+D9MD+IvE/xC8U+PbDTPEVjf2M994j13xnbwatBq0d14n8RAHd/EO31fwzqnijTfCn7E3w18Vrd2Pwd8P2dva/C+zksL7QfETS6r8QdO1bxOuh2+l6xpvhH+yY9J0LSRp+n2EGvXmk6p4nn0HSYPMmAO21X4gLH4HuNf0v9jLWNUg8G3GmeDvhv4Lu/B0umave6F4ZY3XhnUNP0A+AbyLwV4P0e7h03/hHILmJNZ0R49Wmt/Demi10r/hIgDQ8P6jYTeJdK04/sZW+lS2fjDwt4ibxQ3g/w1p1jpGq+Ib/AMLW154xsJhoEs0nijQJtSOq6hc2Fy1xHo/hm7vdQ1vSdbtv+EdtAD6Gf4G/BSSwj0p/g/8AC19LhmFxFpr/AA/8JtYRXC+ftnjszpBt0mH2q6xKsYcfaZ8N++k3AHVaB4I8F+Fbm/vPC/hDwv4bvNVkM2qXWgaBpOj3OpSsVJlv59OtLeW8kJRCXuGkYlFyflGADqKACgD/2Q==" />
        </div>
    </div>
    <div class="wrapper row white-strip mt-3 mb-5 justify-content-center align-items-center">
        <div class="scrollElement col-10 col-md-7 p-5 m-auto text-center">
            <h3 class="mb-5"><?php the_field('intro_title'); ?></h3>
            <p class="mb-5"><?php the_field('intro_text'); ?></p>
            <div class="keypoints mt-5">
                <?php the_field('keypoints'); ?></p>
            </div>
        </div>
    </div>
    <!--*************************m
      ********* DESKTOP SHAPES *****
      *****************************-->
    <div class="d-none d-md-block">
        <?php $shapes_options = get_field('semester_shapes')['semester_shapes_options'] ?>
        <?php if ($shapes_options == 'Option 1') : ?>
            <?php $block1 = get_field('semester_shapes')['shapes_option_1']['option_1_block_1']; ?>
            <?php $block2 = get_field('semester_shapes')['shapes_option_1']['option_1_block_2']; ?>
            <?php $block3 = get_field('semester_shapes')['shapes_option_1']['option_1_block_3']; ?>
            <?php $block4 = get_field('semester_shapes')['shapes_option_1']['option_1_block_4']; ?>
            <?php $block5 = get_field('semester_shapes')['shapes_option_1']['option_1_block_5']; ?>
            <?php $block6 = get_field('semester_shapes')['shapes_option_1']['option_1_block_6']; ?>
            <?php $block7 = get_field('semester_shapes')['shapes_option_1']['option_1_block_7']; ?>
            <?php $block8 = get_field('semester_shapes')['shapes_option_1']['option_1_block_8']; ?>
            <div class="row courses-arrows mb-1 ">
                <div class="arrow">
                    <div class="inner-arrow <?php echo $block1['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block1['title'] ?></div>
                                <div class="semester_title"><?php echo $block1['text'] ?>
                                </div>
                                <div class="semester_signoff"><?php echo $block1['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow">
                    <div class="inner-arrow <?php echo $block2['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block2['title'] ?></div>
                                <div class="semester_title"><?php echo $block2['text'] ?></div>
                                <div class="semester_signoff"><?php echo $block2['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow ">
                    <div class="inner-arrow <?php echo $block3['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block3['title'] ?></div>
                                <div class="semester_title"><?php echo $block3['text'] ?>
                                </div>
                                <div class="semester_signoff"><?php echo $block3['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow ">
                    <div class="inner-arrow <?php echo $block4['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block4['title'] ?></div>
                                <div class="semester_title"><?php echo $block4['text'] ?>
                                </div>
                                <div class="semester_signoff"><?php echo $block4['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow">
                    <div class="inner-arrow <?php echo $block5['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block5['title'] ?></div>
                                <div class="semester_title"><?php echo $block5['text'] ?>
                                </div>
                                <div class="semester_signoff"><?php echo $block5['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow">
                    <div class="inner-arrow <?php echo $block6['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block6['title'] ?></div>
                                <div class="semester_title"><?php echo $block6['text'] ?>
                                </div>
                                <div class="semester_signoff"><?php echo $block6['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow">
                    <div class="inner-arrow <?php echo $block7['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block7['title'] ?></div>
                                <div class="semester_title"><?php echo $block7['text'] ?>
                                </div>
                                <div class="semester_signoff"><?php echo $block7['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow ">
                    <div class="inner-arrow <?php echo $block8['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block8['title'] ?></div>
                                <div class="semester_title"><?php echo $block8['text'] ?> </div>
                                <div class="semester_signoff"><?php echo $block8['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (get_field('semester_shapes')['asterisk_content_1']) : ?>
            <div class="row mb-1 ">
                <div class="col-md-12 col-10 m-auto">
                    <div class="semester_signoff_asterisk"> <?php echo get_field('semester_shapes')['asterisk_content_1'] ?></div>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($shapes_options == 'Option 2') : ?>
            <?php $block1 = get_field('semester_shapes')['shapes_option_2']['option_2_block_1']; ?>
            <?php $block2 = get_field('semester_shapes')['shapes_option_2']['option_2_block_2']; ?>
            <?php $block3 = get_field('semester_shapes')['shapes_option_2']['option_2_block_3']; ?>
            <?php $block4 = get_field('semester_shapes')['shapes_option_2']['option_2_block_4']; ?>
            <?php $block5 = get_field('semester_shapes')['shapes_option_2']['option_2_block_5']; ?>
            <!--*************************
         ********* Start desktop wide SHAPES *****
         *****************************-->
            <div class="row mt-5 mb-1 courses-arrows wide-children">
                <div class="arrow">
                    <div class="inner-arrow <?php echo $block1['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block1['title'] ?></div>
                                <div class="semester_title"><?php echo $block1['text'] ?> </div>
                                <div class="semester_signoff"><?php echo $block1['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow wide">
                    <div class="inner-arrow <?php echo $block2['colour'] ?>">
                        <?php if ($block2['is_split'] == 'No') : ?>
                            <div class="content ">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block2['title'] ?></div>
                                    <div class="semester_title"><?php echo $block2['text'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block2['signoff'] ?></div>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="content item">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block2['title_1'] ?></div>
                                    <div class="semester_title"><?php echo $block2['text_1'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block2['signoff_1'] ?></div>
                                </div>
                            </div>
                            <div class="content item">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block2['title_2'] ?></div>
                                    <div class="semester_title"><?php echo $block2['text_2'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block2['signoff_2'] ?></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="arrow wide">
                    <div class="inner-arrow <?php echo $block3['colour'] ?>">
                        <?php if ($block3['is_split'] == 'No') : ?>
                            <div class="content ">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block3['title'] ?></div>
                                    <div class="semester_title"><?php echo $block3['text'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block3['signoff'] ?></div>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="content item">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block3['title_1'] ?></div>
                                    <div class="semester_title"><?php echo $block3['text_1'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block3['signoff_1'] ?></div>
                                </div>
                            </div>
                            <div class="content item">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block3['title_2'] ?></div>
                                    <div class="semester_title"><?php echo $block3['text_2'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block3['signoff_2'] ?></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="arrow  wide ">
                    <div class="inner-arrow <?php echo $block4['colour'] ?>">
                        <?php if ($block4['is_split'] == 'No') : ?>
                            <div class="content ">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block4['title'] ?></div>
                                    <div class="semester_title"><?php echo $block4['text'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block4['signoff'] ?></div>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="content item">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block4['title_1'] ?></div>
                                    <div class="semester_title"><?php echo $block4['text_1'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block4['signoff_1'] ?></div>
                                </div>
                            </div>
                            <div class="content item">
                                <div class="text">
                                    <div class="semester_num"> <?php echo $block4['title_2'] ?></div>
                                    <div class="semester_title"><?php echo $block4['text_2'] ?> </div>
                                    <div class="semester_signoff"><?php echo $block4['signoff_2'] ?></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="arrow ">
                    <div class="inner-arrow <?php echo $block5['colour'] ?>">
                        <div class="content">
                            <div class="text">
                                <div class="semester_num"> <?php echo $block5['title'] ?></div>
                                <div class="semester_title"><?php echo $block5['text'] ?> </div>
                                <div class="semester_signoff"><?php echo $block5['signoff'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (get_field('semester_shapes')['asterisk_content_2']) : ?>
            <div class="row mb-1 ">
                <div class="col-md-12 col-10 m-auto">
                    <div class="semester_signoff_asterisk"> <?php echo get_field('semester_shapes')['asterisk_content_2'] ?></div>
                </div>
            </div>
        <?php endif; ?>
        <!--*************************
         ********* End desktop wide SHAPES *****
         *****************************-->
        <div class=" row program-structure">
            <?php $content1 = get_field('tab_1_content'); ?>
            <?php $content2 = get_field('tab_2_content'); ?>
            <?php $content3 = get_field('tab_3_content'); ?>
            <?php $content4 = get_field('tab_4_content'); ?>
            <?php $content5 = get_field('tab_5_content'); ?>
            <?php $content6 = get_field('tab_fees_content_1');
            // var_dump($content6);
            $feeItems1 = get_field('tab_fees_content_1')['table_row_1_items'];
            $feeItems2 = get_field('tab_fees_content_1')['table_row_2_items'];
            ?>
            <?php $content7 = get_field('tab_fees_content_22');
            // var_dump($content7);
            $feeItems3 = get_field('tab_fees_content_22')['table_row_111_items'];
            $feeItems4 = get_field('tab_fees_content_22')['table_row_12_items'];
            ?>
            <div class="col-md-4 col-12 px-0 grey-bg blue-text menu <?php if (get_field('lavender_background')) {
                                                                        echo "lavender-bg";
                                                                    } ?>">
                <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                    <h3 class=" d-none d-md-block">Program <br> Structure</h3>
                <?php else : ?>
                    <h3 class=" d-none d-md-block">Structure <br> du programme</h3>
                <?php endif; ?>
                <ul class=" structure_nav">
                    <?php if ($content1['title']) : ?>
                        <li data_target="0" activate_slide="slide1" class="active"><?php echo $content1['title'] ?></li>
                    <?php endif; ?>
                    <?php if ($content2['title']) : ?>
                        <li data_target="100" activate_slide="slide2"><?php echo $content2['title'] ?></li>
                    <?php endif; ?>
                    <?php if ($content3['title']) : ?>
                        <li data_target="200" activate_slide="slide3"><?php echo $content3['title'] ?></li>
                    <?php endif; ?>
                    <?php if ($content4['title']) : ?>
                        <li data_target="300" activate_slide="slide4"><?php echo $content4['title'] ?></li>
                    <?php endif; ?>
                    <?php if ($content5['title']) : ?>
                        <li data_target="400" activate_slide="slide5"><?php echo $content5['title'] ?></li>
                    <?php endif; ?>
                    <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                        <li data_target="500" activate_slide="slide6">FEES</li>
                    <?php else : ?>
                        <li data_target="500" activate_slide="slide6">FRAIS DE COURS</li>

                    <?php endif; ?>

                </ul>
            </div>
            <div class=" col-md-8 col-12 content">
                <div class="row">
                    <div class="col-lg-11 col-md-12 scrollbar m-auto">
                        <div class="row">
                            <div class="wrap">
                                <div id="slide1" class="content-inner slide active_slide">
                                    <?php if ($content1['stat_figure']) : ?>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="row counter">
                                                    <div class="col-12 font bronze-text text-center">
                                                        <?php echo $content1['stat_figure'] ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 black-text text-center">
                                                        <h5><?php echo $content1['stat_text'] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($content1['stat_figure_2']) : ?>
                                                <div class="col-md-6 col-12">
                                                    <div class="row counter">
                                                        <div class="col-12 font bronze-text text-center">
                                                            <?php echo $content1['stat_figure_2'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 black-text text-center">
                                                            <h5><?php echo $content1['stat_text_2'] ?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                <?php endif;
                                        endif; ?>
                                <div class="row">
                                    <?php echo $content1['text'] ?>
                                </div>
                                </div>
                                <div id="slide2" class="content-inner slide ">
                                    <?php if ($content2['stat_figure']) : ?>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="row counter">
                                                    <div class="col-12 font bronze-text text-center">
                                                        <?php echo $content2['stat_figure'] ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 black-text text-center">
                                                        <h5><?php echo $content2['stat_text'] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($content2['stat_figure_2']) : ?>
                                                <div class="col-md-6 col-12">
                                                    <div class="row counter">
                                                        <div class="col-12 font bronze-text text-center">
                                                            <?php echo $content2['stat_figure_2'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 black-text text-center">
                                                            <h5><?php echo $content2['stat_text_2'] ?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                <?php endif;
                                        endif; ?>
                                <div class="row">
                                    <?php echo $content2['text'] ?>
                                </div>
                                </div>
                                <div id="slide3" class="content-inner slide ">
                                    <?php if ($content3['stat_figure']) : ?>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="row counter">
                                                    <div class="col-12 font bronze-text text-center">
                                                        <?php echo $content3['stat_figure'] ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 black-text text-center">
                                                        <h5><?php echo $content3['stat_text'] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($content3['stat_figure_2']) : ?>
                                                <div class="col-md-6 col-12">
                                                    <div class="row counter">
                                                        <div class="col-12 font bronze-text text-center">
                                                            <?php echo $content3['stat_figure_2'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 black-text text-center">
                                                            <h5><?php echo $content3['stat_text_2'] ?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                <?php endif;
                                        endif; ?>
                                <div class="row">
                                    <?php echo $content3['text'] ?>
                                </div>
                                </div>
                                <div id="slide4" class="content-inner slide ">
                                    <?php if ($content4['stat_figure']) : ?>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="row counter">
                                                    <div class="col-12 font bronze-text text-center">
                                                        <?php echo $content4['stat_figure'] ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 black-text text-center">
                                                        <h5><?php echo $content4['stat_text'] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($content4['stat_figure_2']) : ?>
                                                <div class="col-md-6 col-12">
                                                    <div class="row counter">
                                                        <div class="col-12 font bronze-text text-center">
                                                            <?php echo $content4['stat_figure_2'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 black-text text-center">
                                                            <h5><?php echo $content4['stat_text_2'] ?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                <?php endif;
                                        endif; ?>
                                <div class="row">
                                    <?php echo $content4['text'] ?>
                                </div>
                                </div>
                                <div id="slide5" class="content-inner slide ">
                                    <?php if ($content5['stat_figure']) : ?>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="row counter">
                                                    <div class="col-12 font bronze-text text-center">
                                                        <?php echo $content5['stat_figure'] ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 black-text text-center">
                                                        <h5><?php echo $content5['stat_text'] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($content5['stat_figure_2']) : ?>
                                                <div class="col-md-6 col-12">
                                                    <div class="row counter">
                                                        <div class="col-12 font bronze-text text-center">
                                                            <?php echo $content5['stat_figure_2'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 black-text text-center">
                                                            <h5><?php echo $content5['stat_text_2'] ?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                <?php endif;
                                        endif; ?>
                                <div class="row">
                                    <?php echo $content5['text'] ?>
                                </div>
                                </div>
                                <div id="slide6" class="content-inner slide ">
                                    <div class="fees_table">
                                        <?php echo $content6['table_row_1_intro'] ?>
                                        <div class="row grey-bg py-2">
                                            <div class="col-10">
                                                <?php echo $content6['table_row_1_column_1_title'] ?>
                                            </div>
                                            <div class="col-2">
                                                <?php echo $content6['table_row_1_column_2_value'] ?>
                                            </div>
                                        </div>
                                        <?php foreach ($feeItems1 as $item) {
                                            echo '<div class="row py-3 opacity-bg"><div class="col-10">' . $item['product'] . '</div>';
                                            echo '<div class="col-2">' . $item['fee'] . '</div></div>';
                                        } ?>
                                        <div class="row grey-bg py-2">
                                            <div class="col-10">
                                                <?php echo $content6['table_row_1_sign_off_title'] ?>
                                            </div>
                                            <div class="col-2">
                                                <?php echo $content6['table_row_1_sign_off'] ?>
                                            </div>
                                        </div>
                                        <div class="row grey-bg mt-4 py-2">
                                            <div class="col-10">
                                                <?php echo $content6['table_row_2_column_1_title'] ?>
                                            </div>
                                            <div class="col-2">
                                                <?php echo $content6['table_row_2_column_2_value'] ?>
                                            </div>
                                        </div>
                                        <?php foreach ($feeItems2 as $item) {
                                            echo '<div class="row py-3 opacity-bg"><div class="col-10">' . $item['product'] . '</div>';
                                            echo '<div class="col-2">' . $item['fee'] . '</div></div>';
                                        } ?>
                                        <div class="row grey-bg py-2 ">
                                            <div class="col-10">
                                                <?php echo $content6['table_row_3_sign_off_title'] ?>
                                            </div>
                                            <div class="col-2">
                                                <?php echo $content6['table_row_3_column_2_value'] ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($content7['table_row_1_intro']) : ?>
                                        <div class="fees_table">
                                            <?php echo $content7['table_row_1_intro'] ?>
                                            <div class="row grey-bg py-2">
                                                <div class="col-10">
                                                    <?php echo $content7['table_row_1_column_1_title'] ?>
                                                </div>
                                                <div class="col-2">
                                                    <?php echo $content7['table_row_1_column_2_value'] ?>
                                                </div>
                                            </div>
                                            <?php foreach ($feeItems3 as $item) {
                                                echo '<div class="row py-3 opacity-bg"><div class="col-10">' . $item['product'] . '</div>';
                                                echo '<div class="col-2">' . $item['fee'] . '</div></div>';
                                            } ?>
                                            <div class="row grey-bg py-2">
                                                <div class="col-10">
                                                    <?php echo $content7['table_row_1_sign_off_title'] ?>
                                                </div>
                                                <div class="col-2">
                                                    <?php echo $content7['table_row_1_sign_off_value'] ?>
                                                </div>
                                            </div>
                                            <div class="row grey-bg mt-4 py-2">
                                                <div class="col-10">
                                                    <?php echo $content7['table_row_2_column_1_title'] ?>
                                                </div>
                                                <div class="col-2">
                                                    <?php echo $content7['table_row_2_column_2_value'] ?>
                                                </div>
                                            </div>
                                            <?php foreach ($feeItems4 as $item) {
                                                echo '<div class="row py-3 opacity-bg"><div class="col-10">' . $item['product'] . '</div>';
                                                echo '<div class="col-2">' . $item['fee'] . '</div></div>';
                                            } ?>
                                            <div class="row grey-bg py-2 ">
                                                <div class="col-10">
                                                    <?php echo $content7['table_row_3_sign_off_title'] ?>
                                                </div>
                                                <div class="col-2">
                                                    <?php echo $content7['table_row_3_column_2_value'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--*************************
      ********* MOBILE SHAPES *****
      *****************************-->
    <div class="d-block d-md-none program-structure">
        <div class="row">
            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                <h3 class='col text-center'>Program Structure</h3>
            <?php else : ?>
                <h3 class='col text-center'>Structure du programme</h3>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-12 p-0 accordion">
                <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                    <h6 class="accordion_title_black_text">Course overview</h6>
                <?php else : ?>
                    <h6 class="accordion_title_black_text">Aperçu du cours</h6>
                <?php endif; ?>
                <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                <div class="accordion_text">
                    <?php if ($shapes_options == 'Option 1') : ?>
                        <div class="row courses-arrows mb-1 ">
                            <div class="arrow">
                                <div class="inner-arrow <?php echo $block1['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block1['title'] ?></div>
                                            <div class="semester_title"><?php echo $block1['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block1['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow">
                                <div class="inner-arrow <?php echo $block2['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block2['title'] ?></div>
                                            <div class="semester_title"><?php echo $block2['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block2['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow ">
                                <div class="inner-arrow <?php echo $block3['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block3['title'] ?></div>
                                            <div class="semester_title"><?php echo $block3['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block3['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow ">
                                <div class="inner-arrow <?php echo $block4['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block4['title'] ?></div>
                                            <div class="semester_title"><?php echo $block4['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block4['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow">
                                <div class="inner-arrow <?php echo $block5['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block5['title'] ?></div>
                                            <div class="semester_title"><?php echo $block5['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block5['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow">
                                <div class="inner-arrow <?php echo $block6['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block6['title'] ?></div>
                                            <div class="semester_title"><?php echo $block6['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block6['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow">
                                <div class="inner-arrow <?php echo $block7['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block7['title'] ?></div>
                                            <div class="semester_title"><?php echo $block7['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block7['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow ">
                                <div class="inner-arrow <?php echo $block8['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block8['title'] ?></div>
                                            <div class="semester_title"><?php echo $block8['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block8['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (get_field('semester_shapes')['asterisk_content_1']) : ?>
                        <div class="row mb-1 ">
                            <div class="col-md-12 col-10 m-auto">
                                <div class="semester_signoff_asterisk"> <?php echo get_field('semester_shapes')['asterisk_content_1'] ?></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($shapes_options == 'Option 2') : ?>
                        <div class="row mt-5 mb-1 courses-arrows wide-children">
                            <div class="arrow">
                                <div class="inner-arrow <?php echo $block1['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block1['title'] ?></div>
                                            <div class="semester_title"><?php echo $block1['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block1['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="arrow wide">
                                <div class="inner-arrow <?php echo $block2['colour'] ?>">
                                    <?php if ($block2['is_split'] == 'No') : ?>
                                        <div class="content ">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block2['title'] ?></div>
                                                <div class="semester_title"><?php echo $block2['text'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block2['signoff'] ?></div>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div class="content item">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block2['title_1'] ?></div>
                                                <div class="semester_title"><?php echo $block2['text_1'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block2['signoff_1'] ?></div>
                                            </div>
                                        </div>
                                        <div class="content item">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block2['title_2'] ?></div>
                                                <div class="semester_title"><?php echo $block2['text_2'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block2['signoff_2'] ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="arrow wide">
                                <div class="inner-arrow <?php echo $block3['colour'] ?>">
                                    <?php if ($block3['is_split'] == 'No') : ?>
                                        <div class="content ">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block3['title'] ?></div>
                                                <div class="semester_title"><?php echo $block3['text'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block3['signoff'] ?></div>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div class="content item">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block3['title_1'] ?></div>
                                                <div class="semester_title"><?php echo $block3['text_1'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block3['signoff_1'] ?></div>
                                            </div>
                                        </div>
                                        <div class="content item">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block3['title_2'] ?></div>
                                                <div class="semester_title"><?php echo $block3['text_2'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block3['signoff_2'] ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="arrow  wide">
                                <div class="inner-arrow <?php echo $block4['colour'] ?>">
                                    <?php if ($block4['is_split'] == 'No') : ?>
                                        <div class="content ">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block4['title'] ?></div>
                                                <div class="semester_title"><?php echo $block4['text'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block4['signoff'] ?></div>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div class="content item">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block4['title_1'] ?></div>
                                                <div class="semester_title"><?php echo $block4['text_1'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block4['signoff_1'] ?></div>
                                            </div>
                                        </div>
                                        <div class="content item">
                                            <div class="text">
                                                <div class="semester_num"> <?php echo $block4['title_2'] ?></div>
                                                <div class="semester_title"><?php echo $block4['text_2'] ?> </div>
                                                <div class="semester_signoff"><?php echo $block4['signoff_2'] ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="arrow ">
                                <div class="inner-arrow <?php echo $block5['colour'] ?>">
                                    <div class="content">
                                        <div class="text">
                                            <div class="semester_num"> <?php echo $block5['title'] ?></div>
                                            <div class="semester_title"><?php echo $block5['text'] ?> </div>
                                            <div class="semester_signoff"><?php echo $block5['signoff'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;  ?>
                </div>
                <?php if (get_field('semester_shapes')['asterisk_content_2']) : ?>
                    <div class="row mb-1 ">
                        <div class="col-md-12 col-10 m-auto">
                            <div class="semester_signoff_asterisk"> <?php echo get_field('semester_shapes')['asterisk_content_2'] ?></div>
                        </div>
                    </div>
                <?php endif;  ?>
            </div>
            <?php if ($content1['title']) : ?>
                <div class="col-12 p-0 accordion">
                    <h6 class="accordion_title_black_text"><?php echo $content1['title'] ?></h6>
                    <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                    <div class="accordion_text">
                        <?php if ($content1['stat_figure']) : ?>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="row counter">
                                        <div class="col-12 font bronze-text text-center">
                                            <?php echo $content1['stat_figure'] ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 black-text text-center">
                                            <h5><?php echo $content1['stat_text'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($content1['stat_figure_2']) : ?>
                                    <div class="col-md-6 col-12">
                                        <div class="row counter">
                                            <div class="col-12 font bronze-text text-center">
                                                <?php echo $content1['stat_figure_2'] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 black-text text-center">
                                                <h5><?php echo $content1['stat_text_2'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    <?php endif;
                            endif; ?>
                    <div class="row">
                        <?php echo $content1['text'] ?>
                    </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($content2['title']) : ?>
                <div class="col-12 p-0 accordion">
                    <h6 class="accordion_title_black_text"><?php echo $content2['title'] ?></h6>
                    <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                    <div class="accordion_text">
                        <?php if ($content2['stat_figure']) : ?>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="row counter">
                                        <div class="col-12 font bronze-text text-center">
                                            <?php echo $content2['stat_figure'] ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 black-text text-center">
                                            <h5><?php echo $content2['stat_text'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($content2['stat_figure_2']) : ?>
                                    <div class="col-md-6 col-12">
                                        <div class="row counter">
                                            <div class="col-12 font bronze-text text-center">
                                                <?php echo $content2['stat_figure_2'] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 black-text text-center">
                                                <h5><?php echo $content2['stat_text_2'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    <?php endif;
                            endif; ?>
                    <div class="row">
                        <?php echo $content2['text'] ?>
                    </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($content3['title']) : ?>
                <div class="col-12 p-0 accordion">
                    <h6 class="accordion_title_black_text"><?php echo $content3['title'] ?></h6>
                    <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                    <div class="accordion_text">
                        <?php if ($content3['stat_figure']) : ?>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="row counter">
                                        <div class="col-12 font bronze-text text-center">
                                            <?php echo $content3['stat_figure'] ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 black-text text-center">
                                            <h5><?php echo $content3['stat_text'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($content3['stat_figure_2']) : ?>
                                    <div class="col-md-6 col-12">
                                        <div class="row counter">
                                            <div class="col-12 font bronze-text text-center">
                                                <?php echo $content3['stat_figure_2'] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 black-text text-center">
                                                <h5><?php echo $content3['stat_text_2'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    <?php endif;
                            endif; ?>
                    <div class="row">
                        <?php echo $content3['text'] ?>
                    </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($content4['title']) : ?>
                <div class="col-12 p-0 accordion">
                    <h6 class="accordion_title_black_text"><?php echo $content4['title'] ?></h6>
                    <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                    <div class="accordion_text">
                        <?php if ($content4['stat_figure']) : ?>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="row counter">
                                        <div class="col-12 font bronze-text text-center">
                                            <?php echo $content4['stat_figure'] ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 black-text text-center">
                                            <h5><?php echo $content4['stat_text'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($content4['stat_figure_2']) : ?>
                                    <div class="col-md-6 col-12">
                                        <div class="row counter">
                                            <div class="col-12 font bronze-text text-center">
                                                <?php echo $content4['stat_figure_2'] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 black-text text-center">
                                                <h5><?php echo $content4['stat_text_2'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    <?php endif;
                            endif; ?>
                    <div class="row">
                        <?php echo $content4['text'] ?>
                    </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($content5['title']) : ?>
                <div class="col-12 p-0 accordion">
                    <h6 class="accordion_title_black_text"><?php echo $content5['title'] ?></h6>
                    <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                    <div class="accordion_text">
                        <?php if ($content5['stat_figure']) : ?>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="row counter">
                                        <div class="col-12 font bronze-text text-center">
                                            <?php echo $content5['stat_figure'] ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 black-text text-center">
                                            <h5><?php echo $content5['stat_text'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($content5['stat_figure_2']) : ?>
                                    <div class="col-md-6 col-12">
                                        <div class="row counter">
                                            <div class="col-12 font bronze-text text-center">
                                                <?php echo $content5['stat_figure_2'] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 black-text text-center">
                                                <h5><?php echo $content5['stat_text_2'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    <?php endif;
                            endif; ?>
                    <div class="row">
                        <?php echo $content5['text'] ?>
                    </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-12 p-0 accordion">
                <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                    <h6 class="accordion_title_black_text">FEES</h6>
                <?php else : ?>
                    <h6 class="accordion_title_black_text">FRAIS DE COURS</h6>
                <?php endif; ?>
                <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                <div class="accordion_text">
                    <div class="fees_table col-11 mx-auto my-3">
                        <?php echo $content6['table_row_1_intro'] ?>
                        <div class="row grey-bg py-2">
                            <div class="col-9">
                                <?php echo $content6['table_row_1_column_1_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_1_column_2_value'] ?>
                            </div>
                        </div>
                        <?php foreach ($feeItems1 as $item) {
                            echo '<div class="row py-3 opacity-bg"><div class="col-9">' . $item['product'] . '</div>';
                            echo '<div class="col-3">' . $item['fee'] . '</div></div>';
                        } ?>
                        <div class="row grey-bg py-2">
                            <div class="col-9">
                                <?php echo $content6['table_row_1_sign_off_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_1_sign_off'] ?>
                            </div>
                        </div>
                        <div class="row grey-bg mt-4 py-2">
                            <div class="col-9">
                                <?php echo $content6['table_row_2_column_1_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_2_column_2_value'] ?>
                            </div>
                        </div>
                        <?php foreach ($feeItems2 as $item) {
                            echo '<div class="row py-3 opacity-bg"><div class="col-9">' . $item['product'] . '</div>';
                            echo '<div class="col-3">' . $item['fee'] . '</div></div>';
                        } ?>
                        <div class="row grey-bg py-2 ">
                            <div class="col-9">
                                <?php echo $content6['table_row_3_sign_off_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_3_column_2_value'] ?>
                            </div>
                        </div>
                    </div>


                    <div class="fees_table col-11 mx-auto my-3">
                        <?php echo $content6['table_row_1_intro'] ?>
                        <div class="row grey-bg py-2">
                            <div class="col-9">
                                <?php echo $content6['table_row_1_column_1_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_1_column_2_value'] ?>
                            </div>
                        </div>
                        <?php foreach ($feeItems1 as $item) {
                            echo '<div class="row py-3 opacity-bg"><div class="col-9">' . $item['product'] . '</div>';
                            echo '<div class="col-3">' . $item['fee'] . '</div></div>';
                        } ?>
                        <div class="row grey-bg py-2">
                            <div class="col-9">
                                <?php echo $content6['table_row_1_sign_off_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_1_sign_off'] ?>
                            </div>
                        </div>
                        <div class="row grey-bg mt-4 py-2">
                            <div class="col-9">
                                <?php echo $content6['table_row_2_column_1_title'] ?>
                            </div>
                            <div class="col-3">
                                <?php echo $content6['table_row_2_column_2_value'] ?>
                            </div>
                        </div>

                        <div class="row py-3 opacity-bg">
                            <div class="col-9">' . $item['product'] . '</div>';
                            <div class="col-3"></div>
                        </div>';


                        <div class="row grey-bg py-2 ">
                            <div class="col-9">
                                item
                            </div>
                            <div class="col-3">
                                value
                            </div>
                            <div class="col-9">
                                item
                            </div>
                            <div class="col-3">
                                value
                            </div>
                            <div class="col-9">
                                item
                            </div>
                            <div class="col-3">
                                value
                            </div>
                        </div>
                    </div>




                    <?php if ($content7['table_row_1_intro']) : ?>
                        <div class="fees_table  col-11 mx-auto my-3">
                            <?php echo $content7['table_row_1_intro'] ?>
                            <div class="row grey-bg py-2">
                                <div class="col-10">
                                    <?php echo $content7['table_row_1_column_1_title'] ?>
                                </div>
                                <div class="col-2">
                                    <?php echo $content7['table_row_1_column_2_value'] ?>
                                </div>
                            </div>
                            <?php foreach ($feeItems3 as $item) {
                                echo '<div class="row py-3 opacity-bg"><div class="col-10">' . $item['product'] . '</div>';
                                echo '<div class="col-2">' . $item['fee'] . '</div></div>';
                            } ?>
                            <div class="row grey-bg py-2">
                                <div class="col-10">
                                    <?php echo $content7['table_row_1_sign_off_title'] ?>
                                </div>
                                <div class="col-2">
                                    <?php echo $content7['table_row_1_sign_off_value'] ?>
                                </div>
                            </div>
                            <div class="row grey-bg mt-4 py-2">
                                <div class="col-10">
                                    <?php echo $content7['table_row_2_column_1_title'] ?>
                                </div>
                                <div class="col-2">
                                    <?php echo $content7['table_row_2_column_2_value'] ?>
                                </div>
                            </div>
                            <?php foreach ($feeItems4 as $item) {
                                echo '<div class="row py-3 opacity-bg"><div class="col-10">' . $item['product'] . '</div>';
                                echo '<div class="col-2">' . $item['fee'] . '</div></div>';
                            } ?>
                            <div class="row grey-bg py-2 ">
                                <div class="col-10">
                                    <?php echo $content7['table_row_3_sign_off_title'] ?>
                                </div>
                                <div class="col-2">
                                    <?php echo $content7['table_row_3_column_2_value'] ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row wrapper <?php if (get_field('hide_left_right_section')) {
                                echo 'd-none';
                            } else {
                                echo 'd-block';
                            } ?>">
        <div class="col-12 bronze-bg left_text_right_img">
            <div id='remote_learning_option' class="row ">
                <div class="col-md-6 pt-5 pb-3 py-md-0 col-12 d-flex justify-content-center align-items-center order-md-1 order-2">
                    <div class="scrollElement col-xl-9 col-lg-11 m-auto">
                        <h2><?php the_field('left_right_title') ?></h2>
                        <?php the_field('left_right_text') ?>
                        <!-- <a class="btn btn-primary blue" href="<?php echo get_field('left_right_link')['url'] ?>"><?php echo get_field('left_right_link')['title'] ?></a> -->
                    </div>
                </div>
                <div class="col-md-6 col-12 p-0 order-md-2 order-1">
                    <img src="<?php echo get_field('left_right_image')['url']; ?>" alt="<?php echo get_field('left_right_image')['alt']; ?>" />
                </div>
            </div>
        </div>
    </div>
    <div class="row wrapper <?php if (get_field('hide_left_right_section2')) {
                                echo 'd-none';
                            } else {
                                echo 'd-block';
                            } ?>">
        <div class="col-12 blue-bg left_text_right_img"><?php //hidden this (.d-none) as it was rendering emtpy (no image)
                                                                ?>
            <div class="row ">

                <div class="col-md-6 col-12 p-0 ">
                    <img src="<?php echo get_field('left_right_image2')['url']; ?>" alt="<?php echo get_field('left_right_image2')['alt']; ?>" />
                </div>
                <div class="col-md-6 pt-5 pb-3 py-md-0 col-12 d-flex justify-content-center align-items-center ">
                    <div class="scrollElement col-xl-9 col-lg-11 m-auto">
                        <h2><?php the_field('left_right_title2') ?></h2>
                        <?php the_field('left_right_text2') ?>
                        <!-- <a class="btn btn-primary blue" href="<?php echo get_field('left_right_link2')['url'] ?>"><?php echo get_field('left_right_link2')['title'] ?></a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper row big_image line-after">
        <div class="background_image">
            <img src="<?php echo get_field('background_image')['url']; ?>" alt="<?php echo get_field('background_image')['alt']; ?>" />
        </div>
        <div class="scrollElement foreground_text">
            <h1><?php the_field('overlay_text') ?>
            </h1>
        </div>
    </div>
    <div class="wrapper row stats">
        <div class="scrollElement col-12 my-5">
            <div class="counter inner no-background text-center">
                <div class="row">
                    <div class="col-sm-8 col-10 m-auto">
                        <h3 class="my-5"><?php the_field('stats_title') ?></h3>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="row">
                                    <div class="col-12 bronze-text">
                                        <h4 class="black-text col-12" <?php if (!the_field('stat_1_intro')) {
                                                                            echo "style='margin-top: 65px'";
                                                                        } ?>><?php the_field('stat_1_intro') ?></h4>
                                        <div class="font"><span class="value "><?php the_field('stat_1_figure') ?></span><?php the_field('stat_1_figure_after') ?></div>
                                        <p class="col-12 black-text"><?php the_field('stat_1_close_text') ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 bronze-text">
                                        <h4 class="black-text col-12" <?php if (!the_field('stat_2_intro')) {
                                                                            echo "style='margin-top: 65px'";
                                                                        } ?>><?php the_field('stat_2_intro') ?></h4>
                                        <div class="font"><span class="value "><?php the_field('stat_2_figure') ?></span><?php the_field('stat_2_figure_after') ?></div>
                                        <p class="col-12 black-text"><?php the_field('stat_2_close_text') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="row">
                                    <div class="col-12 bronze-text">
                                        <h4 class="black-text col-12" <?php if (!the_field('stat_3_intro')) {
                                                                            echo "style='margin-top: 65px'";
                                                                        } ?>><?php the_field('stat_3_intro') ?></h4>
                                        <div class="font"> <span class="value "><?php the_field('stat_3_figure') ?></span><?php the_field('stat_3_figure_after') ?></div>
                                        <p class="col-12 black-text"><?php the_field('stat_3_close_text') ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 bronze-text">
                                        <h4 class="black-text col-12" <?php if (!the_field('stat_4_intro')) {
                                                                            echo "style='margin-top: 65px'";
                                                                        } ?>><?php the_field('stat_4_intro') ?></h4>
                                        <div class="font"> <span class="value "><?php the_field('stat_4_figure') ?></span><?php the_field('stat_4_figure_after') ?></div>
                                        <p class="col-12 black-text"><?php the_field('stat_4_close_text') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center m-auto">
                                <a class="btn btn-primary blue  m-auto" href="<?php echo get_field('program_stats_link')['url'] ?>"><?php echo get_field('program_stats_link')['title'] ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper row left_img_right_text">
        <div class="col-md-6 col-12 p-0  d-flex justify-content-center align-items-center">
            <div class="overlay"></div>
            <img class="img-fluid" src="<?php echo get_field('quote_image')['url']; ?>" alt="<?php echo get_field('quote_image')['alt']; ?>" />
            <div class="scrollElement absolute col-9 m-auto text-center white-text">
                <h3><?php the_field('quote_image_text'); ?></h3>
            </div>
        </div>
        <div class="col-md-6 col-12  py-5  d-flex justify-content-center align-items-center beige-bg">
            <div class="scrollElementOffsetThird col-9 col-sm-11  m-auto  text-center">
                <h3 class="mb-4">“<?php the_field('main_quote'); ?>”
                </h3>
                <p><?php the_field('cite'); ?></p>
                <a class="mt-5 btn btn-primary transparent" href="<?php echo get_field('program_quote_link')['url'] ?>"><?php echo get_field('program_quote_link')['title'] ?></a>
            </div>
        </div>
    </div>
    <section class="faculty-section row <?php if (get_field('show_or_hide_faculty_section')) {
                                            echo 'd-none';
                                        } else {
                                            echo 'd-block';
                                        } ?>">
        <div class="wrapper row my-5">
            <div class="scrollElement col-12  my-3">
                <h2 class="text-center mt-5"><?php the_field('faculty_title'); ?></h2>
                <p class="text-center"><?php the_field('faculty_text'); ?></p>
            </div>
        </div>
        <div class="row my-5 alumni">
            <div id="alumni-carousel" class="wrapper carousel slide glion-alumni-carousel" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators d-flex d-md-none">
                    <li data-target="#alumni-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#alumni-carousel" data-slide-to="1"></li>
                    <li data-target="#alumni-carousel" data-slide-to="2"></li>
                    <li data-target="#alumni-carousel" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                    <?php
                    $faculty_selector = get_field('faculty_selector');
                    $postCount = 0;
                    if ($faculty_selector) :
                        // var_dump($faculty_selector) 
                    ?>
                        <?php foreach ($faculty_selector as $post) :
                            // var_dump($post);
                            // Setup this post for WP functions (variable must be named $post).
                            setup_postdata($post); ?>
                            <div class="carousel-item item col-12 col-md-3  p-0 <?php if ($postCount == 0) {
                                                                                    echo 'scrollElement active';
                                                                                } else if ($postCount == 1) {
                                                                                    echo 'scrollElementOffset ';
                                                                                } else if ($postCount == 2) {
                                                                                    echo 'scrollElementOffsetThird ';
                                                                                } else if ($postCount == 3) {
                                                                                    echo 'scrollElementOffsetLast ';
                                                                                } ?>">
                                <div class="d-flex" style='flex-direction: column'>
                                    <div class="row <?php if ($postCount == 1 || $postCount == 3) {
                                                        echo 'order-1 order-md-2 ';
                                                    } ?>">
                                        <div class="col-12">
                                            <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>" />
                                        </div>
                                    </div>
                                    <div class="row <?php if ($postCount == 1 || $postCount == 3) {
                                                        echo 'order-2 order-md-1 ';
                                                    } ?>">
                                        <div class="col-10 py-5 m-auto  text-center">
                                            <p><?php echo get_field('faculty_excerpt', get_the_ID()) ?></p>
                                            <div class="row">
                                                <div class="learn_more_container m-auto text-center">
                                                    <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                                        <a data-toggle="modal" data-target="#alumniModal" data-slide-to="<?php echo $postCount ?>" class="learn_more" href="">Learn more</a>
                                                    <?php else : ?>
                                                        <a data-toggle="modal" data-target="#alumniModal" data-slide-to="<?php echo $postCount ?>" class="learn_more" href="">En Savoir Plus</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $postCount++; ?>
                        <?php endforeach; ?>
                        <?php
                        // Reset the global post object so that the rest of the page works correctly.
                        wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
                <a class="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
    <div class="modal fade alumniModal" id="alumniModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
                </div>
                <div class="modal-body">
                    <div id="carouselExampleControls" class="carousel slide alumni" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <?php
                            $faculty_selector = get_field('faculty_selector');
                            $postCount = 0;
                            if ($faculty_selector) :
                                // var_dump($faculty_selector) 
                            ?>
                                <?php foreach ($faculty_selector as $post) :
                                    // var_dump($post);
                                    // Setup this post for WP functions (variable must be named $post).
                                    setup_postdata($post); ?>
                                    <div class="carousel-item <?php if ($postCount == 0) {
                                                                    echo ' active';
                                                                } ?>">
                                        <div id="alumni1" class="left_img_right_text alumni_profile">
                                            <div class="row">
                                                <div class="col-md-6 col-12  p-0  d-flex justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3) {
                                                                                                                                        echo 'order-1 order-md-2 beige-bg';
                                                                                                                                    } else {
                                                                                                                                        echo "grey-bg";
                                                                                                                                    } ?>">
                                                    <div class=" col-9 m-auto text-center text-md-left">
                                                        <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12 p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3) {
                                                                                                                                            echo 'order-2 order-md-1';
                                                                                                                                        }  ?>">
                                                    <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $postCount++; ?>
                                <?php endforeach; ?>
                                <?php
                                // Reset the global post object so that the rest of the page works correctly.
                                wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id='DownloadBrochure' class="row blue-bg line-after py-5">
        <div class="col-md-5 col-12 p-5">
            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                <h2 class="text-center text-md-left">Download <br>
                    a brochure
                </h2>
            <?php else : ?>
                <h2 class="text-center text-md-left">
                    Télécharger <br> une brochure

                </h2>
            <?php endif; ?>
        </div>
        <div class="col-md-7 col-12 p-5">
            <?php the_field('form_code'); ?>

        </div>
    </div>
    <div class="wrapper row my-5">
        <div class="scrollElement col-12">
            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                <h2 class="text-center">Campus Locations</h2>
            <?php else : ?>
                <h2 class="text-center">
                    Nos campus</h2>
            <?php endif; ?>
        </div>
    </div>
    <div class="row my-5">
        <div id="campus-carousel" class="wrapper carousel slide mb-5 col-12 p-0" data-ride="carousel" data-interval="false">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                <div class="scrollElement col-12 col-md-6 campus-rollover p-0 active <?php if (get_field('show_campuses') == 'Switzerland') {
                                                                                                    echo "col-md-12 px-0";
                                                                                                }
                                                                                                elseif (get_field('show_campuses') == 'London') {
                                                                                                    echo "d-none";
                                                                                                }
                                                                                                else{
                                                                                                    echo "px-md-3";
                                                                                                }  ?>">
                    <div class="d-flex justify-content-center align-items-center ">
                        <div class="image col-12 p-0">
                            <div class="overlay"></div>
                            <?php if (get_field('show_campuses') == 'Switzerland') : ?><img class="img-fluid" src="<?php echo get_field('campus_1_image_wide', 23)['url']; ?>" alt="<?php echo get_field('campus_1_image_wide', 23)['alt']; ?>" /><?php else : ?><img class="img-fluid" src="<?php echo get_field('campus_1_image', 23)['url']; ?>" alt="<?php echo get_field('campus_1_image', 23)['alt']; ?>" /><?php endif; ?>
                        </div>
                        <div class="absolute col-9 m-auto text-center">
                            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                <h2><?php the_field('campus_1_title', 23) ?></h2>
                            <?php else : ?>
                                <h2><?php the_field('campus_1_title', 4549) ?></h2>
                            <?php endif; ?>
                            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                <p><?php the_field('campus_1_location', 23) ?></p>
                            <?php else : ?>
                                <p><?php the_field('campus_1_location', 4549) ?></p>
                            <?php endif; ?>
                        </div>
                        <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                            <button class="btn btn-primary blue"><?php echo get_field('campus_1_link_homee', 23)['title'] ?></button>

                            <a href="<?php echo get_field('campus_1_link_homee', 23)['url'] ?>"></a>
                        <?php endif; ?>
                        <?php if (ICL_LANGUAGE_CODE == 'fr') : ?>
                            <button class="btn btn-primary blue">EN SAVOIR PLUS</button>
                            <a href="/fr/lieux/suisse/"></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="scrollElementOffset col-12 col-md-6 campus-rollover p-0 mt-md-0 mt-3 <?php if (get_field('show_campuses') == 'London') {
                                                                                                            echo "col-md-12 d-block px-0";
                                                                                                        }
                                                                                                        elseif (get_field('show_campuses') == 'Switzerland') {
                                                                                                            echo "d-none col-md-12";
                                                                                                        }
                                                                                                        else{
                                                                                                            echo "px-md-3";
                                                                                                        }   ?>">
                    <div class="d-flex justify-content-center align-items-center ">
                        <div class="image col-12 p-0">
                            <div class="overlay"></div>
                            <?php if (get_field('show_campuses') == 'London') : ?><img class="img-fluid" src="<?php echo get_field('campus_2_image_wide', 23)['url']; ?>" alt="<?php echo get_field('campus_2_image_wide', 23)['alt']; ?>" /><?php else : ?><img class="img-fluid" src="<?php echo get_field('campus_2_image', 23)['url']; ?>" alt="<?php echo get_field('campus_2_image', 23)['alt']; ?>" /><?php endif; ?>
                        </div>
                        <div class="absolute col-9 m-auto  text-center">
                            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                <h2><?php the_field('campus_2_title', 23) ?></h2>
                            <?php else : ?>
                                <h2><?php the_field('campus_2_title', 4549) ?></h2>
                            <?php endif; ?>
                            <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                <p><?php the_field('campus_2_location', 23) ?></p>
                            <?php else : ?>
                                <p><?php the_field('campus_2_location', 4549) ?></p>
                            <?php endif; ?>
                        </div>
                        <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                            <button class="btn btn-primary blue"><?php echo get_field('campus_2_link_homee', 23)['title'] ?></button>
                            <a href="<?php echo get_field('campus_2_link_homee', 23)['url'] ?>"></a>
                        <?php else : ?>
                            <button class="btn btn-primary blue">EN SAVOIR PLUS</button>
                            <a href="/fr/lieux/londres/"></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5  <?php if (get_field('hide_faq_section')) {
                                echo 'd-none';
                            } ?>">
        <div class="col-12">
            <h2 class="text-center mt-5"><?php the_field('frequently_asked_title') ?>
            </h2>
        </div>
    </div>
    <div class="row my-5  <?php if (get_field('hide_faq_section')) {
                                echo 'd-none';
                            } ?>">
        <?php if (have_rows('accordion_row_test')) : $accordion++; ?>
            <?php while (have_rows('accordion_row_test')) : the_row(); ?>
                <div class="col-12 p-0 accordion">
                    <h2 class="accordion_title_black_text program-ind-accordion-title"><?php the_sub_field('title'); ?></h2>
                    <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                    <div class="accordion_text">
                        <?php if (get_field('number_acc_columns') == 'Single column') : ?>
                            <div class="row ">
                                <div class="col-12">
                                    <?php the_sub_field('content_left'); ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <?php the_sub_field('content_left'); ?>
                                </div>
                                <div class="col-12 col-md-6">
                                    <?php the_sub_field('content_right'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
    </div>
<?php endif; ?>
<div class="wrapper row my-5 m-auto">
    <div class="scrollElement col-12  my-3">
        <h2 class="text-center"><?php the_field('view_programs_title') ?></h2>
        <p class="text-center"><?php the_field('view_programs_text') ?></p>
        <div class="col text-center m-auto">
            <a class="my-5 btn btn-primary blue " href="<?php echo get_field('programs_programs_link')['url'] ?>"><?php echo get_field('programs_programs_link')['title'] ?></a>
        </div>
    </div>
</div>
</div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer();
