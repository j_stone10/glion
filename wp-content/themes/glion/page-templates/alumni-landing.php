<!-- Template for the all main Program pages  -->
<?php
   /*
   Template Name: Alumni Landing
   */
   
   get_header(); ?>
<div class="container-fluid">
   <div class="row line-after">
      <div class="col-md-6 col-12 pr-md-0 program-content ind order-2 order-md-1">
         <div class="breadcrumb-container d-none d-md-block">
            <?php the_breadcrumb() ?>
         </div>
         <div class="row program-text individual mr-md-0">
            <div class="scrollElementStart col-xl-9 col-lg-11 col-11 m-auto p-sm-2">
               <h1><?php the_title(); ?>
               </h1>
               <?php the_field('alumni_overview'); ?>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-12 order-1 order-md-2 image fill p-0">
         <img src="<?php echo get_field('alumni_header_image')['url']; ?>" alt="<?php echo get_field('alumni_header_image')['alt']; ?>" />
      </div>
   </div>
   <div class=" row stats">
      <div class="col-12 my-5">
         <div class=" counter  no-background text-center bronze-text">
            <div class="row">
               <div class="col-sm-8 col-10 m-auto">
                  <h3 class="my-5"><?php the_field('stats_title') ?></h3>
               </div>
            </div>
            <div class="wrapper row">
               <div class=" col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class=" col-12  my-5">
                        <?php if ( the_field('stat_1_intro') ) : ?> 
                        <p class="col-12"> <?php the_field('stat_1_intro') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffset font"> <span class="value "><?php the_field('stat_1_figure') ?></span><?php the_field('stat_1_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_1_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class=" col-md-4 mr-auto col-12 <?php if ( !the_field('stat_2_figure') )  { echo 'd-none'; } ?>">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( the_field('stat_2_intro') ) : ?> 
                        <p class="col-12"> <?php the_field('stat_2_intro') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffsetThird font"><span class="value "><?php the_field('stat_2_figure') ?></span><?php the_field('stat_2_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_2_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class=" col-md-4 mr-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( the_field('stat_3_intro') ) : ?> 
                        <p class="col-12"> <?php the_field('stat_3_intro') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffsetLast font"><span class="value "><?php the_field('stat_3_figure') ?></span><?php the_field('stat_3_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_3_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <?php  if ( get_field('secondary_title') ) : ?>
            </div>
            <div class="row">
               <div class="col-sm-8 col-10 m-auto">
                  <h3 class="my-5"><?php the_field('secondary_title') ?></h3>
               </div>
            </div>
            <div class="wrapper row">
               <?php endif; ?>
               <div class="col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( the_field('stat_4_intro') ) : ?> 
                        <p class="col-12"> <?php the_field('stat_4_intro') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffset font"><span class="value "><?php the_field('stat_4_figure') ?></span><?php the_field('stat_4_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_4_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( the_field('stat_5_intro') ) : ?> 
                        <p class="col-12"> <?php the_field('stat_5_intro') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffsetThird font"><span class="value "><?php the_field('stat_5_figure') ?></span><?php the_field('stat_5_figure_after') ?></div>
                        <p class="col-12"><?php the_field('stat_5_close_text') ?>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <?php if (get_field('stats_link')) : ?>
            <div class="row">
               <div class="col text-center m-auto">
                  <button class="btn btn-primary blue  m-auto" href="<?php echo get_field('stats_link')['url'] ?>"><?php echo get_field('stats_link')['title']?></button>
               </div>
            </div>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <?php if( have_rows('image_text_row') ): $image_text_row++; ?>
   <?php while ( have_rows('image_text_row') ) : the_row(); ?>
   <?php if ( get_sub_field('layout_for_this_row') == "Option 1" ) : ?>
   <div class="wrapper row left_img_right_text alumni_landing no-image">
      <div class="col-md-6 col-12 p-0  d-flex justify-content-center align-items-center <?php echo get_sub_field('block_option_1')['background_colour_block_1']; if (get_sub_field('block_option_1')['has_shield_block_1']) { echo " shield";  } ?>">
         <?php if ( get_sub_field('block_option_1')['image_block_1'] == "" ) : ?>
         <div class="scrollElement col-9 m-auto text-center">
            <?php echo get_sub_field('block_option_1')['content_block_1']; ?>
         </div>
         <?php else : ?>
         <div class="overlay"></div>
         <img class="img-fluid"  src="<?php echo get_sub_field('block_option_1')['image_block_1']['url']; ?>"  alt="<?php echo get_sub_field('block_option_1')['image_block_1']['alt']; ?>" />
         <div class="scrollElement absolute col-11 m-auto text-center ">
            <?php echo get_sub_field('block_option_1')['content_block_1']; ?>
         </div>
         <?php endif; ?>
      </div>
      <div class="col-md-6 col-12 p-0  d-flex justify-content-center align-items-center <?php echo get_sub_field('block_option_1')['background_colour_block_2']; if (get_sub_field('block_option_1')['has_shield_block_2']) { echo " shield";  } ?>">
         <?php if ( get_sub_field('block_option_1')['image_block_2'] == "" ) : ?>
         <div class="scrollElementOffsetThird col-9 m-auto text-center ">
            <?php echo get_sub_field('block_option_1')['content_block_2']; ?>
         </div>
         <?php else : ?>
         <div class="overlay"></div>
         <img class="img-fluid"  src="<?php echo get_sub_field('block_option_1')['image_block_2']['url']; ?>"  alt="<?php echo get_sub_field('block_option_1')['image_block_2']['alt']; ?>" />
         <div class="scrollElementOffsetThird absolute col-9 m-auto text-center ">
            <?php echo get_sub_field('block_option_1')['content_block_2']; ?>
         </div>
         <?php endif; ?>
      </div>
   </div>
   <?php elseif ( get_sub_field('layout_for_this_row') == "Option 2" ) : ?>
   <div class="wrapper row left_img_right_text image alumni_landing">
      <div class="col-12 p-0  d-flex justify-content-center align-items-center">
         <div class="overlay"></div>
         <div class="image_container">
            <img src="<?php echo get_sub_field('block_option_2')['quote_image_option_2']['url']; ?>"  alt="<?php echo get_sub_field('block_option_2')['quote_image_option_2']['alt']; ?>" />
         </div>
         <div class="scrollElement absolute col-11 m-auto text-center white-text">
            <h3 class="mb-5">“<?php echo get_sub_field('block_option_2')['quote_image_text_option_2']; ?>”</h3>
            <p class="cite mb-5"><?php  echo get_sub_field('block_option_2')['quote_image_cite_option_2']; ?></p>
            <a class="learn_more white" href="<?php echo get_sub_field('block_option_2')['quote_image_link_option_2']['url'] ?>"><?php  echo get_sub_field('block_option_2')['quote_image_link_option_2']['title'] ?></a>
         </div>
      </div>
   </div>
   <?php endif; ?>
   <?php endwhile; ?>
   <?php endif; ?>
   <div class="wrapper row mb-5 bronze-bg">
      <div class="scrollElement col-12 col-md-6 m-auto my-3 py-5 text-center">
         <h2 class="text-center pt-5"><?php the_field('alumni_title') ?></h2>
         <p class="text-center"><?php the_field('alumni_text') ?></p>
         <div class="col text-center m-auto">
            <a class="my-5 btn btn-primary blue " href="<?php echo get_field('alumni_link')['url'] ?>"><?php echo get_field('alumni_link')['title']?></a>
         </div>
      </div>
   </div>
   <div class="wrapper row my-5  <?php if ( get_field('hide_programs_section') )  { echo 'd-none'; } ?>">
      <div class="col-12  my-3">
         <div class="scrollElement text-center">
            <h2 class="text-center pt-5"><?php the_field('view_programs_title') ?></h2>
            <p class="text-center"><?php the_field('view_programs_text') ?></p>
            <div class="col text-center m-auto">
               <a class="my-5 btn btn-primary blue " href="<?php echo get_field('view_programs_link')['url'] ?>"><?php echo get_field('view_programs_link')['title']?></a>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid mt-0">
<?php   if(ICL_LANGUAGE_CODE=='en') : ?>
<div class="wrapper row">
   <div class="scrollElement col-12 text-center">
      <h2>News</h2>
   </div>
</div>

<div class="wrapper row mb-5">
   <div class="splide">
      <div class="splide__track">
         <ul class="splide__list">
            <?php 
               $category_select = get_field('category_select');
               $tag_select = get_field('tag_select');
               $category_name = get_cat_name( $category_select[0] );
               // echo  $category_name;
               // var_dump($category_select);
               
               // $category_selected = echo $featured_post[0]
               
                  $args = array(
                     // 'category__not_in' => array( 52, 54 ),
                     // 'category__in' => array( $category_id ),
                     'category__in' => $category_select,
                     'tag_id' => $tag_select,
                     'posts_per_page' => 5,
                    
                 );
                 $query = new WP_Query($args);
                 $max_num_pages = $query->max_num_pages;
                    if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
            <li class="splide__slide">
               <div class="overlay"></div>
               <div class="image">
                  <?php the_post_thumbnail(); ?>
               </div>
               <div class="title">
                  <p class="text-center"><?php echo $category_name; ?></p>
                  <h6 class="text-center"><?php the_title(); ?></h6>
                  <div class="learn_more_container mx-auto pt-5 text-center">
                     <a class="learn_more white" href="<?php the_permalink() ?>">Learn More </a>
                  </div>
               </div>
            </li>
            <?php endwhile; 
               endif; 
               wp_reset_postdata(); 
               ?>
         </ul>
      </div>
   </div>
</div>
            <?php endif;  ?>
</div>
     
<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
<script>
   document.addEventListener( 'DOMContentLoaded', function () {
        // new Splide( '.splide' ).mount();
        if ($(window).width() >= 992) {
         const splide = new Splide( '.splide' );
            new Splide( '.splide', {
                  // rewind : true,

                           perPage: 3.5,
                           perMove: 1,
                           width: '100%',
                           type: 'loop',
                           //  autoplay: true,
                           gap: '1rem',
                           pagination: false,
                           isNavigation: false,
                           arrows: false,
                           heightRatio: 0.3,
            } ).mount();
         }
        if ($(window).width() >= 768 && $(window).width() <= 991) {
            new Splide( '.splide', {
                  // rewind : true,
                  
                           perPage: 2.5,
                           perMove: 1,
                           width: '100%',
                           type: 'loop',
                           //  autoplay: true,
                           gap: '1rem',
                           pagination: false,
                           isNavigation: false,
                           arrows: false,
                           heightRatio: 0.4,
            } ).mount();
         }
        if ($(window).width() <= 767) {
            new Splide( '.splide', {
                  // rewind : true,
                  
                           perPage: 1.2,
                           perMove: 1,
                           width: '100%',
                           type: 'loop',
                           //  autoplay: true,
                           gap: '1rem',
                           pagination: false,
                           isNavigation: false,
                           arrows: false,
                           heightRatio:  0.6,
            } ).mount();
         }
   } );
</script>
<!-- <script type="text/javascript" src="https://scrollmagic.io/assets/js/lib/greensock/TweenMax.min.js"></script>
   <script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/ScrollMagic.js"></script>
   <script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
   <script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script>
   <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll_magic_script.js"></script> -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer();