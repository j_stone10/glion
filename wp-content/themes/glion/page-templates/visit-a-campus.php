<!-- Template for the visit a campus pages  -->
<?php
   /*
   Template Name: Visit a Campus
   */
   
   get_header(); ?>
<div class="container-fluid">
   <div class="row campus hero">
      <div class="background_image">
         <img src="<?php echo get_field('campus_hero')['url']; ?>" alt="<?php echo get_field('campus_hero')['alt']; ?>"" />
      </div>
      <div class="scrollElementStart foreground_text col-md-9 col-11 m-auto">
         <h1 class="mb-3"><?php the_field('campus_hero_title'); ?></h1>
      </div>
   </div>
   <div class="wrapper row white-strip mt-5 justify-content-center align-items-center">
      <div class="scrollElement col-10 col-md-9  m-auto text-center">
         <h2 class="mb-0">Which campus would you like to visit?</h2>
      </div>
   </div>
   <div class=" row block-tabarea-02-2tabs pb-5">
      <ul class=" nav nav-tabs m-auto home-programmes two-tabs d-flex" role="tablist">
         <li class="nav-item col ml-auto">
            <!-- <button class="mt-5  btn btn-primary active mr-5" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab"><?php the_field('tab_1'); ?></button> -->
            <button class="mt-5  btn btn-primary active mr-5" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab">Switzerland</button>
         </li>
         <li class="nav-item col mr-auto">
            <!-- <button class="mt-5 btn btn-primary" href="#tabarea-02-2tabs-tab2" role="tab" data-toggle="tab"><?php the_field('tab_2'); ?></button> -->
            <button class="mt-5 btn btn-primary" href="#tabarea-02-2tabs-tab2" role="tab" data-toggle="tab">London</button>
         </li>
      </ul>
   </div>
   <?php 
      $tab1 = get_field('visit_a_campus_tabs')['campus_tab_1'];
      $tab2 = get_field('visit_a_campus_tabs')['campus_tab_2'];
      $accordions1 = get_field('visit_a_campus_tabs')['campus_tab_1']['accordion_row'];
      $accordions2 = get_field('visit_a_campus_tabs')['campus_tab_2']['accordion_row'];
      
       ?>
   <div class="row">
      <div class=" tab-content module-tabs-content" style="overflow: hidden">
         <!-- TAB 1 -->
         <div role="tabpanel" class=" tab-pane visit-campus flex-tabs w-100 pb-5 active" id="tabarea-02-2tabs-tab1">
            <div class="wrapper row">
               <div class="col-md-6 col-12 image fill p-0 order-2 order-md-1">
                  <img src="<?php echo $tab1['left_image_block_1']['url']; ?>" alt="<?php echo $tab1['left_image_block_1']['alt']; ?>" />
               </div>
               <div class=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
                  <div class="scrollElement row program-text individual mr-md-0">
                     <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                        <?php echo $tab1['upcoming_open_days']; ?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="wrapper row hero ">
               <div class="background_image">
                  <img src="<?php echo $tab1['open_day_image_update']['url']; ?>" alt="<?php echo $tab1['open_day_image_update']['alt']; ?>"  />
               </div>
               <div class="scrollElement foreground_text col-md-9 col-11 m-auto about-us">
                  <?php echo $tab1['alternative_days_content'] ?>  
               </div>
            </div>
            <div class="wrapper row">
               <div class=" col-md-6 col-12 pr-md-0 d-flex   blue-bg">
                  <div class="scrollElement row program-text individual mr-md-0">
                     <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                        <?php echo $tab1['what_to_expect_content']; ?>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 image fill p-0 order-2 order-md-1">
                  <img src="<?php echo $tab1['right_image_update']['url']; ?>" alt="<?php echo $tab1['right_image_update']['alt']; ?>" />
               </div>
            </div>
            <div class="wrapper row">
               <div class="col-md-6 col-12 image fill p-0" style="padding-bottom: 50%">
               <div class="overlay"></div>
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2749.8332857024384!2d6.919475915804299!3d46.43218387635199!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478e9afcb5ec5c97%3A0xfe2cb1a9ab8f3f17!2sGlion%20Institute%20of%20Higher%20Education!5e0!3m2!1sen!2suk!4v1595924958822!5m2!1sen!2suk" width="1200" height="1200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
               </div>
               <div class=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
                  <div class="scrollElement row program-text individual mr-md-0">
                     <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                        <?php echo $tab1['meeting_point_content']; ?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt-1 blue-bg  py-5">
               <div class="col-md-5 col-12 p-5">
                  <h2 class="text-center text-md-left">
                  <?php echo $tab1['title']; ?>
                  </h2>
               </div>
               <div class="col-md-7 col-12 p-5">
               <?php echo $tab1['marketo_form_code']; ?>
               </div>
            </div>
            <div class="row my-5  <?php if ( $tab1['hide_faq_section'] )  { echo 'd-none'; } ?>">
               <div class="wrapper m-auto row my-5 white-strip my justify-content-center align-items-center">
                  <div class="scrollElement my-5 col-10 col-md-9 m-auto text-center">
                     <h2>Frequently asked questions</h2>
                  </div>
               </div>
               <div class="wrapper row">
                  <?php foreach($accordions1 as $item) { ?>
                  <div class=" col-12 p-0 accordion">
                     <h6 class="accordion_title_black_text"><?php echo $item['title']; ?></h6>
                     <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                     <div class="accordion_text">
                        <?php if ( $item['number_acc_columns'] == 'Single column' ) : ?>
                        <div class="row ">
                           <div class="col-12">
                              <?php echo $item['content_left']; ?>
                           </div>
                        </div>
                        <?php else : ?>
                        <div class="row">
                           <div class="col-12 col-md-6">
                              <?php echo $item['content_left']; ?>
                           </div>
                           <div class="col-12 col-md-6">
                              <?php echo $item['content_right']; ?>
                           </div>
                        </div>
                        <?php endif; ?>
                     </div>
                  </div>
                  <?php } ?>
               </div>
            </div>
         </div>
  
      <!-- TAB 2 -->
      <div role="tabpanel" class="tab-pane visit-campus  flex-tabs w-100 pb-5" id="tabarea-02-2tabs-tab2">
         <div class="wrapper row">
            <div class="col-md-6 col-12 image fill p-0 order-2 order-md-1">
               <img src="<?php echo $tab2['left_image']['url']; ?>" alt="<?php echo $tab2['left_image']['alt']; ?>" />
            </div>
            <div class=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
               <div class="scrollElement row program-text individual mr-md-0">
                  <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                     <?php echo $tab2['upcoming_open_days']; ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="wrapper row hero ">
            <div class="background_image">
               <img src="<?php echo $tab2['open_day_image']['url']; ?>" alt="<?php echo $tab2['open_day_image']['alt']; ?>"  />
            </div>
            <div class="scrollElement foreground_text col-md-9 col-11 m-auto about-us">
               <?php echo $tab2['alternative_days_content'] ?>  
            </div>
         </div>
         <div class="wrapper row">
            <div class=" col-md-6 col-12 pr-md-0 d-flex   blue-bg">
               <div class="scrollElement row program-text individual mr-md-0">
                  <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                     <?php echo $tab2['what_to_expect_content']; ?>
                  </div>
               </div>
            </div>
            <div class="col-md-6 col-12 image fill p-0 order-2 order-md-1">
               <img src="<?php echo $tab2['right_image']['url']; ?>" alt="<?php echo $tab2['right_image']['alt']; ?>" />
            </div>
         </div>
         <div class="wrapper row">
            <div class="col-md-6 col-12 image fill p-0" style="padding-bottom: 50%">
               <div class="overlay"></div>
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2486.2975132589345!2d-0.24635758406445235!3d51.45269382252235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760edc3a646d13%3A0x73f596efd4f47d6b!2sGlion%20Institute%20of%20Higher%20Education%2C%20London!5e0!3m2!1sen!2suk!4v1595924860194!5m2!1sen!2suk" width="1200" height="1200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
               <div class="scrollElement row program-text individual mr-md-0">
                  <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
                     <?php echo $tab2['meeting_point_content']; ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt-1 blue-bg  py-5">
            <div class="col-md-5 col-12 p-5">
               <h2 class="text-center text-md-left">
               <?php echo $tab2['title']; ?>
               </h2>
            </div>
            <div class="col-md-7 col-12 p-5">
            <?php echo $tab2['marketo_form_code']; ?>
            </div>
         </div>
         <div class="row my-5  <?php if ( $tab2['hide_faq_section'] )  { echo 'd-none'; } ?>">
            <div class="wrapper m-auto row my-5 white-strip my justify-content-center align-items-center">
                  <div class="scrollElement my-5 col-10 col-md-9 m-auto text-center">
                     <h2>Frequently asked questions</h2>
                  </div>
               </div>
            <div class="wrapper row">
               <?php foreach($accordions2 as $item) { ?>
               <div class=" col-12 p-0 accordion">
                  <h6 class="accordion_title_black_text"><?php echo $item['title']; ?></h6>
                  <div class="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                  <div class="accordion_text">
                     <?php if ( $item['number_acc_columns'] == 'Single column') : ?>
                     <div class="row ">
                        <div class="col-12">
                           <?php  echo $item['content_left']; ?>
                        </div>
                     </div>
                     <?php else : ?>
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <?php echo $item['content_left']; ?>
                        </div>
                        <div class="col-12 col-md-6">
                           <?php echo $item['content_right']; ?>
                        </div>
                     </div>
                     <?php endif; ?>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer();