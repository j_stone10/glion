<?php
   /*
   Template Name: About Us
   */
   
   get_header(); ?>
<div class="container-fluid">
   <div class="row hero line-after">
      <div class="background_image">
         <img src="<?php echo get_field('hero_section_about_image')['url']; ?>" alt="<?php echo get_field('hero_section_about_image')['alt']; ?>"  />
      </div>
      <div class="scrollElementStart foreground_text col-md-9 col-11 m-auto about-us">
         <h2>
            <?php the_field('hero_section_about_title') ?>  
         </h2>
      </div>
   </div>
   <div class="wrapper row intro-glion grey-bg py-md-5">
      <div class="container">
         <div class="row py-5">
            <div class=" col-12 pt-5">
               <h1><?php the_field('glion_intro_title') ?></h1>
            </div>
         </div>
         <div class="row offset">
            <div class="col-12 col-md-8 d-flex justify-content-center align-items-center">
               <div class="row mt-md-5">
                  <div class="scrollElementOffset col-12 col-md-6">
                     <?php the_field('glion_intro_text') ?>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4">
               <img src="<?php echo get_field('glion_intro_image')['url']; ?>" alt="<?php echo get_field('glion_intro_image')['alt']; ?>"  />
            </div>
         </div>
      </div>
   </div>
   <div class=" row stats blue-bg">
      <div class="col-12 my-5">
         <div class=" counter  no-background text-center white-text">
            <div class="wrapper row">
               <div class="col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( get_field('intro_stat_1') ) : ?> 
                        <p class="col-12"> <?php the_field('intro_stat_1') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElement font"> <span class="value "><?php the_field('figure_stat_1') ?></span><?php the_field('symbol_stat_1') ?></div>
                        <p class="col-12"><?php the_field('signoff_stat_1') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 mr-auto col-12 <?php if ( !the_field('figure_stat_2') )  { echo 'd-none'; } ?>">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( get_field('intro_stat_2') ) : ?> 
                        <p class="col-12"> <?php the_field('intro_stat_2') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffset font"><span class="value "><?php the_field('figure_stat_2') ?></span><?php the_field('symbol_stat_2') ?></div>
                        <p class="col-12"><?php the_field('signoff_stat_2') ?>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="wrapper row">
               <div class="col-md-4 mr-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( get_field('intro_stat_3') ) : ?> 
                        <p class="col-12"> <?php the_field('intro_stat_3') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElement font"><span class="value "><?php the_field('figure_stat_3') ?></span><?php the_field('symbol_stat_3') ?></div>
                        <p class="col-12"><?php the_field('signoff_stat_3') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( get_field('intro_stat_4') ) : ?> 
                        <p class="col-12"> <?php the_field('intro_stat_4') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffset font"><span class="value "><?php the_field('figure_stat_4') ?></span><?php the_field('symbol_stat_4') ?></div>
                        <p class="col-12"><?php the_field('signoff_stat_4') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <?php if ( get_field('intro_stat_5') ) : ?> 
                        <p class="col-12"> <?php the_field('intro_stat_5') ?> </p>
                        <?php endif; ?>
                        <div class="scrollElementOffsetThird font"><span class="value "><?php the_field('figure_stat_5') ?></span><?php the_field('symbol_stat_5') ?></div>
                        <p class="col-12"><?php the_field('signoff_stat_5') ?>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <?php if (get_field('about_stats_cta')) : ?>
            <div class="row">
                     <div class="col text-center m-auto">
                        <a class="btn btn-primary bronze  m-auto" href="<?php echo get_field('about_stats_cta')['url'] ?>"><?php echo get_field('about_stats_cta')['title']?></a>
                     </div>
                  </div>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <div class="wrapper row big_image ">
      <div class="background_image">
         <img src="<?php echo get_field('full_width_image_about')['url']; ?>"  alt="<?php echo get_field('full_width_image_about')['alt']; ?>" />
      </div>
      <div class="scrollElement foreground_text">
         <h1><?php the_field('full_width_image_title_about') ?>
         </h1>
      </div>
   </div>
   <div class="wrapper row my-2">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-12 py-5 ">
               <div class="row ">
                  <div class="scrollElement col-12 col-md-8  small-font">
                     <?php the_field('reputation_section_left_content') ?>
                  </div>
               </div>
            </div>
            <div class="col-md-6 col-12">
               <img class="img-fluid" src="<?php echo get_field('reputation_section_right_image')['url']; ?>"  alt="<?php echo get_field('reputation_section_right_image')['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row testimonial shield mt-5 mb-5 justify-content-center align-items-center ">
      <div class="scrollElement col-9  p-5 m-auto text-center">
         <h2>“<?php the_field('about_us_quote') ?>”
         </h2>
      </div>
   </div>
   <div class="wrapper row intro-glion remove py-md-5">
      <div class="col-12 col-md-10 m-auto">
         <div class="row py-5">
            <div class="col-12 pt-5">
               <h1><?php the_field('career_section_title') ?></h1>
            </div>
         </div>
         <div class=" row offset fix">
            <div class="col-12 col-md-8 d-flex justify-content-center align-items-center">
               <div class="row mt-md-5 mb-5">
                  <div class="scrollElement col-12 col-md-8 fix">
                     <?php the_field('career_section_content') ?>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-4">
               <img src="<?php echo get_field('career_section_image')['url']; ?>" alt="<?php echo get_field('career_section_image')['alt']; ?>"  />
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container mt-5">
   <div class="wrapper row  pt-5">
      <div class=" col-12 col-md-5 m-auto">
         <?php echo get_field('learn_more_intro'); ?>
      </div>
   </div>
   <div class="row my-5 alumni">
      <div id="alumni-carousel" class="wrapper carousel slide w-100 glion-alumni-carousel" data-ride="carousel" data-interval="false">
         <ol class="carousel-indicators d-flex d-md-none">
            <li data-target="#alumni-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#alumni-carousel" data-slide-to="1"></li>
            <li data-target="#alumni-carousel" data-slide-to="2"></li>
            <!-- <li data-target="#alumni-carousel" data-slide-to="3"></li> -->
         </ol>
         <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <div class="scrollElement carousel-item item col-12 col-md-4 p-0  active">
               <div class="d-flex" style='flex-direction: column'>
                  <div class="row">
                     <div class="col-12">
                        <img  style="min-width:100%"  src="<?php echo get_field('learn_more_image_col_1')['url']; ?>"  alt="<?php echo get_field('learn_more_image_col_1')['alt']; ?>" />
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-10 py-5 m-auto text-center">
                        <?php echo get_field('learn_more_content_col_1'); ?>
                        <div class="row">
                           <div class="learn_more_container m-auto text-center">
                              <a class="learn_more" href="<?php echo get_field('learn_more_link_col_1')['url'] ?>"><?php echo get_field('learn_more_link_col_1')['title']?></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="scrollElementOffset carousel-item item col-12 col-md-4  p-0 ">
               <div class="d-flex" style='flex-direction: column'>
                  <div class="row order-1 order-md-2">
                     <div class="col-12">
                        <img  style="min-width:100%"  src="<?php echo get_field('learn_more_image_col_2')['url']; ?>"  alt="<?php echo get_field('learn_more_image_col_2')['alt']; ?>" />
                     </div>
                  </div>
                  <div class="row order-2 order-md-1">
                     <div class="col-10 py-5 m-auto text-center">
                        <?php echo get_field('learn_more_content_col_2'); ?>
                        <div class="row">
                           <div class="learn_more_container m-auto text-center">
                              <a class="learn_more" href="<?php echo get_field('learn_more_link_col_2')['url'] ?>"><?php echo get_field('learn_more_link_col_2')['title']?></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="scrollElementOffsetThird carousel-item item col-12 col-md-4  p-0 ">
               <div class="d-flex" style='flex-direction: column'>
                  <div class="row ">
                     <div class="col-12">
                        <img  style="min-width:100%"  src="<?php echo get_field('learn_more_image_col_3')['url']; ?>"  alt="<?php echo get_field('learn_more_image_col_3')['alt']; ?>" />
                     </div>
                  </div>
                  <div class="row ">
                     <div class="col-10 py-5 m-auto text-center">
                        <?php echo get_field('learn_more_content_col_3'); ?>
                        <div class="row">
                           <div class="learn_more_container m-auto text-center">
                              <a class="learn_more" href="<?php echo get_field('learn_more_link_col_3')['url'] ?>"><?php echo get_field('learn_more_link_col_3')['title']?></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <a class="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
         <h3 class="text-center">Programs</h3>
         <?php else : ?>
         <h3 class="text-center">PROGRAMMES</h3>
         <?php endif; ?>
      </div>
   </div>
   <div class="wrapper row">
      <div class="scrollElement col-md-6 col-12 pr-md-0 program-content">
         <div class="row mr-md-0">
            <div class="col-12 p-0">
               <ul id="myTab" class="nav nav-tabs" role="tablist">
                  <li class="nav-item darken active m-0" href="#bachelors_this" data-target=".bachelors_this, .bachelors_that" data-toggle="tab">
                     <a class="nav-link text-center nav-item" >
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <?php the_field('program_1_tab_title', 23) ?>
                     <?php else : ?>
                     <?php the_field('program_1_tab_title', 4549) ?>
                     <?php endif; ?>
                     </a>
                  </li>
                  <li class="nav-item darken m-0" href="#masters_this" data-target=".masters_this, .masters_that" data-toggle="tab">
                     <a class="nav-link text-center nav-item" >
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <?php the_field('program_2_tab_title', 23) ?>
                     <?php else : ?>
                     <?php the_field('program_2_tab_title', 4549) ?>
                     <?php endif; ?>
                  </li>
                  </a>
               </ul>
            </div>
         </div>
         <div class="row program-text mr-md-0">
            <div class="col-xl-9 col-lg-11 col-11 m-auto pl-md-0">
               <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade show bachelors_this">
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <h3>  <?php the_field('program_1_title', 23) ?></h3>
                     <?php else : ?>
                     <h3> <?php the_field('program_1_title', 4549) ?></h3>
                     <?php endif; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <p> <?php the_field('program_1_details', 23) ?></p>
                     <?php else : ?>
                     <p>  <?php the_field('program_1_details', 4549) ?></p>
                     <?php endif; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <a class="mt-5 btn btn-primary bronze"  href="<?php echo get_field('program_1_link', 23)['url'] ?>" role="button"><?php echo get_field('program_1_link', 23)['title'] ?></a>
                     <?php else : ?>
                     <a class="mt-5 btn btn-primary bronze"  href="<?php echo get_field('program_1_link', 4549)['url'] ?>" role="button"><?php echo get_field('program_1_link', 4549)['title'] ?></a>
                     <?php endif; ?>
                  </div>
                  <div class="tab-pane fade hide masters_this">
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <h3><?php the_field('program_2_title', 23) ?></h3>
                     <?php else : ?> 
                     <h3> <?php the_field('program_2_title', 4549) ?></h3>
                     <?php endif; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <p><?php the_field('program_2_details', 23) ?></p>
                     <?php else : ?>
                     <p><?php the_field('program_2_details', 4549) ?></p>
                     <?php endif; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <?php if (get_field('program_2_link', 23)) : ?><a class="mt-5 btn btn-primary bronze"  href="<?php echo get_field('program_2_link', 23)['url'] ?>" role="button"><?php echo get_field('program_2_link', 23)['title'] ?></a><?php endif; ?>
                     <?php else : ?>
                     <?php if (get_field('program_2_link', 23)) : ?><a class="mt-5 btn btn-primary bronze"  href="<?php echo get_field('program_2_link', 4549)['url'] ?>" role="button"><?php echo get_field('program_2_link', 4549)['title'] ?></a><?php endif; ?>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-12  p-0">
         <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade show bachelors_that">
               <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
               <img class="img-fluid" src="<?php echo get_field('program_1_image', 23)['url']; ?>" alt="<?php echo get_field('program_1_image', 23)['alt']; ?>"  />
               <?php else : ?>
               <img class="img-fluid" src="<?php echo get_field('program_1_image', 4549)['url']; ?>" alt="<?php echo get_field('program_1_image', 4549)['alt']; ?>"  />
               <?php endif; ?>
            </div>
            <div class="tab-pane fade hide  masters_that">
               <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
               <img class="img-fluid" src="<?php echo get_field('program_1_image', 23)['url']; ?>" alt="<?php echo get_field('program_1_image', 23)['alt']; ?>"  />
               <?php else : ?>
               <img class="img-fluid" src="<?php echo get_field('program_1_image', 4549)['url']; ?>" alt="<?php echo get_field('program_1_image', 4549)['alt']; ?>"  />
               <?php endif; ?>
               <div class="row p-0 m-0">
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class="masters-int-title accordion_title accordion_title_black_text coloured">
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>   
                           <?php the_field('program_2_block_1_title', 23) ?>
                        </h6>
                        <?php else : ?>
                        <?php the_field('program_2_block_1_title', 4549) ?></h6>
                        <?php endif; ?>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-int-text accordion_text  pb-5 p-md-0">
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <p><?php the_field('program_2_block_1_text', 23) ?></p>
                           <?php else : ?>
                           <p><?php the_field('program_2_block_1_text', 4549) ?></p>
                           <?php endif; ?>
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_11', 23)['url'] ?>"><?php  echo get_field('testing_new_link_11', 23)['title'] ?></a>
                           <?php else : ?>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_11', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_11', 4549)['title'] ?></a>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11  col-12 m-auto px-0 py-md-3">
                        <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                        <h6 class=" business-masters-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_2_title', 23) ?></h6>
                        <?php else : ?>
                        <h6 class=" business-masters-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_2_title', 4549) ?></h6>
                        <?php endif; ?>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg business-masters-text  accordion_text  pb-5 p-md-0">
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <p><?php the_field('program_2_block_2_text', 23) ?></p>
                           <?php else : ?>
                           <p><?php the_field('program_2_block_2_text', 4549) ?></p>
                           <?php endif; ?>
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_22', 23)['url'] ?>"><?php  echo get_field('testing_new_link_22', 23)['title'] ?></a>
                           <?php else : ?>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_22', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_22', 4549)['title'] ?></a>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row p-0 m-0">
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                        <h6 class=" masters-hosp-title accordion_title accordion_title_black_text coloured"><?php the_field('program_2_block_3_title', 23) ?></h6>
                        <?php else : ?>
                        <h6 class=" masters-hosp-title accordion_title accordion_title_black_text coloured"><?php the_field('program_2_block_3_title', 4549) ?></h6>
                        <?php endif; ?>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-hosp-text  accordion_text  pb-5 p-md-0">
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <p><?php the_field('program_2_block_3_text', 23) ?></p>
                           <?php else : ?>
                           <p><?php the_field('program_2_block_3_text', 4549) ?></p>
                           <?php endif; ?>
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_33', 23)['url'] ?>"><?php  echo get_field('testing_new_link_33', 23)['title'] ?></a>
                           <?php else : ?>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_33', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_33', 4549)['title'] ?></a>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                        <h6 class=" masters-lux-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_4_title', 23) ?></h6>
                        <?php else : ?>
                        <h6 class=" masters-lux-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_4_title', 4549) ?></h6>
                        <?php endif; ?>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-lux-text  accordion_text  pb-5 p-md-0">
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <p><?php the_field('program_2_block_4_text', 23) ?></p>
                           <?php else : ?>
                           <p><?php the_field('program_2_block_4_text', 4549) ?></p>
                           <?php endif; ?>
                           <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_44', 23)['url'] ?>"><?php  echo get_field('testing_new_link_44', 23)['title'] ?></a>
                           <?php else : ?>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_44', 4549)['url'] ?>"><?php  echo get_field('testing_new_link_44', 4549)['title'] ?></a>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="campusScrollInit" class="wrapper row  my-5">
      <div class="scrollElement col-12">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
         <h2 class="text-center"><?php the_field('main_campus_title', 23) ?></h2>
         <?php else : ?>
         <h2 class="text-center"><?php the_field('main_campus_title', 4549) ?></h2>
         <?php endif; ?>
      </div>
   </div>
   <div class="wrapper row my-5">
      <div id="campus-carousel" class="carousel slide mb-5 col-12 p-0" data-ride="carousel" data-interval="false">
         <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <div class="scrollElement col-12 col-md-6 campus-rollover p-0 px-md-3 active <?php if ( get_field('show_campuses') == 'Switzerland') { echo "col-md-12 px-0"; } elseif ( get_field('show_campuses') == 'London') { echo "d-none"; } else { echo "px-md-3"; }  ?>">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <?php if ( get_field('show_campuses') == 'Switzerland') : ?><img class="img-fluid"  src="<?php echo get_field('campus_1_image_wide', 23)['url']; ?>" alt="<?php echo get_field('campus_1_image_wide', 23)['alt']; ?>" /><?php else : ?><img class="img-fluid"  src="<?php echo get_field('campus_1_image', 23)['url']; ?>" alt="<?php echo get_field('campus_1_image', 23)['alt']; ?>" /><?php endif; ?>
                  </div>
                  <div class="absolute col-9 m-auto text-center">
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <h2><?php the_field('campus_1_title', 23) ?></h2>
                     <?php else : ?>
                     <h2><?php the_field('campus_1_title', 4549) ?></h2>
                     <?php endif; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <p><?php the_field('campus_1_location', 23) ?></p>
                     <?php else : ?>
                     <p><?php the_field('campus_1_location', 4549) ?></p>
                     <?php endif; ?>
                  </div>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <button class="btn btn-primary blue" ><?php  echo get_field('campus_1_link', 23)['title'] ?></button>
                  <a href="<?php echo get_field('campus_1_link', 23)['url'] ?>"></a>
                  <?php else : ?>
                     <button class="btn btn-primary blue" >EN SAVOIR PLUS</button>
                  <a href="<?php echo get_field('campus_1_link', 4549)['url'] ?>"></a>
                  <?php endif; ?>
               </div>
            </div>
            <div class="scrollElementOffset col-12 col-md-6 campus-rollover p-0 px-md-3 mt-md-0 mt-3 <?php if ( get_field('show_campuses') == 'London') { echo "col-md-12 d-block px-0"; } elseif ( get_field('show_campuses') == 'Switzerland') { echo "d-none col-md-12"; } else { echo "px-md-3"; }   ?>">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <?php if ( get_field('show_campuses') == 'London') : ?><img class="img-fluid"  src="<?php echo get_field('campus_2_image_wide', 23)['url']; ?>" alt="<?php echo get_field('campus_2_image_wide', 23)['alt']; ?>" /><?php else : ?><img class="img-fluid"  src="<?php echo get_field('campus_2_image', 23)['url']; ?>" alt="<?php echo get_field('campus_2_image', 23)['alt']; ?>" /><?php endif; ?>
                  </div>
                  <div class="absolute col-9 m-auto  text-center">
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <h2><?php the_field('campus_2_title', 23) ?></h2>
                     <?php else : ?>
                     <h2><?php the_field('campus_2_title', 4549) ?></h2>
                     <?php endif; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                     <p><?php the_field('campus_2_location', 23) ?></p>
                     <?php else : ?>
                     <p><?php the_field('campus_2_location', 4549) ?></p>
                     <?php endif; ?>
                  </div>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <button class="btn btn-primary blue" ><?php  echo get_field('campus_2_link', 23)['title'] ?></button>
                  <a href="<?php echo get_field('campus_2_link', 23)['url'] ?>"></a>
                  <?php else : ?>
                     <button class="btn btn-primary blue" >EN SAVOIR PLUS</button>
                  <a href="<?php echo get_field('campus_2_link', 4549)['url'] ?>"></a>
                  <?php endif; ?>
               </div>
            </div>
         </div>
         <a class="carousel-control-prev  <?php if ( ( get_field('show_campuses') == 'London') ||  ( get_field('show_campuses') == 'Switzerland') ) { echo "d-none "; } else { echo "d-flex d-md-none"; } ?>" href="#campus-carousel" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next  <?php if ( ( get_field('show_campuses') == 'London') ||  ( get_field('show_campuses') == 'Switzerland') ) { echo "d-none "; } else { echo "d-flex d-md-none"; } ?>" href="#campus-carousel" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
</div>
<script type="text/javascript" src="https://scrollmagic.io/assets/js/lib/greensock/TweenMax.min.js"></script>
<script type="text/javascript" src="https://scrollmagic.io/assets/js/lib/greensock/TimelineMax.min.js"></script>
<script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/ScrollMagic.js"></script>
<script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
<script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll_magic_script.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer();