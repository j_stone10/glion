<!-- Template for the all careers page  -->
<?php
   /*
   Template Name: Careers 
   */
   
   get_header(); ?>
<div class="container-fluid">
   <div class="wrapper row hero line-after">
      <div class="background_image">
         <img src="<?php echo get_field('careers_hero_image')['url']; ?>" alt="<?php echo get_field('careers_hero_image')['alt']; ?>"  />
      </div>
      <div class="scrollElementStart foreground_text col-md-9 col-11 m-auto">
         <?php the_field('hero_content') ?>
      </div>
   </div>
   <div class="wrapper row  mt-5 mb-5 justify-content-center align-items-center">
      <div class="scrollElement col-md-7 col-9  pt-5 m-auto text-center">
         <?php the_field('stats_text') ?>
      </div>
   </div>
   <div class=" row stats">
      <div class="col-12 ">
         <div class=" counter  no-background text-center">
            <div class="wrapper row">
               <div class="col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <div class="scrollElementOffset font"><span class="value "><?php the_field('stat_figure_1') ?></span><?php the_field('stat_figure_after_1') ?></div>
                        <p class="col-12"><?php the_field('stat_signoff_1') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <div class="scrollElementOffsetThird font"><span class="value "><?php the_field('stat_figure_2') ?></span><?php the_field('stat_figure_after_2') ?></div>
                        <p class="col-12"><?php the_field('stat_signoff_2') ?>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <div class="scrollElementOffsetLast font"><span class="value "><?php the_field('stat_figure_3') ?></span><?php the_field('stat_figure_after_3') ?></div>
                        <p class="col-12"><?php the_field('stat_signoff_3') ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row bronze-bg py-5 careers mt-5">
      <div class="col-md-9 col-12 m-auto py-5">
         <div class="row">
            <div class="col text-center">
               <h2><?php the_field('careers_title') ?></h2>
            </div>
         </div>
         <div class="row">
            <div class="col-md-5 col-12 m-auto text-center py-5">
               <div class="icon"><img src="<?php echo get_field('row_1_column_1_icon')['url']; ?>" alt="<?php echo get_field('row_1_column_1_icon')['alt']; ?>"  /></div>
               <div class="icon-text"><?php the_field('row_1_column_1_text') ?></div>
            </div>
            <div class="col-md-5 col-12 m-auto text-center py-5">
               <div class="icon"><img src="<?php echo get_field('row_1_column_2_icon')['url']; ?>" alt="<?php echo get_field('row_1_column_2_icon')['alt']; ?>"  /></div>
               <div class="icon-text"><?php the_field('row_1_column_2_text') ?></div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-5 col-12 m-auto text-center py-5">
               <div class="icon"><img src="<?php echo get_field('row_2_column_1_icon')['url']; ?>" alt="<?php echo get_field('row_2_column_1_icon')['alt']; ?>"  /></div>
               <div class="icon-text"><?php the_field('row_2_column_1_text') ?></div>
            </div>
            <div class="col-md-5 col-12 m-auto text-center py-5">
               <div class="icon"><img src="<?php echo get_field('row_2_column_2_icon')['url']; ?>" alt="<?php echo get_field('row_2_column_2_icon')['alt']; ?>"  /></div>
               <div class="icon-text"><?php the_field('row_2_column_2_text') ?></div>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row line-after">
      <div class="col-md-6 col-12 image fill p-0">
         <img src="<?php echo get_field('left_right_image')['url']; ?>" alt="<?php echo get_field('left_right_image')['alt']; ?>" />
      </div>
      <div class=" col-md-6 col-12 pr-md-0 d-flex   grey-bg">
         <div class="scrollElement row program-text individual mr-md-0">
            <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
               <?php the_field('left_right_content'); ?>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row  mt-5 mb-5 justify-content-center align-items-center">
      <div class=" col-md-7 col-9  pt-5 m-auto text-center">
         <h2><?php the_field('students_title') ?></h2>
      </div>
   </div>
   <?php if( have_rows('video_quote_row') ): $video_quote_row = 0; ?>
   <?php while ( have_rows('video_quote_row') ) : the_row(); ?>
   <div class="row bronze-bg">
      <div class="col-md-6 col-12  p-0  <?php if ( get_sub_field('media_side') == 'right' ) { echo "order-md-2"; } ?>">
         <div class="video_wrapper">
            <a data-toggle="modal" data-target="#content<?php echo $video_quote_row ?>">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo get_sub_field('image_video_content_image')['url']; ?>" alt="<?php echo get_sub_field('image_video_content_image')['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="wrapper col-md-6 col-12 pr-md-0 d-flex ind <?php if ( get_sub_field('media_side') == 'right' ) { echo "order-1"; } ?>">
         <div class="row program-text individual mr-md-0">
            <div class="scrollElement col-xl-6 col-lg-8 col-10 m-auto p-md-3">
               <div class="quote_before"><?php the_sub_field('image_video_content'); ?></div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="content<?php echo $video_quote_row ?>" tabindex="-1" role="dialog" aria-labelledby="content<?php echo $video_quote_row ?>" aria-hidden="true">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
               <iframe class="video_id" width="100%" height="500px" src="<?php echo get_sub_field('image_video_content_video') ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
   <?php $video_quote_row++;
      endwhile; ?>
   <?php endif; ?>
   <div class="wrapper row  mt-5 mb-5 justify-content-center align-items-center">
      <div class=" col-md-7 col-9  pt-5 m-auto text-center">
         <h2><?php the_field('stats_text_2') ?></h2>
      </div>
   </div>
   <div class=" row stats">
      <div class="col-12 ">
         <div class=" counter  no-background text-center">
            <div class="wrapper row">
               <div class="col-md-4 ml-auto col-12">
                  <div class="row">
                     <div class="col-12  my-5">
                        <div class="scrollElementOffset font"><span class="value "><?php the_field('stat_figure_10') ?></span><?php the_field('stat_figure_after_10') ?></div>
                        <p class="col-12"><?php the_field('stat_signoff_10') ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <div class="scrollElementOffsetThird font"><span class="value "><?php the_field('stat_figure_20') ?></span><?php the_field('stat_figure_after_20') ?></div>
                        <p class="col-12"><?php the_field('stat_signoff_20') ?>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mr-auto">
                  <div class="row">
                     <div class="col-12  my-5">
                        <div class="scrollElementOffsetLast font"><span class="value "><?php the_field('stat_figure_30') ?></span><?php the_field('stat_figure_after_30') ?></div>
                        <p class="col-12"><?php the_field('stat_signoff_30') ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row line-after">
      <div class=" col-md-6 col-12 pr-md-0 d-flex   grey-bg">
         <div class="scrollElement row program-text individual mr-md-0">
            <div class="col-xl-9 col-lg-11 col-11 m-auto p-5">
               <?php the_field('left_right_content_2'); ?>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-12 image fill p-0">
         <img src="<?php echo get_field('left_right_image_2')['url']; ?>" alt="<?php echo get_field('left_right_image_2')['alt']; ?>" />
      </div>
   </div>
   <div class="wrapper row  mt-5 mb-5 justify-content-center align-items-center">
      <div class=" col-md-7 col-9  pt-5 m-auto text-center">
         <h2><?php the_field('glion_grads_title') ?></h2>
      </div>
   </div>
   <?php $block1 = get_field('block_1'); ?>
   <?php $block2 = get_field('block_2'); ?>
   <?php $block3 = get_field('block_3'); ?>
   <?php $block4 = get_field('block_4'); ?>
   <?php $block5 = get_field('block_5'); ?>
   <?php $block6 = get_field('block_6'); ?>
   <div class="wrapper row bronze-bg">
      <div class="col-md-4 col-12 p-5 p-md-0 d-flex justify-content-center align-items-center order-4 order-md-1">
         <div class="scrollElement col-lg-8 col-10">
            <div class="quote_before"><?php echo $block1['quote']; ?></div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-0 d-flex order-1 order-md-2">
         <div class="video_wrapper w-100">
            <a data-toggle="modal" data-target="#content20">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo $block2['image_block_2']['url']; ?>" alt="<?php echo $block2['image_block_2']['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="modal fade" id="content20" tabindex="-1" role="dialog" aria-labelledby="content20" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php echo $block2['video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-5 p-md-0 d-flex justify-content-center align-items-center order-6 order-md-3">
         <div class="scrollElementOffsetThird  col-lg-8 col-10">
            <div class="quote_before"><?php echo $block3['quote']; ?></div>
         </div>
      </div>

      <div class="col-md-4 col-12 p-0 d-flex  order-3 order-md-4">
         <div class="video_wrapper w-100">
            <a data-toggle="modal" data-target="#content21">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo $block4['image_block_4']['url']; ?>" alt="<?php echo $block4['image_block_4']['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="modal fade" id="content21" tabindex="-1" role="dialog" aria-labelledby="content21" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php echo $block4['video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-5 p-md-0 d-flex justify-content-center align-items-center order-2 order-md-5 ">
         <div class="scrollElement col-lg-8 col-10">
            <div class="quote_before"><?php echo $block5['quote']; ?></div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-0 d-flex  order-4 order-md-6">
         <div class="video_wrapper w-100">
            <a data-toggle="modal" data-target="#content22">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo $block6['image_block_6']['url']; ?>" alt="<?php echo $block6['image_block_6']['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="modal fade" id="content22" tabindex="-1" role="dialog" aria-labelledby="content22" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php echo $block6['video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <div class="wrapper row  mt-5 mb-5 justify-content-center align-items-center">
      <div class=" col-md-7 col-9  pt-5 m-auto text-center">
         <h2><?php the_field('empoyers_title') ?></h2>
      </div>
   </div>
   <?php $block7 = get_field('block_111'); ?>
   <?php $block8 = get_field('block_222'); ?>
   <?php $block9 = get_field('block_333'); ?>
   <?php $block10 = get_field('block_444'); ?>
   <?php $block11 = get_field('block_555'); ?>
   <?php $block12 = get_field('block_666'); ?>
   <div class="wrapper row bronze-bg">
      <div class="col-md-4 col-12 p-0 d-flex order-1 order-md-1">
         <div class="video_wrapper w-100">
            <a data-toggle="modal" data-target="#content31">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo $block7['image_block_11']['url']; ?>" alt="<?php echo $block7['image_block_11']['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="modal fade" id="content31" tabindex="-1" role="dialog" aria-labelledby="content31" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php echo $block7['video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-5 p-md-0 d-flex justify-content-center align-items-center order-6 order-md-2">
         <div class="scrollElement col-lg-8 col-10">
            <div class="quote_before"><?php echo $block8['quote']; ?></div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-0 d-flex order-3 order-md-3">
         <div class="video_wrapper w-100">
            <a data-toggle="modal" data-target="#content32">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo $block9['image_block_33']['url']; ?>" alt="<?php echo $block9['image_block_33']['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="modal fade" id="content32" tabindex="-1" role="dialog" aria-labelledby="content32" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php echo $block9['video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>

      <div class="col-md-4 col-12 p-5 p-md-0 d-flex justify-content-center align-items-center order-2 order-md-4">
         <div class="scrollElement col-lg-8 col-10">
            <div class="quote_before"><?php echo $block10['quote']; ?></div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-0 d-flex order-5 order-md-5">
         <div class="video_wrapper w-100">
            <a data-toggle="modal" data-target="#content33">
               <div class="video">
                  <img style="min-width: 100%" src="<?php echo $block11['image_block_55']['url']; ?>" alt="<?php echo $block11['image_block_55']['alt']; ?>" />
               </div>
               <div class="play_icon">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon">
               </div>
            </a>
         </div>
      </div>
      <div class="modal fade" id="content33" tabindex="-1" role="dialog" aria-labelledby="content33" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <iframe class="video_id" width="100%" height="500px" src="<?php echo $block11['video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-12 p-5 p-md-0 d-flex justify-content-center align-items-center order-3 order-md-6">
         <div class="scrollElementOffsetThird  col-lg-8 col-10">
            <div class="quote_before"><?php echo $block12['quote']; ?></div>
         </div>
      </div>
   </div>
   <div class="wrapper row  mt-5 mb-5 justify-content-center align-items-center">
      <div class=" col-md-7 col-9  pt-5 m-auto text-center">
         <h2><?php the_field('view_programs_content') ?></h2>
      </div>
   </div>
</div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer();