<?php
/*
   Template Name: Home
   */

get_header(); ?>
<div class="fixed-ctas d-none d-sm-block">
   <?php wp_nav_menu(array('theme_location' => 'header actions')); ?>
   <div class="d-block d-sm-none">
      <div class="fixed_open slide">
         <img src="the_insider_glion.svg" />
      </div>
   </div>
</div>
<div class="container-fluid <?php if (get_field('show_top_banner_callout', get_the_ID())) : echo "callout-adjust";
                              endif; ?>">
   <!-- <div id='overlay-slide'></div> -->
   <div class="wrapper row hero">
      <div class="background_image">
         <img src="<?php echo get_field('hero_image')['url']; ?>" alt="<?php echo get_field('hero_image')['alt']; ?>" />
      </div>
      <div class="scrollElementStart foreground_text col-md-9 col-11 m-auto">
         <h1 class="mb-3"><?php the_field('hero_title') ?>
         </h1>
         <p><?php the_field('hero_text') ?></p>
      </div>
   </div>
   <div class="row wrapper">
      <div class="scrollElement col-md-6 col-12 pr-md-0 program-content ">
         <div class="row mr-md-0">
            <div class="col-12 program-title">
               <h3><?php the_field('program_header') ?></h3>
               <div class="program_study_text">
                  <?php if( !empty( get_field('programs_header_remote_study_text') ) ): ?> 
                     <svg width="28" height="18" viewBox="0 0 28 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M25.042 1.15227C25.038 0.510246 24.544 -0.00422567 23.9376 2.61653e-05L14.0461 0.0361668L4.15451 2.61653e-05C3.54808 -0.00422567 3.05411 0.512372 3.05009 1.15227L3.04407 2.03028V14.9303L14.0461 14.9027L25.0481 14.9303L25.042 1.15227ZM23.7087 13.4932L14.0461 13.5187L4.38342 13.4932L4.36535 1.65824C4.36535 1.54344 4.4537 1.45203 4.56013 1.45203H23.532" fill="#F0D4C6"></path>
                        <path d="M27.8092 15.9504L17.6385 15.9122L17.3494 16.4139H10.6245L10.3634 15.8867L0.194779 15.8484C0.0863453 15.8484 0 15.9398 0 16.0546V16.6966C0 16.7795 0.0461847 16.8539 0.116466 16.8858L1.55622 17.5513C1.93574 17.7256 2.34337 17.817 2.75702 17.817H25.247C25.6606 17.8191 26.0542 17.8149 26.4337 17.6427L27.8775 16.9879C27.9498 16.956 27.9959 16.8816 27.9959 16.7987L28 16.1566C28.002 16.044 27.9156 15.9504 27.8092 15.9504Z" fill="#F0D4C6"></path>
                        <path d="M15.3172 8.56258C16.3232 8.04386 17.018 6.95114 17.018 5.68622C17.018 3.92171 15.6666 2.49097 13.9999 2.49097C12.3333 2.49097 10.9819 3.92171 10.9819 5.68622C10.9819 6.95114 11.6766 8.04386 12.6826 8.56258C10.6365 9.1706 9.13647 11.1605 9.13647 13.5202H18.8634C18.8634 11.1583 17.3634 9.1706 15.3172 8.56258Z" fill="#F0D4C6"></path>
                     </svg>
                  <?php endif; ?>
                  <?php the_field('programs_header_remote_study_text') ?>
               </div>
            </div>
         </div>
         <div class="row mr-md-0">
            <div class="col-12 p-0">
               <ul id="myTab" class="nav nav-tabs" role="tablist">
                  <li class="nav-item darken m-0 active" href="#bachelors_this" data-target=".bachelors_this, .bachelors_that" data-toggle="tab">
                     <a class="nav-link text-center nav-item">
                        <?php the_field('program_1_tab_title') ?>
                     </a>
                  </li>
                  <li class="nav-item darken m-0 " href="#masters_this" data-target=".masters_this, .masters_that" data-toggle="tab">
                     <a class="nav-link text-center nav-item">
                        <?php the_field('program_2_tab_title') ?></a>
                  </li>
                  </a>
               </ul>
            </div>
         </div>
         <div class="row program-text mr-md-0">
            <div class="col-xl-9 col-lg-11 col-11 m-auto pl-md-0">
               <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade show bachelors_this">
                     <h3> <?php the_field('program_1_title') ?>
                     </h3>
                     <p> <?php the_field('program_1_details') ?></p>
                     <p class="remote_learning_text batchelors"><strong><?php the_field('remote_learning_overview_text', 4169) ?></strong></p>
                     <a class="mt-5 btn btn-primary bronze" href="<?php echo get_field('program_1_link')['url'] ?>" role="button"><?php echo get_field('program_1_link')['title'] ?></a>
                  </div>
                  <div class="tab-pane fade hide masters_this">
                     <h3><?php the_field('program_2_title') ?></h3>
                     <p><?php the_field('program_2_details') ?></p>
                     <?php if (get_field('program_2_link')) : ?><a class="mt-5 btn btn-primary bronze" href="<?php echo get_field('program_2_link')['url'] ?>" role="button"><?php echo get_field('program_2_link')['title'] ?></a><?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-12  p-0">
         <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade show bachelors_that">
               <img class="img-fluid" src="<?php echo get_field('program_1_image')['url']; ?>" alt="<?php echo get_field('program_1_image')['alt']; ?>" />
            </div>
            <div class="tab-pane fade hide  masters_that">
               <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/images/bachelors_image.jpg">
               <div class="row p-0 m-0">
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class="masters-int-title accordion_title accordion_title_black_text coloured"><?php the_field('program_2_block_1_title') ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-int-text accordion_text  pb-5 p-md-0">
                           <span>
                              <p><?php the_field('program_2_block_1_text') ?></p>
                           </span>
                           <p class="remote_learning_text masters"><?php the_field('remote_learning_overview_text', 1748) ?></p>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_11')['url'] ?>"><?php echo get_field('testing_new_link_11')['title'] ?></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11  col-12 m-auto px-0 py-md-3">
                        <h6 class=" business-masters-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_2_title') ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg business-masters-text  accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_2_text') ?></p>
                           <p class="remote_learning_text masters white"><?php the_field('remote_learning_overview_text', 1758) ?></p>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_22')['url'] ?>"><?php echo get_field('testing_new_link_22')['title'] ?></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row p-0 m-0">
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class=" masters-hosp-title accordion_title accordion_title_black_text coloured"><?php the_field('program_2_block_3_title') ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-hosp-text  accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_3_text') ?></p>
                           <p class="remote_learning_text masters"><?php the_field('remote_learning_overview_text', 1769) ?></p>
                           <a class="learn_more" href="<?php echo get_field('testing_new_link_33')['url'] ?>"><?php echo get_field('testing_new_link_33')['title'] ?></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured">
                     <div class="col-xl-9 col-lg-11 col-12 m-auto px-0 py-md-3">
                        <h6 class=" masters-lux-title accordion_title accordion_title_white_text coloured"><?php the_field('program_2_block_4_title') ?></h6>
                        <div class="expand-icon d-block d-lg-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" /></div>
                        <div class="white-bg masters-lux-text  accordion_text  pb-5 p-md-0">
                           <p><?php the_field('program_2_block_4_text') ?></p>
                           <p class="remote_learning_text masters white"><?php the_field('remote_learning_overview_text', 1777) ?></p>
                           <a class="learn_more white" href="<?php echo get_field('testing_new_link_44')['url'] ?>"><?php echo get_field('testing_new_link_44')['title'] ?></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row big_image line-after">
      <div class="background_image">
         <div class="d-none d-lg-block">
            <div><img style="min-width:100%" src="<?php echo get_field('parallax_image_desktop')['url']; ?>" alt="<?php echo get_field('parallax_image_desktop')['alt']; ?>" /></div>
         </div>
         <div class="d-block d-lg-none">
            <div class="overlay"></div>
            <img class="mobile" src="<?php echo get_field('image_mobile')['url']; ?>" alt="<?php echo get_field('image_mobile')['alt']; ?>" />
         </div>
      </div>
      <div class="counter text-center white-text">
         <div class="row wrapper">
            <div class="  col-sm-8 col-10 m-auto">
               <h2><?php the_field('main_stats_title') ?></h2>
               <div class="row">
                  <div class="col-md-6 col-12">
                     <div class="scrollElementOffsetThird font">
                        <?php if ('stat_1_title') :  ?>
                           <p><?php the_field('stat_1_title') ?></p>
                        <?php endif; ?>
                        <span class="value"><?php the_field('stat_1_figure') ?></span><?php the_field('stat_1_symbol_after') ?>
                     </div>
                     <p><?php the_field('stat_1_sign_off') ?></p>
                  </div>
                  <div class="col-md-6 col-12">
                     <div class="scrollElementOffsetLast font">
                        <?php if ('stat_2_title') :  ?>
                           <p><?php the_field('stat_2_title') ?></p>
                        <?php endif; ?>
                        <span class="value"><?php the_field('stat_2_figure') ?></span><?php the_field('stat_2_symbol_after') ?>
                     </div>
                     <p><?php the_field('stat_2_sign_off') ?></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row wrapper mt-5 <?php if (get_field('hide_left_right_section')) {
                                    echo 'd-none';
                                 } else {
                                    echo 'd-block';
                                 } ?>">
      <div class="col-12 bronze-bg left_text_right_img">
         <div class="row ">
            <div class="col-md-6 pt-5 pb-3 py-md-0 col-12 d-flex justify-content-center align-items-center">
               <div class="scrollElement col-xl-9 col-lg-11 m-auto">
                  <h2><?php the_field('title') ?></h2>
                  <?php the_field('text') ?>
                  <a class="btn btn-primary blue" href="<?php echo get_field('left_right_linkk')['url'] ?>"><?php echo get_field('left_right_linkk')['title'] ?></a>
               </div>
            </div>
            <div class="col-md-6 col-12 p-0">
               <img src="<?php echo get_field('image')['url']; ?>" alt="<?php echo get_field('image')['alt']; ?>" />
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row  my-5 ">
      <div class="scrollElement col-12">
         <h2 class="text-center"><?php the_field('main_campus_title') ?></h2>
      </div>
   </div>
   <div class="row my-5">
      <div id="campus-carousel" class="wrapper carousel slide" data-ride="carousel" data-interval="false">
         <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <div class="scrollElement col-12 col-md-6 campus-rollover p-0 px-md-3 active">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <img class="img-fluid" src="<?php echo get_field('campus_1_image')['url']; ?>" alt="<?php echo get_field('campus_1_image')['alt']; ?>" />
                  </div>
                  <div class="absolute col-9 m-auto text-center">
                     <h2><?php the_field('campus_1_title') ?></h2>
                     <p><?php the_field('campus_1_location') ?></p>
                  </div>
                  <button class="btn btn-primary blue"><?php echo get_field('campus_1_link_homee')['title'] ?></button>
                  <a href="<?php echo get_field('campus_1_link_homee')['url'] ?>"></a>
               </div>
            </div>
            <div class="scrollElementOffsetcol-12 col-md-6 campus-rollover p-0 px-md-3 justify-content-center align-items-center mt-md-0 mt-3">
               <div class="d-flex justify-content-center align-items-center ">
                  <div class="image col-12 p-0">
                     <div class="overlay"></div>
                     <img class="img-fluid" src="<?php echo get_field('campus_2_image')['url']; ?>" alt="<?php echo get_field('campus_2_image')['alt']; ?>" />
                  </div>
                  <div class="absolute col-9 m-auto  text-center">
                     <h2><?php the_field('campus_2_title') ?></h2>
                     <p><?php the_field('campus_2_location') ?></p>
                  </div>
                  <button class="btn btn-primary blue"><?php echo get_field('campus_2_link_homee')['title'] ?></button>
                  <a href="<?php echo get_field('campus_2_link_homee')['url'] ?>"></a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row testimonial d-none d-md-block testimonial mt-5 mb-5 justify-content-center align-items-center">
      <div class="scrollElement col-md-7 col-9 p-5 m-auto text-center">
         <h2>“<?php the_field('quote_1_text') ?>”
         </h2>
      </div>
   </div>
   <div class="row wrapper open-days mt-md-5 mb-5 ">
      <div class="col-sm-6 col-12  blue-bg order-2 order-sm-1">
         <div class="row scrollElement">
            <div class="col-md-6 col-9 py-5  text-center m-auto">
               <h3><?php the_field('block_1_title') ?></h3>
               <p><?php the_field('block_1_text') ?></p>
            </div>
         </div>
         <div class="row scrollElement">
            <div class="col-md-8 col-11 m-auto">
               <img class="img-fluid" src="<?php echo get_field('block_1_image')['url']; ?>" alt="<?php echo get_field('block_1_image')['alt']; ?>" />
            </div>
         </div>
         <div class="row scrollElement">
            <div class="learn_more_container m-auto py-5 text-center">
               <a class="learn_more white" href="<?php echo get_field('block_1_link_homee')['url'] ?>"><?php echo get_field('block_1_link_homee')['title'] ?></a>
            </div>
         </div>
      </div>
      <div class="col-sm-6 col-12 beige-bg  order-1 order-sm-2">
         <div class="row scrollElementOffset">
            <div class="col-md-7 col-9 py-5  text-center m-auto">
               <h3><?php the_field('block_2_title') ?></h3>
               <p><?php the_field('block_2_text') ?></p>
            </div>
         </div>
         <div class="row scrollElementOffset">
            <div class="col-md-8 col-11 m-auto">
               <img class="img-fluid" src="<?php echo get_field('block_2_image')['url']; ?>" alt="<?php echo get_field('block_2_image')['alt']; ?>" />
            </div>
         </div>
         <div class="row scrollElementOffset">
            <div class="learn_more_container m-auto py-5 text-center">
               <a class="learn_more " href="<?php echo get_field('block_2_link_homee')['url'] ?>"><?php echo get_field('block_2_link_homee')['title'] ?></a>
            </div>
         </div>
      </div>
   </div>
   <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
      <div class="wrapper row">
         <div class="scrollElement col-12 text-center">
            <h2>Glion: Insider Magazine</h2>
         </div>
      </div>
      <?php
      $args = array(
         'category__in' => array(52),
         'posts_per_page' => 1,
      );
      $featured_1_query = new WP_Query($args);
      // var_dump($featured_1_query);
      if ($featured_1_query->have_posts()) : while ($featured_1_query->have_posts()) : $featured_1_query->the_post();
            // $post_id = get_the_ID();
            // echo $post_id;

      ?>
            <div class="row ">
               <div class="blogPost col-12 p-0 d-flex justify-content-center">
                  <div class="line-after clear-mob">
                     <div class="image_container">
                        <div class="overlay"></div>
                        <!-- <img src="<?php echo get_field('hero_image_blog', get_the_ID())['url']; ?>" alt="<?php echo get_field('hero_image_blog', get_the_ID())['alt']; ?>" /> -->
                        <img src="<?php echo get_field('blog_featured_hero_overview', get_the_ID())['url']; ?>" alt="<?php echo get_field('blog_featured_hero_overview', get_the_ID())['alt']; ?>" />
                     </div>
                     <div class="col-10 py-5 m-auto text-center featuredCarousel d-block d-md-none">
                        <h6 class="text-center mb-2"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h6>
                        <div class="row  mt-5">
                           <div class="learn_more_container m-auto text-center">
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name !=  'Featured Priority 1') {
                                    echo '<a href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }

                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row featured1Home d-none d-md-block">
               <div class="col-12">
                  <h3 class="text-center mb-2"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                  <span class='mag_span text-center mb-0'>
                     <?php $categories = get_the_category();
                     foreach ($categories as $category) {
                        if ($category->name !=  'Featured Priority 1') {
                           echo '<a href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                        }
                     }
                     ?>
               </div>
            </div>
      <?php endwhile;
      endif;
      wp_reset_postdata();
      ?>
      <div class="container-fluid p-0">
         <div class="row my-md-5 alumni ">
            <div id="alumni-carousel" class="wrapper carousel slide" data-ride="carousel" data-interval="false">
               <ol class="carousel-indicators d-flex d-md-none">
                  <li data-target="#alumni-carousel" data-slide-to="0" class="active"></li>
                  <li data-target="#alumni-carousel" data-slide-to="1"></li>
                  <li data-target="#alumni-carousel" data-slide-to="2"></li>
               </ol>
               <div class="carousel-inner row w-100 mx-auto" role="listbox">
                  <?php $args = array(
                     'category__in' => array(54, 465, 467),
                     'posts_per_page' => 3,
                     'orderby' => 'date', //new orderby, uses date instead of 'featured priority category
                     'order'   => 'DESC',
                  );
                  $featured_posts = array(
                     get_posts(['category__in' => array(54), 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1])[0],
                     get_posts(['category__in' => array(465), 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1])[0],
                     get_posts(['category__in' => array(467), 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1])[0]
                  );
                  foreach ($featured_posts as $key => $post) :
                  ?>
                     <div class="carousel-item item col-12 col-md-4 p-0 <?php echo ($key == 0) ? "active" : ""; ?>">
                        <div class=" overlay"></div>
                        <div class="row ">
                           <div class="col-12">
                              <?php echo get_the_post_thumbnail($post->ID); ?>
                           </div>
                        </div>
                        <div class="row d-flex  align-items-end">
                           <div class="col-10 py-5 m-auto text-center featuredCarousel">
                              <h6 class="text-center mb-2"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a></h6>
                              <div class="row  mt-5">
                                 <div class="learn_more_container m-auto text-center">
                                    <?php
                                    $categories = get_the_category($post->ID);
                                    echo get_primary_category($categories);
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php endforeach; ?>
               </div>
               <a class="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
               </a>
            </div>
         </div>
      </div>
      <div class="wrapper row mb-5 <?php if (empty($category_select)) echo "d-none" ?>">
         <div class="splide">
            <div class="splide__track">
               <ul class="splide__list">
                  <?php
                  // $category_select = get_field('category_select');
                  $category_select = get_field('category_select');
                  $tag_select = get_field('tag_select');
                  $category_name = get_cat_name($category_select[0]);
                  echo  $category_name;
                  // var_dump($category_select);

                  // $category_selected = echo $featured_post[0]

                  $args = array(
                     // 'category__not_in' => array( 52, 54 ),
                     // 'category__in' => array( $category_id ),
                     // 'category__in' => $category_select,
                     'category__not_in' => array(52, 54, 465, 467, 1),
                     'tag_id' => $tag_select,
                     'posts_per_page' => 5,

                  );
                  $query = new WP_Query($args);
                  $max_num_pages = $query->max_num_pages;
                  if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <li class="splide__slide">
                           <div class="overlay"> </div>
                           <div class="image">
                              <?php the_post_thumbnail(); ?>
                           </div>
                           <div class="title">
                              <!-- <p class="text-center"><?php echo $category_name; ?></p> -->
                              <p class="text-center">


                                 <?php $categories = get_the_category();
                                 if (!empty($categories)) {
                                    echo '<a href="' . esc_url(get_category_link($categories[0]->term_id)) . '">' . esc_html($categories[0]->name) . '</a>';
                                 }
                                 ?>


                              </p>
                              <h6 class="text-center"><?php the_title(); ?></h6>
                              <div class="learn_more_container mx-auto pt-5 text-center">
                                 <a class="learn_more white" href="<?php the_permalink() ?>">Learn More </a>
                              </div>
                           </div>
                        </li>
                  <?php endwhile;
                  endif;
                  wp_reset_postdata();
                  ?>
               </ul>
            </div>
         </div>
      </div>
   <?php endif;  ?>
   <div class="wrapper row testimonial shield mt-5 mb-5 justify-content-center align-items-center">
      <div class="scrollElement col-md-7 col-9  p-5 m-auto text-center">
         <h2>“<?php the_field('quote_2_text') ?>”
         </h2>
      </div>
   </div>
   <div class="wrapper row my-5">
      <div class="scrollElement col-12 text-center">
         <h2>#GlionSpirit</h2>
         <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
            <p class="follow-us">FOLLOW US</p>
         <?php else :   ?>
            <p class="follow-us">SUIVEZ NOUS</p>
         <?php endif ?>
         <div class="row social_view">
            <div class="col-12">
               <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" /></a>
               <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/youtube.png" /></a>
               <a target="_blank" href="https://www.facebook.com/glionswitzerland/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" /></a>
               <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram.png" /></a>
            </div>
         </div>
      </div>
   </div>
   <div class="wrapper row my-5">
      <div class=" col-12 p-0">
         <div>
            <div id="curator-feed-default-layout" class="scrollElement">
               <?php the_field('social_feed_code') ?>
            </div>
         </div>
      </div>
   </div>
   <!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll_magic_script.js"></script> -->
   <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
   <script>
      document.addEventListener('DOMContentLoaded', function() {
         // new Splide( '.splide' ).mount();
         if ($(window).width() >= 992) {
            new Splide('.splide', {
               // rewind : true,

               perPage: 3.5,
               perMove: 1,
               width: '100%',
               type: 'loop',
               //  autoplay: true,
               gap: '1rem',
               pagination: false,
               isNavigation: false,
               arrows: false,
               heightRatio: 0.3,
            }).mount();
         }
         if ($(window).width() >= 768 && $(window).width() <= 991) {
            new Splide('.splide', {
               // rewind : true,

               perPage: 2.5,
               perMove: 1,
               width: '100%',
               type: 'loop',
               //  autoplay: true,
               gap: '1rem',
               pagination: false,
               isNavigation: false,
               arrows: false,
               heightRatio: 0.4,
            }).mount();
         }
         if ($(window).width() <= 767) {
            new Splide('.splide', {
               // rewind : true,

               perPage: 1.2,
               perMove: 1,
               width: '100%',
               type: 'loop',
               //  autoplay: true,
               gap: '1rem',
               pagination: false,
               isNavigation: false,
               arrows: false,
               heightRatio: 0.6,
            }).mount();
         }
      });
   </script>
   <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
   <?php get_footer();
