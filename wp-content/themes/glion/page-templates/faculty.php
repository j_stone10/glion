<!-- Template for the faculty page  -->
<?php
   /*
   Template Name: Faculty 
   */
   
   get_header(); ?>
<div class="container-fluid">
   <div class="row campus hero">
      <div class="background_image">
         <img src="<?php echo get_field('faculty_hero')['url']; ?>" alt="<?php echo get_field('faculty_hero')['alt']; ?>"" />
      </div>
      <div class="scrollElementStart foreground_text col-md-9 col-11 m-auto">
         <h1 class="mb-3"><?php the_field('faculty_hero_title'); ?></h1>
      </div>
   </div>
   <div class="wrapper row  pt-5">
      <div class=" col-12 col-md-9 m-auto">
         <?php echo get_field('intro_text'); ?>
      </div>
   </div>
   <div class="row  my-5">
      <div class="form-selector m-auto">
         <form>
            <div class="form-group">
               <select id="facultyOption" class="form-control"  name="facultyselect">
               <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option selected value="all">Select All</option>
                  <?php   else : ?>
                  <option selected value="all">Tout Sélectionner</option>
                  <?php   endif ; ?>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option value="financeFaculty">Finance</option>
                  <?php   else : ?>
                     <option value="financeFaculty">Finances</option>
                     <?php   endif ; ?>
                     <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option value="businessFaculty">Business and Management</option>
                  <?php   else : ?>
                  <option value="businessFaculty">Commerce et management</option>
                  <?php   endif ; ?>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option value="marketingFaculty">Marketing, Event Management and Luxury</option>
                  <?php   else : ?>
                  <option value="marketingFaculty"> Marketing, Événementiel et Luxe</option>
                  <?php   endif ; ?>

                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option value="entrepreneurshipFaculty">Entrepreneurship and Innovation</option>
                  <?php   else : ?>
                  <option value="entrepreneurshipFaculty">Entrepreneuriat et Innovation</option>
                  <?php   endif ; ?>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option  value="practicalFaculty">Practical Arts</option>
                  <?php   else : ?>
                  <option  value="practicalFaculty">Arts pratiques</option>
                  <?php   endif ; ?>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option  value="generalFaculty">General Education</option>
                  <?php   else : ?>
                  <option  value="generalFaculty">Culture Générale</option>
                  <?php   endif ; ?>
                  <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
                  <option  value="hospitalityFaculty">Hospitality Management</option>
                  <?php   else : ?>
                  <option  value="hospitalityFaculty">Gestion des opérations hôtelières</option>
                  <?php   endif ; ?>
               </select>
               </div>
         </form>
    
      </div>
   </div>
   <div class="row faculty-loop">
      <!--- Finance Faculty ---->
      <?php  
       if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 88 ),
         //  'posts_per_page' => 4,
         );
       else :  
         $args = array(    
            'post_type' => 'faculty',
            'category__in' => array( 383 ),
            //  'posts_per_page' => 4,
            );
         endif;
         
         $finance_faculty = new WP_Query($args);
         $finance_faculty_postCount = 0;
         if($finance_faculty->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border blue-bg financeFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">Finance</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Finances</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($finance_faculty->have_posts())  : $finance_faculty->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border financeFaculty all">
         <a data-toggle="modal" data-target="#finance_Modal<?php  echo $finance_faculty_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $finance_faculty_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
      <!--- Business Management Faculty ---->
      <?php  
          if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 90 ),
         //  'posts_per_page' => 4,
         );
         else :  
            $args = array(    
               'post_type' => 'faculty',
               'category__in' => array( 391 ),
               //  'posts_per_page' => 4,
               );
            endif;


         $business_management = new WP_Query($args);
         $business_management_postCount = 0;
         if($business_management->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border grey-bg businessFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">Business and Management</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Commerce et management</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($business_management->have_posts())  : $business_management->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border businessFaculty all">
         <a data-toggle="modal" data-target="#business_management_Modal<?php  echo $business_management_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $business_management_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
      <!--- marketing Event Managament Faculty ---->
      <?php  
             if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 92 ),
         //  'posts_per_page' => 4,
         );
         else :  
            $args = array(    
               'post_type' => 'faculty',
               'category__in' => array( 393 ),
               //  'posts_per_page' => 4,
               );
            endif;
         
         $marketing = new WP_Query($args);
         $marketing_postCount = 0;
         if($marketing->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border beige-bg marketingFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">Marketing Event Management and Luxury</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Marketing, Événementiel et Luxe</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($marketing->have_posts())  : $marketing->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border marketingFaculty all">
         <a data-toggle="modal" data-target="#marketing_Modal<?php  echo $marketing_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $marketing_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
      <!--- Entrepreneur  Faculty ---->
      <?php  
             if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 94 ),
         //  'posts_per_page' => 4,
         );
         else :  
            $args = array(    
               'post_type' => 'faculty',
               'category__in' => array( 389 ),
               //  'posts_per_page' => 4,
               );
            endif;
         
         $entrepreneur = new WP_Query($args);
         $entrepreneur_postCount = 0;
         if($entrepreneur->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border grey-bg entrepreneurshipFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">Entrepreneurship and innovation</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Entrepreneuriat et Innovation</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($entrepreneur->have_posts())  : $entrepreneur->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border entrepreneurshipFaculty all">
         <a data-toggle="modal" data-target="#entrepreneur_Modal<?php  echo $entrepreneur_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $entrepreneur_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
      <!--- practical arts Faculty ---->
      <?php  
             if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 96 ),
         //  'posts_per_page' => 4,
         );
         else :  
            $args = array(    
               'post_type' => 'faculty',
               'category__in' => array( 395 ),
               //  'posts_per_page' => 4,
               );
            endif;
         
         $practical = new WP_Query($args);
         $practical_postCount = 0;
         if($practical->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border bronze-bg practicalFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">Practical arts</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Arts pratiques</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($practical->have_posts())  : $practical->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border practicalFaculty all">
         <a data-toggle="modal" data-target="#practical_Modal<?php  echo $practical_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $practical_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
      <!--- general Faculty ---->
      <?php  
             if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 98 ),
         //  'posts_per_page' => 4,
         );
         else :  
            $args = array(    
               'post_type' => 'faculty',
               'category__in' => array( 385 ),
               //  'posts_per_page' => 4,
               );
            endif;
         
         $general = new WP_Query($args);
         $general_postCount = 0;
         if($general->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border blue-bg generalFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">General Education</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Culture Générale</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($general->have_posts())  : $general->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border generalFaculty all">
         <a data-toggle="modal" data-target="#general_Modal<?php  echo $general_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $general_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
      <!--- hostpitality Faculty ---->
      <?php  
             if(ICL_LANGUAGE_CODE=='en') :
         $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 100 ),
         //  'posts_per_page' => 4,
         );
         else :  
            $args = array(    
               'post_type' => 'faculty',
               'category__in' => array( 387 ),
               //  'posts_per_page' => 4,
               );
            endif;
         
         $hostpitality = new WP_Query($args);
         $hostpitality_postCount = 0;
         if($hostpitality->have_posts()) : ?>
      <div class="col-12 col-md-3 p-0 white-border grey-bg  hospitalityFaculty all">
         <div class="d-flex justify-content-center align-items-center h-100 w-100">
         <?php   if(ICL_LANGUAGE_CODE=='en') : ?>
            <h4 class="p-5 text-center">Hospitality Management</h4>
            <?php   else : ?>
            <h4 class="p-5 text-center">Gestion des opérations hôtelières</h4>
            <?php   endif ; ?>
         </div>
      </div>
      <?php  while ($hostpitality->have_posts())  : $hostpitality->the_post(); ?>
      <div class="col-12 col-md-3 p-0 white-border hospitalityFaculty all">
         <a data-toggle="modal" data-target="#hostpitality_Modal<?php  echo $hostpitality_postCount ?>" href="#">
         <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
         </a>
      </div>
      <?php  
         $hostpitality_postCount++;  
         endwhile; endif; wp_reset_postdata(); ?>
   </div>
</div>
<!--- Finance Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 88 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 383 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $finance_faculty = new WP_Query($args);
   $finance_faculty_postCount = 0;
   if($finance_faculty->have_posts()) : while ($finance_faculty->have_posts())  : $finance_faculty->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="finance_Modal<?php echo $finance_faculty_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($finance_faculty_postCount == 1 || $finance_faculty_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $finance_faculty_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<!--- Business Management Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 90 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 391 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $business_management = new WP_Query($args);
   $business_management_postCount = 0;
   if($business_management->have_posts()) : while ($business_management->have_posts())  : $business_management->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="business_management_Modal<?php echo $business_management_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($business_management_postCount == 1 || $business_management_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $business_management_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<!--- Marketing Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 92 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 393 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $marketing = new WP_Query($args);
   $marketing_postCount = 0;
   if($marketing->have_posts()) : while ($marketing->have_posts())  : $marketing->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="marketing_Modal<?php echo $marketing_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($marketing_postCount == 1 || $marketing_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $marketing_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<!--- entrepreneur Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 94 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 389 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $entrepreneur = new WP_Query($args);
   $entrepreneur_postCount = 0;
   if($entrepreneur->have_posts()) : while ($entrepreneur->have_posts())  : $entrepreneur->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="entrepreneur_Modal<?php echo $entrepreneur_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($entrepreneur_postCount == 1 || $entrepreneur_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $entrepreneur_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<!--- Practical Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 96 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 395 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $practical = new WP_Query($args);
   $practical_postCount = 0;
   if($practical->have_posts()) : while ($practical->have_posts())  : $practical->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="practical_Modal<?php echo $practical_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($practical_postCount == 1 || $practical_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $practical_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<!--- General Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 98 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 385 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $general = new WP_Query($args);
   $general_postCount = 0;
   if($general->have_posts()) : while ($general->have_posts())  : $general->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="general_Modal<?php echo $general_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($general_postCount == 1 || $general_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $general_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<!--- Hospitality Modals ---->
<?php  
       if(ICL_LANGUAGE_CODE=='en') :
   $args = array(    
   'post_type' => 'faculty',
   'category__in' => array( 100 ),
   //  'posts_per_page' => 4,
   );
   else :  
      $args = array(    
         'post_type' => 'faculty',
         'category__in' => array( 387 ),
         //  'posts_per_page' => 4,
         );
      endif;
   
   $hospitality = new WP_Query($args);
   $hospitality_postCount = 0;
   if($hospitality->have_posts()) : while ($hospitality->have_posts())  : $hospitality->the_post();
   
   ?>
<div class="container-fluid mt-0">
   <div class="row faculty-loop">
      <div class="modal fade alumniModal" id="hostpitality_Modal<?php echo $hospitality_postCount ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
               </div>
               <div class="modal-body">
                  <div id="alumni1" class="left_img_right_text alumni_profile">
                     <div class="row m-0">
                        <div class="col-md-6 col-12  p-0 d-flex justify-content-center align-items-center <?php if ($hospitality_postCount == 1 || $hospitality_postCount == 3  ) { echo 'order-1 order-md-2 beige-bg' ; } else { echo "grey-bg"; } ?>">
                           <div class=" col-9 m-auto text-center text-md-left">
                              <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                           </div>
                        </div>
                        <div class="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center <?php if ($postCount == 1 || $postCount == 3  ) { echo 'order-2 order-md-1' ; }  ?>">
                           <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $hospitality_postCount++;  
   endwhile; endif;  wp_reset_postdata();   ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/stats_counter.js"></script>
<?php get_footer(); 