<?php
   /**
    * The template for displaying all single posts
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package WP_Bootstrap_Starter
    */
   
   get_header(); ?>
<div class="container-fluid">
<div id="alumni1" class="left_img_right_text alumni_profile">
                        <div class="row mb-5">
                           <div class="col-md-6 col-12  p-0  d-flex justify-content-center align-items-center grey-bg">
                              <div class=" col-9 m-auto text-center text-md-left">
                                 <p><?php echo get_field('faculty_content', get_the_ID()) ?></p>
                              </div>
                           </div>
                           <div class="col-md-6 col-12 p-0 d-md-flex  justify-content-center align-items-center ">
                              <img src="<?php echo get_field('faculty_image', get_the_ID())['url']; ?>" alt="<?php echo get_field('faculty_image', get_the_ID())['alt']; ?>"  />
                           </div>
                        </div>
                     </div>

</div>
<?php
get_footer();