<?php
   /**
    * The template for displaying all single posts
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package WP_Bootstrap_Starter
    */
   
   get_header(); ?>
<div class="container-fluid">
   <div class="container breacrumb-trail-container d-none d-md-block absolute">
      <div class="row">
         <div class="col">
            <?php the_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>
<div class="row magazine-header my-5" style="max-width: 100%">
      <div class="col-12 col-md-5  mx-auto my-5">
         <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/the_insider_glion.svg" /> -->
         <a href="<?php echo home_url( '/' ); ?><?php  if(ICL_LANGUAGE_CODE=='en') : ?>magazine<?php else: ?>magazine-fr<?php endif; ?>">
         <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/NEw_insider.svg" style="max-width: 100%; height: auto" />
         </a>
      </div>
    
   </div>
<div class="container-fluid mt-5 post-template-default">
   <?php
      while ( have_posts() ) : the_post(); 
      $post_ID = get_the_ID();
      // echo $post_ID;
      ?>
   <div class="row">
      <div class="col-12 col-md-10 p-md-0 m-auto">
         <h1 class="mb-5"><?php the_title(); ?></h1>
      </div>
   </div>
   <?php  get_template_part( 'template-parts/content', 'modules' );
      // If comments are open or we have at least one comment, load up the comment template.

      endwhile; // End of the loop.
      ?>	
</div>
<div class="row share mx-0 mb-3 ">
   <div class="container">
      <div class="row my-1">
         <div class="col" style="max-width:75px">
            <img class="social_share_toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/share.svg" />
         </div>
         <div class="col social_view pt-1">
            <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo "<a   class='social-share wattsap' data-toggle='tooltip; data-placement='top' title='Share on Wattsap' target='_blank' href='https://api.whatsapp.com/send?text=" . urlencode($actual_link) . "' data-action='share/whatsapp/share'>"; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/email.svg" /></a>
            <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" class='social-share facebook' href="#" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/messenger.svg" /></a>
            <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" class='social-share copy'  href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/whatson.svg" /></a>
            <a data-toggle="tooltip" data-placement="top"  class='social-share email' title="Share via Email" href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mail.svg" /></a>   
         </div>
      </div>
   </div>
</div>

<div class="container similar-stories">
      <div class="col-12 col-md-12">
      <h2 class="text-center stories-title"> <?php if(ICL_LANGUAGE_CODE=='en') : ?>View similar stories <?php else :?>Dans la même rubrique<?php endif ?></h2>
      </div>
   <div class="row latest-news mt-5">
      <div class="col-12 col-md-12 m-auto">
         <div class="row ">
            <?php
               if(ICL_LANGUAGE_CODE=='en') :
               /* Start the Loop */
               $categories = get_the_category();
               // var_dump($categories);
               // $categories = get_the_category();
            //    foreach($categories as $category) {
            //     if ( $category->name !=  'Featured Priority 1') {
            //       //   echo '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a>';
                
            //         $category_id = $category->cat_ID;
            //       //   echo $category_id;
            //       }
            // }
        
               $args = array(
                'category__not_in' => array( 52, 54 ),
                'post__not_in' => array($post_ID),
               //  'category__in' => array( $category_id ),
                'posts_per_page' => 12,
               
            );
            else :
               /* Start the Loop */
               $categories = get_the_category();
               // var_dump($categories);
            //    $categories = get_the_category();
            //    foreach($categories as $category) {
            //    if ( $category->name !=  'FR Featured Priority 1') {
            //       //   echo '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a>';
               
            //       $category_id = $category->cat_ID;
            //       //   echo $category_id;
            //       }
            // }

               $args = array(
               'category__not_in' => array( 499, 501 ),
               'post__not_in' => array($post_ID),
               // 'category__in' => array( $category_id ),
               'posts_per_page' => 12,
               
            );
            endif;
            $query = new WP_Query($args);
            $max_num_pages = $query->max_num_pages;
               if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
            <div class="blogPost col-12 col-md-6 item mb-5">
               <div class="row ">
                  <div class="col-12 d-flex col-md-10 ml-auto mr-auto ">
                     <div class="row mt-auto mb-0 line-after">
                     <a class="w-100" href="<?php the_permalink() ?>">
                        <div class="image-container">
                           <?php the_post_thumbnail(); ?>
                           <div class="image-overlay"></div>
                        </div>
                     </a>
                     </div>
                     <div class="row">
                        <div class="absolute-excerpt col-11 ml-auto col-md-12 ml-auto mr-auto text-center">
                           <p class="category mt-3">
                              <?php $categories = get_the_category();

                              if($categories[0]->cat_name === 'FR Featured Priority 3' || $categories[0]->cat_name === 'FR Featured Priority 4'){
                                 $number_of_categories = $categories[1]->name;
                                 $number_of_categories_links = $categories[1]->term_id;
                              }
                              else{
                                 $number_of_categories = $categories[0]->name;
                                 $number_of_categories_links = $categories[0]->term_id;
                              }

                              if ( ! empty( $categories ) ) {
                                  echo '<a href="' . esc_url( get_category_link( $number_of_categories_links ) ) . '">' . esc_html( $number_of_categories ) . '</a>';
                              }
                              ?></p>
                           <a href="<?php the_permalink() ?>">
                              <h3 class="mt-3"><?php the_title() ?></h3>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php endwhile;
               // glion_pagination($max_num_pages);
         
               
               endif; 
               wp_reset_postdata(); 
               ?>
         </div>
      </div>
   </div>
</div>

<script>
$('.single-post #content article .block-hero-01 .block-editorial-content-01').prepend('<div><div class="row"><div class="col-12"><?php while ( have_posts() ) : the_post(); $post_ID = get_the_ID(); ?> <h3 class="text-center mt-5 mb-0"><?php echo get_the_date(); ?></h3> <?php endwhile; // End of the loop. ?> </div></div></div>');
</script>

<?php

get_footer();