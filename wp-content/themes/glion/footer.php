<?php
   /**
    * The template for displaying the footer
    *
    * Contains the closing of the #content div and all content after.
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package WP_Bootstrap_Starter
    */
   
   ?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
</div><!-- .row -->
</div><!-- .container -->
</div><!-- #content -->
<?php //get_template_part( 'footer-widget' ); ?>
<footer class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
   <div class="container">
      
      <div class="row">
         <div class="col-md-3 px-0 px-md-3 col-12">
            
            <div class="footer-about-us accordion item">
               <?php if(ICL_LANGUAGE_CODE=='en'): ?>
               <span>About</span>
               <?php else : ?>
               <span>A propos</>
               <?php endif; ?>
               <div class="expand-icon d-block d-md-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php wp_nav_menu( array( 'theme_location' => 'work with us' ) ); ?>
            </div>
            </div>
            <div class="item logo">
            <?php if(ICL_LANGUAGE_CODE=='en'): ?>
               <span>Accredited By</span>
               <?php else : ?>
               <span>Accréditations</span>
               <?php endif; ?>
               <div class="expand-icon d-block d-md-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                     <a href="<?php echo get_home_url() ?>/about-us/accreditation-and-recognition/">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/neche_logo.svg" />
                     </a>
               <?php else : ?>
                  <a href="<?php echo get_home_url() ?>/a-propos-de-glion/accreditation-et-reconnaissance/">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/neche_logo.svg" />
                     </a>
               <?php endif; ?>
            </div>
         </div>
         <div class="col-md-3 px-0 px-md-3 accordion col-12">
            <div class="footer-work-with-glion item">
   
               <?php if(ICL_LANGUAGE_CODE=='en'): ?>
               <span>Work With Us</span>
               <?php else : ?>
               <span>Travailler avec nous</span>
               <?php endif; ?>
               <div class="expand-icon d-block d-md-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div class="accordion_text">
               <?php wp_nav_menu( array( 'theme_location' => 'sub menu - partnerships and recruitment' ) ); ?>
            </div>
            </div>
         </div>
         <div class="col-md-3 px-0 px-md-3 accordion col-12">
            <div class="footer-quick-links item">

               <?php if(ICL_LANGUAGE_CODE=='en'): ?>
               <span>Quick Links</span>
               <?php else : ?>
               <span>Liens rapides</span>
               <?php endif; ?>
           
                <div class="expand-icon d-block d-md-none"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
                <div class="accordion_text">
                <?php wp_nav_menu( array( 'theme_location' => 'quick links' ) ); ?>
                </div>
            </div>
            <div class="item d-none d-lg-block">
            <?php if(ICL_LANGUAGE_CODE=='en'): ?>
               <span>Follow Us</span>
               <?php else : ?>
               <span>Suivez nous</span>
               <?php endif; ?>

               <div class="row social">
                  <div class="col-12">
                  <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" /></a>
                     <a target="_blank"  href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/youtube.png" /></a>
                     <a  target="_blank" href="https://www.facebook.com/glionswitzerland/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" /></a>
                     <a target="_blank"   href="https://www.instagram.com/glionhospitalityschool/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram.png" /></a>
              
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-3 px-0 px-md-3 accordion col-12">
            <div class="item">
            <?php if(ICL_LANGUAGE_CODE=='en'): ?>
               <span>Get Started</span>
               <?php else : ?>
               <span>Commencer</span>
               <?php endif; ?>
  
               <?php wp_nav_menu( array( 'theme_location' => 'header actions' ) ); ?>
            </div>
         </div>
      </div>
      <div class="row social d-block d-md-none">
         <div class="col-12">
            <div class="item">
               <div class="row social">
                  <div class="col-3">
                  <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                     <a href="<?php echo get_home_url() ?>/about-us/accreditation-and-recognition/">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/neche_logo.svg" />
                     </a>
               <?php else : ?>
                  <a href="<?php echo get_home_url() ?>/a-propos-de-glion/accreditation-et-reconnaissance/">
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/neche_logo.svg" />
                     </a>
               <?php endif; ?>
          
                  </div>
                  <div class="col-9">
                     <div class="col text-right ml-auto">
                     <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" /></a>
                     <a target="_blank"  href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/youtube.png" /></a>
                     <a  target="_blank" href="https://www.facebook.com/glionswitzerland/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" /></a>
                     <a target="_blank"   href="https://www.instagram.com/glionhospitalityschool/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram.png" /></a>
              
                     </div>
                   </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
<!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->
<?php wp_footer(); ?>

   <script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="https://www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>
   <script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/ScrollMagic.js"></script>
<script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.0/gsap.js"></script>
<script type="text/javascript" src="https://scrollmagic.io/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.0/CSSRulePlugin.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/script.js"></script>

</body>
</html>