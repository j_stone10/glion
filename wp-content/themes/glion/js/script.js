jQuery(document).ready(function ($) {
  var blue = "#243646";
  var bronze = "#9E785B";
  var light_brown = "#C1ABA5";
  var grey = "#E2E0DF";
  var purple = "#B4ADB9";
  var beige = "#F0D4C6";

  // Social Share buttons
  setShareLinks();

  function socialWindow(url) {
    var params = "";
    // this loads share link in a pop-up.
    // var params = "menubar=no,toolbar=no,status=no,width=570,height=570,top=" + top + ",left=" + left;
    //   var left = (screen.width - 570) / 2;
    //   var top = (screen.height - 570) / 2;
    window.open(url, "NewWindow", params);
  }

  function setShareLinks() {
    var pageUrl = encodeURIComponent(document.URL);
    var tweet = encodeURIComponent(
      $("meta[property='og:description']").attr("content")
    );

    $(".social-share.facebook").on("click", function (event) {
      event.preventDefault();
      url = "https://www.facebook.com/sharer.php?u=" + pageUrl;
      socialWindow(url);
    });

    $(".social-share.twitter").on("click", function (event) {
      event.preventDefault();
      url =
        "https://twitter.com/intent/tweet?url=" + pageUrl + "&text=" + tweet;
      socialWindow(url);
    });

    $(".social-share.linkedin").on("click", function (event) {
      event.preventDefault();
      url = "https://www.linkedin.com/shareArticle?mini=true&url=" + pageUrl;
      socialWindow(url);
    });

    $(".social-share.email").on("click", function (event) {
      event.preventDefault();
      url =
        "mailto:?Subject=Checkout out this great page I found on the Glion website&Body=" +
        pageUrl;
      socialWindow(url);
    });
    $(".social-share.copy").on("click", function (event) {
      event.preventDefault();
      var dummy = document.createElement("input"),
        text = window.location.href;
      document.body.appendChild(dummy);
      dummy.value = text;
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
      alert("Link copied to your clipboard");
    });
  }

  /***************************************************
      Add white bg slide up on load
      ***************************************************/
  //  setTimeout(function(){
  //   $('#content').addClass('scroll')
  // },  100);
  //  setTimeout(function(){
  //   $('header').removeClass('scroll')
  // },  280);

  $(".modal").on("show.bs.modal", function () {
    $(this).find(".modal-body").css({
      width: "auto", //probably not needed
      height: "auto", //probably not needed
      "max-height": "100%",
    });
  });

  /***************************************************
Remove animations from blog posts
***************************************************/

  //  setTimeout(function(){
  if ($(".post-template-default").length > 0) {
    if ($(".post-template-default").find(".wrapper").length > 0) {
      // $(".scrollElement").addClass("scrollElementFixed");
      $(".scrollElement").removeClass("scrollElement");
      // alert("remove animation from form for pages where form is at the top");
    } else {
      // alert("doesnt have form in block");
    }
  }
  // }, 200);

  /***************************************************
   Global Scrollmagic transitions
    ***************************************************/

  var rule = CSSRulePlugin.getRule(".container-fluid:after");

  var tl3 = gsap.timeline();

  tl3.to(rule, 0.8, { cssRule: { scaleY: 0, ease: Power4.easeOut } });
  tl3.from(
    ".background_image img",
    1.8,
    {
      scaleX: 1.2,
      scaleY: 1.2,
      transformOrigin: "center",
      ease: Power4.easeOut,
    },
    "-=.8"
  );
  tl3.to("header", 0.8, { margin: 0, ease: Power4.easeOut }, "-=.8");
  tl3.from(
    ".scrollElementStart",
    0.6,
    { y: 150, opacity: 0, ease: Power4.easeOut },
    "-=.3"
  );

  if ($(window).width() >= 768) {
    var controller = new ScrollMagic.Controller();

    $(".wrapper").each(function () {
      var tl = new TimelineMax();
      var target = $(this).find(".scrollElement");

      tl.from(target, 0.5, { y: 150, opacity: 0 });

      var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 0.6,
        // duration: 200,
      })

        .setTween(tl)

        // .addIndicators()
        .addTo(controller);
    });

    var controller2 = new ScrollMagic.Controller();

    $(".wrapper").each(function () {
      var tl2 = new TimelineMax();
      var target2 = $(this).find(".scrollElementOffset");

      tl2.from(target2, 0.5, { y: 150, opacity: 0 });

      var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 0.5,
        // duration: 200,
      })

        .setTween(tl2)

        //  .addIndicators()
        .addTo(controller2);
    });

    $(".wrapper").each(function () {
      var tl2 = new TimelineMax();
      var target2 = $(this).find(".scrollElementOffsetThird");

      tl2.from(target2, 0.5, { y: 200, opacity: 0 });

      var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 0.4,
        // duration: 200,
      })

        .setTween(tl2)

        //  .addIndicators()
        .addTo(controller2);
    });

    $(".wrapper").each(function () {
      var tl2 = new TimelineMax();
      var target2 = $(this).find(".scrollElementOffsetLast");

      tl2.from(target2, 0.5, { y: 250, opacity: 0 });

      var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 0.3,
        // duration: 200,
      })

        .setTween(tl2)

        //  .addIndicators()
        .addTo(controller2);
    });
  }

  /***************************************************
   Blog Scrollmagic transitions
    ***************************************************/

  // var controller = new ScrollMagic.Controller();

  // $(".blogPost").each(function () {
  //   var tl4 = new TimelineMax();
  //   var target = $(this).find(".line-after");

  //   tl4.from(target, 1.8, { scaleX: 1.2, scaleY: 1.2, opacity: 0, transformOrigin: "50% 70%", ease: Power4.easeOut });

  //   var scene = new ScrollMagic.Scene({
  //     triggerElement: this,
  //     triggerHook: 0.6,
  //     // duration: 200,
  //   })

  //     .setTween(tl4)

  //     // .addIndicators()
  //     .addTo(controller);
  // });

  /***************************************************
   Add style classes to gutenburg tables
    ***************************************************/
  if ($(".wp-block-table").length > 0) {
    $("table").addClass("fees_table");
    $("tr").addClass("py-3 opacity-bg").css("height", "40px");
    $("tr").first().removeClass("py-3 opacity-bg");
    $("tr").first().addClass("grey-bg py-2").css("height", "40px");
    $("tr").last().removeClass("py-3 opacity-bg");
    $("tr").last().addClass("grey-bg py-2").css("height", "40px");
    $("td").css("padding-left", "10px");
  }

  /***************************************************
    Add drop shadow to header on first scroll
    ***************************************************/

  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    // console.log(scroll)

    if (scroll > 1) {
      $("header").addClass("scrolled");
    } else {
      $("header").removeClass("scrolled");
    }
  });

  /***************************************************
   Faculty Selector
   ***************************************************/
  $("#facultyOption").on("change", function () {
    let option = $(this).val();
    console.log(option);

    $(".white-border").each(function () {
      if ($(this).hasClass(option)) {
        $(this).css({ display: "flex", position: "relative" });
      } else {
        $(this).css({ display: "none", position: "absolute" });
      }
    });
  });

  /***************************************************
   Prevent body scroll when nav is open
   ***************************************************/

  $(".mega-toggle-block").on("click", function () {
    $("body").toggleClass("nav-open");
  });

  /***************************************************
   Nav Hover Effects
    ***************************************************/

  if ($(window).width() >= 768) {
    $(".mega-menu-link, .mega-sub-menu, .mega-menu-column").hover(
      function () {
        $("header").css("background-color", blue);
        // $('.mega-menu-item a').animate({color: "#ffffff"}, 500 );
        $(".mega-menu-item > a").css("color", "#fff");
        $(".navbar-brand svg path").css("fill", "#fff");
      },
      function () {
        $("header").css("background-color", "#fff");
        $(".mega-menu-item  > a").css("color", bronze);
        // $('.mega-menu-item a').animate({color: blue}, 500 );
        $(".navbar-brand svg path").css("fill", bronze);
      }
    );
  }

  if ($(window).width() <= 767) {
    $("#mega-menu-primary").append(
      '<div class="d-block d-md-none nav_social"><div class="row"><div class="col-6 logo"><a href=""><img src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/themes/glion/images/neche_logo.svg" /></a></div><div class="col-6 icons"><a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/"><img src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/themes/glion/images/linkedin.png" /></a><a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg"><img src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/themes/glion/images/youtube.png" /></a><a target="_blank" href="https://www.facebook.com/glionswitzerland/"><img src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/themes/glion/images/facebook.png" /></a><a target="_blank" href="https://www.instagram.com/glionhospitalityschool/"><img src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/themes/glion/images/instagram.png" /></a></div></div></div>'
    );
  }

  /***************************************************
  Programs Tabs (Not required in React)
  ***************************************************/

  //mutli tab pane for home page
  $("#myTab li").click(function (event) {
    event.preventDefault();
    $(this).siblings().removeClass("active");
    var href = $(this).attr("href");
    if (href == "#bachelors_this") {
      $(".bachelors_this, .bachelors_that").addClass("show");
      $(".bachelors_this, .bachelors_that").removeClass("hide");
      $(".masters_this, .masters_that").removeClass("show");
      $(".masters_this, .masters_that").addClass("hide");
    }
    if (href == "#masters_this") {
      $(".masters_this, .masters_that").addClass("show");
      $(".masters_this, .masters_that").removeClass("hide");

      $(".bachelors_this, .bachelors_that").removeClass("show");
      $(".bachelors_this, .bachelors_that").addClass("hide");
    }
  });

  /***************************************************
  Accordion
  ***************************************************/
  $("h6.accordion_title_black_text:not(.coloured), h2.accordion_title_black_text:not(.coloured)").on("click", function () {
    $(this).toggleClass("active");
    $(this).siblings(".expand-icon").toggleClass("active");
    let expandIcon = $(this).siblings(".expand-icon").find("img");
    if (
      $(expandIcon).attr("src") ==
      "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
    )
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
      );
    else
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
      );
    $(this)
      .siblings(".accordion_text")
      .slideToggle("fast", function () {
        // Animation complete
      });
  });
  $("h6.accordion_title_black_text.coloured, h2.accordion_title_black_text.coloured").on("click", function () {
    $(this).toggleClass("active");
    $(this).siblings(".expand-icon").toggleClass("active");
    let expandIcon = $(this).siblings(".expand-icon").find("img");
    if (
      $(expandIcon).attr("src") ==
      "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
    )
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
      );
    else
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
      );
    $(this)
      .siblings(".accordion_text")
      .slideToggle("fast", function () {
        // Animation complete
      });
  });
  $("h6.accordion_title_white_text, h2.accordion_title_white_text").on("click", function () {
    $(this).toggleClass("active");
    $(this).siblings(".expand-icon").toggleClass("active");
    let expandIcon = $(this).siblings(".expand-icon").find("img");
    if (
      $(expandIcon).attr("src") ==
      "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
    )
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
      );
    else
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
      );
    $(this)
      .siblings(".accordion_text")
      .slideToggle("fast", function () {
        // Animation complete
      });
  });

  $("footer .accordion h6").on("click", function () {
    $(this).toggleClass("active");
    $(this).siblings(".expand-icon").toggleClass("active");
    let expandIcon = $(this).siblings(".expand-icon").find("img");
    if (
      $(expandIcon).attr("src") ==
      "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
    )
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
      );
    else
      $(expandIcon).attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
      );
    $(this)
      .siblings(".accordion_text")
      .slideToggle("fast", function () {
        // Animation complete
      });
  });

  /***************************************************
  Program Structure Slider
  ***************************************************/

  $(".program-structure li").on("click", function () {
    var scrollLeft = $(this).attr("data_target");
    var activeSlide = $(this).attr("activate_slide");
    console.log(activeSlide);
    event.preventDefault();
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
    $(".scrollbar").animate({ scrollTop: 0 }, 200, function () {
      $(".program-structure .slide").each(function (i) {
        $(this).addClass("active_slide");
        $(this).animate(
          {
            right: scrollLeft + "%",
            top: 0,
          },
          500,
          function () {
            if ($(this).attr("id") == activeSlide) {
              $(this).addClass("active_slide");
              $(this).siblings().removeClass("active_slide");
            }
          }
        );
      });
    });
  });

  /***************************************************
    Video function - Add custom play button, reset program video to 0:00 when it ends
    ***************************************************/

  // Apply custom play button/controls to videos on the program pages (pages that contain .program-structure class)
  $(document).ready(function () {
    if ($(".program-structure")[0]) {
      var slidevid = document.querySelector("video");

      if (slidevid) {
        var videotag_width = slidevid.offsetWidth;
        var videotag_height = slidevid.offsetHeight;
      }

      $("video").each(function () {
        $(this).get(0).controls = false;
      });

      // var programplaybtn =
      //   '<div class="play_icon_btn"><img style="left:45%;position:absolute;top:50%;transform:translateY(-50%)" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon" /></div>';
      var programplaybtn = '<div class="play_icon_btn"></div>';

      $(".program-structure video").each(function () {
        $(this).before(programplaybtn);
        // $(this)
        //   .siblings(".play_icon_btn")
        //   .css({
        //     width: $(this).width(),
        //     height: $(this).height(),
        //   });
        // console.log(
        //   $(this),
        //   $(this).siblings(".play_icon_btn"),
        //   $(this).width()
        // );
      });

      callNextVideoPlayBtn("slide1");
    } else {
    }
  });

  // function for when another tab has been selected
  function callNextVideoPlayBtn(activeSlide) {
    var slidevid = document.getElementById(activeSlide).querySelector("video");
    if (slidevid != null) {
      var videotag_width = slidevid.offsetWidth;
      var videotag_height = slidevid.offsetHeight;
    }

    var activeslideclass = ".program-structure .active_slide .play_icon_btn";

    if ($(window).width() <= 767) {
      // console.log(activeSlide, videotag_width);
      // $(".program-structure .play_icon_btn").css({
      //   width: videotag_width,
      //   height: videotag_height,
      // });
      activeslideclass = "#" + activeSlide + " .play_icon_btn";
    }

    if (slidevid != null) {
      var slidevidtime = document
        .getElementById(activeSlide)
        .getElementsByTagName("video");
      var vidsperslide = document
        .getElementById(activeSlide)
        .getElementsByTagName("video").length;

      // this stops the controls disappearing if you have started playing a video, then close the accordion/switch tab, then open it again

      for (var i = 0; i < vidsperslide; i++) {
        if (slidevidtime[i].currentTime == 0) {
          slidevidtime[i].controls = false;
        } else {
          slidevidtime[i].controls = true;
        }
      }

      $(".play_icon_btn").on("click", function (e) {
        console.log("clciked", e);
        // get the video after the clicked .play_icon_btn
        var siblingvideo = this.nextElementSibling;

        // play first video directly under the clicked .play_icon_btn
        siblingvideo.play();
        // add video controls
        siblingvideo.controls = true;

        // hide play button
        $(this).css({ display: "none" });

        // when current video ends, return it to the start, remove default controls and add custom play button back
        siblingvideo.addEventListener("ended", function () {
          siblingvideo.load();
          siblingvideo.controls = false;
          $(this).prev().css({ display: "block" });
          $(activeslideclass).css({
            width: videotag_width,
            height: videotag_height,
          });
        });
      });

      // when the window resizes, calculate the new height of the video (used to center align the custom play button)
      window.onresize = function (event) {
        var videotag_width = slidevid.offsetWidth;
        var videotag_height = slidevid.offsetHeight;
        $(activeslideclass).css({
          width: videotag_width,
          height: videotag_height,
        });
      };
    }
  }

  /***************************************************
  Bootstrap Carousel  
  ***************************************************/

  /*
      Carousel
  // */
  // $('#carousel-example').on('slide.bs.carousel', function (e) {
  //   /*
  //       CC 2.0 License Iatek LLC 2018 - Attribution required
  //   */
  //   var $e = $(e.relatedTarget);
  //   var idx = $e.index();
  //   var itemsPerSlide = 5;
  //   var totalItems = $('.carousel-item').length;

  //   if (idx >= totalItems - (itemsPerSlide - 1)) {
  //     var it = itemsPerSlide - (totalItems - idx);
  //     for (var i = 0; i < it; i++) {
  //       // append slides to end
  //       if (e.direction == "left") {
  //         $('.carousel-item').eq(i).appendTo('.carousel-inner');
  //       }
  //       else {
  //         $('.carousel-item').eq(0).appendTo('.carousel-inner');
  //       }
  //     }
  //   }
  // });

  /***************************************************
  Carousel Slider
  ***************************************************/

  // $('.carousel-toggle img, .svg-click, .campus-left svg, .campus-right svg').on('click', function () {
  //   var scrollLeft = $(this).attr('data_target')
  //   event.preventDefault();
  //   $(this).addClass('active');
  //   $(this).siblings().removeClass('active');
  //   $(this).parent().siblings('.slide').each(function (i) {
  //     $(this).animate({
  //       left: scrollLeft + "%",
  //     }, 500)
  //   });

  // })

  // $('.campus-right svg, .right-toggle ').on('click', function () {
  //   $('.left-toggle').removeClass('active');
  //   $('.right-toggle').addClass('active');
  //   $('.right-toggle, .campus-right svg').hide();
  //   $('.campus-left svg').show()
  // });

  // $('.campus-left svg, .left-toggle').on('click', function () {
  //   $('.right-toggle').removeClass('active');
  //   $('.left-toggle').addClass('active');
  //   $('.left-toggle, .campus-left svg').hide();
  //   $('.campus-right svg').show()
  // });

  // counterRight = -100
  // counterLeft = 0
  // $('.alumni-right svg').on('click', function () {
  //   console.log(counterRight)
  //   $(this).parent().siblings('.slide').each(function (i) {
  //     $(this).animate({
  //       left: (counterRight + "%"),
  //     }, 500)
  //   });
  //   counterRight = counterRight - 100
  //   counterLeft = 0
  //   if (counterRight <= -400) {
  //     $(this).hide()
  //   } else {
  //     $(this).show()
  //   }
  //   $('.alumni-left svg').show()
  // });
  // $('.alumni-left svg').on('click', function () {
  //   console.log(counterLeft)
  //   $(this).parent().siblings('.slide').each(function (i) {
  //     $(this).animate({
  //       left: (counterLeft + "%"),
  //     }, 500)
  //   });
  //   counterRight = -100
  //   counterLeft = counterLeft + 100
  //   $(this).hide()
  // });

  // $('.alumni-left svg, .left-toggle').on('click', function () {
  //   // $('.right-toggle').removeClass('active');
  //   // $('.left-toggle').addClass('active');
  //   // $('.left-toggle, .alumni-left svg').hide();
  //   $('.alumni-right svg').show()
  // });

  /***************************************************
  Alumni Profile Modal Carousel
  ***************************************************/

  $(".alumni a[data-slide-to]").on("click", function () {
    $("#carouselExampleControls").carousel($(this).data("slide-to"));
  });

  /***************************************************
  Social share toggle
  ***************************************************/

  $(".social_share_toggle").on("click", function () {
    event.preventDefault();
    $(".social_view").slideToggle("fast");
  });

  /***************************************************
  Scroll in out fixed CTA's
  ***************************************************/

  var body = document.body,
    html = document.documentElement;

  var height = Math.max(
    body.scrollHeight,
    body.offsetHeight,
    html.clientHeight,
    html.scrollHeight,
    html.offsetHeight
  );
  // console.log(height)
  // console.log(height / 5)
  // console.log(height - 1000)

  $(window).scroll(function (event) {
    if ($(window).width() >= 768) {
      var scroll = $(window).scrollTop();
      // console.log(scroll)

      if (scroll >= 200) {
        $(".fixed-ctas").addClass("slide");
      }
      if (scroll < 200 || scroll > height - 1000) {
        $(".fixed-ctas").removeClass("slide");
      }
    }
  });

  // $(window).scroll(function (event) {
  //   if ($(window).width() < 767) {
  //     var scroll = $(window).scrollTop();
  //     // console.log(scroll)

  //     if (scroll >= 200) {
  //       $('.fixed_open').addClass('slide')
  //     }
  //     if ((scroll < 200) || (scroll > (height - 1000))) {
  //       $('.fixed_open').removeClass('slide')
  //     }
  //   }

  // });

  $(".fixed_open").on("click", function () {
    $(".fixed-ctas").toggleClass("slide");

    if (
      $(".fixed_open img").attr("src") ==
      "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
    )
      $(".fixed_open img").attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
      );
    else
      $(".fixed_open img").attr(
        "src",
        "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K"
      );
  });

  /***************************************************
  Hash link scroll 
  ***************************************************/

  $(document).on("click", 'a[href^="#"]', function (event) {
    event.preventDefault();

    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top - 160,
      },
      500
    );
  });

  /***************************************************
  Footer Toggle Menu
  ***************************************************/
  // $(".footer-about-us h6").on('click', function () {
  //   $(this).toggleClass('active')
  //   $(this).siblings('.expand-icon').toggleClass('active')
  //   let expandIcon = $(this).siblings('.expand-icon').find('img');
  //   if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K')
  //     $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
  //   else
  //     $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
  //   $(".menu-sub-menu-about-glion-container").slideToggle("fast", function () {
  //     // Animation complete
  //   });
  //   $(this).parent().siblings().children('.accordion_text').slideUp();
  //   $(this).parent().siblings().children('h6').removeClass('active')
  //   $(this).parent().siblings().children('.expand-icon').removeClass('active')
  //   $($(this).parent().siblings().children('.expand-icon').find('img')).each(function () {
  //     $(this).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K')

  //   });
  // });
  // $(".footer-work-with-glion  h6").on('click', function () {
  //   $(this).toggleClass('active')
  //   $(this).siblings('.expand-icon').toggleClass('active')
  //   let expandIcon = $(this).siblings('.expand-icon').find('img');
  //   if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K')
  //     $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
  //   else
  //     $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
  //   $(".menu-sub-menu-partnerships-and-recruitment-container").slideToggle("fast", function () {
  //     // Animation complete
  //   });

  // });
  // $(".footer-quick-links  h6").on('click', function () {
  //   $(this).toggleClass('active')
  //   $(this).siblings('.expand-icon').toggleClass('active')
  //   let expandIcon = $(this).siblings('.expand-icon').find('img');
  //   if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K')
  //     $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
  //   else
  //     $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
  //   $(".menu-sub-menu-quick-links-container").slideToggle("fast", function () {
  //     // Animation complete
  //   });
  // });

  $(".modal-footer button").on("click", function (e) {
    var video = $(this).parent().siblings().find("iframe").attr("src");
    var videoId = $(this).parent().siblings().find("iframe");
    console.log(video);
    $(videoId).attr("src", "");
    $(videoId).attr("src", video);
  });

  /***************************************************
      Video modal modifydata_target for when more than 1 block
       ***************************************************/
  if ($(".video_wrapper").length > 0) {
    var countVid = 1;
    $(".video_wrapper").each(function () {
      var dataTargeVid = "#content" + countVid;
      $(this).find("a").attr("data-target", dataTargeVid);
      countVid++;
    });
    var modalVid = 1;
    $(".modal").each(function () {
      var modalID = "content" + modalVid;
      $(this).attr("id", modalID);
      modalVid++;
    });
  }
});
