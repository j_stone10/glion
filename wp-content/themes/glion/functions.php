<?php

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');
function my_theme_enqueue_styles()
{

    $parent_style = 'parent-style';

    // wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style(
        'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
    );
}


// Registering Widgets
register_sidebar(array(
    'name'          => esc_html__('Blog Filter Mobile', 'glion'),
    'id'            => 'blog-filter-mobile',
    'description'   => esc_html__('Add widgets here.', 'glion'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
));
register_sidebar(array(
    'name'          => esc_html__('Blog Filter', 'glion'),
    'id'            => 'blog-filter',
    'description'   => esc_html__('Add widgets here.', 'glion'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
));
register_sidebar(array(
    'name'          => esc_html__('Blog Filter ES', 'glion'),
    'id'            => 'blog-filter-es',
    'description'   => esc_html__('Add widgets here.', 'glion'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
));
register_sidebar(array(
    'name'          => esc_html__('Faculty Filter Mobile', 'glion'),
    'id'            => 'faculty-filter-mobile',
    'description'   => esc_html__('Add widgets here.', 'glion'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
));
register_sidebar(array(
    'name'          => esc_html__('Faculty Filter', 'glion'),
    'id'            => 'faculty-filter',
    'description'   => esc_html__('Add widgets here.', 'glion'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
));
register_sidebar(array(
    'name'          => esc_html__('Search', 'glion'),
    'id'            => 'search',
    'description'   => esc_html__('Add widgets here.', 'glion'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
));
// register_sidebar( array(
//     'name'          => esc_html__( 'Footer 1', 'glion' ),
//     'id'            => 'footer-1',
//     'description'   => esc_html__( 'Add widgets here.', 'glion' ),
//     'before_widget' => '<section id="%1$s" class="widget %2$s">',
//     'after_widget'  => '</section>',
//     'before_title'  => '<h3 class="widget-title">',
//     'after_title'   => '</h3>',
// ) );
// register_sidebar( array(
//     'name'          => esc_html__( 'Footer 2', 'glion' ),
//     'id'            => 'footer-2',
//     'description'   => esc_html__( 'Add widgets here.', 'glion' ),
//     'before_widget' => '<section id="%1$s" class="widget %2$s">',
//     'after_widget'  => '</section>',
//     'before_title'  => '<h3 class="widget-title">',
//     'after_title'   => '</h3>',
// ) );
// register_sidebar( array(
//     'name'          => esc_html__( 'Footer 3', 'glion' ),
//     'id'            => 'footer-3',
//     'description'   => esc_html__( 'Add widgets here.', 'glion' ),
//     'before_widget' => '<section id="%1$s" class="widget %2$s">',
//     'after_widget'  => '</section>',
//     'before_title'  => '<h3 class="widget-title">',
//     'after_title'   => '</h3>',
// ) );
// register_sidebar( array(
//     'name'          => esc_html__( 'Footer 4', 'glion' ),
//     'id'            => 'footer-4',
//     'description'   => esc_html__( 'Add widgets here.', 'glion' ),
//     'before_widget' => '<section id="%1$s" class="widget %2$s">',
//     'after_widget'  => '</section>',
//     'before_title'  => '<h3 class="widget-title">',
//     'after_title'   => '</h3>',
// ) );
register_nav_menus(array(
    'primary' => esc_html__('Primary', 'glion'),
    'mobile-ctas' => esc_html__('Mobile CTAs', 'glion'),
    'quick links' => esc_html__('Footer quick links', 'glion'),
    'work with us' => esc_html__('Footer about us', 'glion'),
    'useful links' => esc_html__('Footer useful links', 'glion'),
    'actions' => esc_html__('Footer actions', 'glion'),
    'header actions' => esc_html__('Header actions', 'glion'),
    'campus actions' => esc_html__('Campus actions', 'glion'),
    'program actions' => esc_html__('Program actions', 'glion'),
    'sub menu - meet glion' => esc_html__('Sub Menu - meet Glion', 'glion'),
    'sub menu - partnerships and recruitment' => esc_html__('Sub Menu - partnerships and recruitment', 'glion'),
    'sub menu - bachelors' => esc_html__('Sub Menu - bachelors', 'glion'),
    'sub menu - masters' => esc_html__('Sub Menu - masters', 'glion'),
    'sub menu - short programs' => esc_html__('Sub Menu - short programs', 'glion'),
    'sub menu - graduate' => esc_html__('Sub Menu - graduate', 'glion'),
    'sub menu - executive education' => esc_html__('Sub Menu - executive education', 'glion'),
    'login and languages' => esc_html__('Login and languages', 'glion'),
    'sub menu - applying to glion' => esc_html__('Sub Menu - applying to glion', 'glion'),
    'sub menu - pre-arrival information' => esc_html__('Sub Menu - pre-arrival information', 'glion'),
    'sub menu - news' => esc_html__('Sub Menu - news', 'glion'),
    'sub menu - events' => esc_html__('Sub Menu - events', 'glion'),
));


//excertp filter 
// add_action( 'pre_get_posts', 'hide_categories' );

// Filter the except length to 20 words.
function wpdocs_custom_excerpt_length($length)
{
    return 20;
}
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);


// Adds breadcrumb.
function the_breadcrumb()
{
    global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        if (ICL_LANGUAGE_CODE == 'es') :
            echo get_home_url();

        else :
            echo home_url('/');
        endif;
        echo '">';
        if (ICL_LANGUAGE_CODE == 'fr') :
            echo 'PAGE D’ACCUEIL';

        else :


            echo 'Home';
        endif;
        echo '</a></li><li class="separator"> - </li>';
        if (is_page()) {
            if ($post->post_parent) {
                $anc = get_post_ancestors($post->ID);
                $title = get_the_title();
                foreach ($anc as $ancestor) {
                    $output = '<li><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li> <li class="separator"> - </li>';
                }
                echo $output;
                echo '<li><u>' . get_the_title() . '</u></li>';
            } else {
                echo '<li><u>' . get_the_title() . '</u></li>';
            }
        }
        if (is_singular('alumni')) {

            echo '<li><a href="' . get_home_url() . '/alumni/">Alumni</a></li> - ';
            echo '<li><u>' . get_the_title() . '</u></li>';
        }
        if (is_single()) {

            if (ICL_LANGUAGE_CODE == 'en') :
                echo '<li><a href="' . get_home_url() . '/magazine/">Magazine</u></a></li> - ';
            //   $categories = get_the_category();
            //     foreach($categories as $category) {
            //      if (  ( $category->name !=  'Featured Priority 1' )) {
            //          echo '<li><a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a></li> - ';
            //      }
            //  }
            else :
                echo '<li><a href="' . get_home_url() . '/magazine-fr/">Magazine</u></a></li> - ';
            endif;
            echo '<li><u>' . get_the_title() . '</u></li>';
        }
    } elseif (is_tag()) {
        single_tag_title();
    } elseif (is_day()) {
        echo "<li>Archive for ";
        the_time('F jS, Y');
        echo '</li>';
    } elseif (is_month()) {
        echo "<li>Archive for ";
        the_time('F, Y');
        echo '</li>';
    } elseif (is_year()) {
        echo "<li>Archive for ";
        the_time('Y');
        echo '</li>';
    } elseif (is_author()) {
        echo "<li>Author Archive";
        echo '</li>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
        echo "<li>Blog Archives";
        echo '</li>';
    } elseif (is_search()) {
        echo "<li>Search Results";
        echo '</li>';
    }
    echo '</ul>';
}

if (!function_exists('wp_bootstrap_starter_posted_on')) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function wp_bootstrap_starter_posted_on()
    {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if (get_the_time('U') !== get_the_modified_time('U')) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
        }

        $time_string = sprintf(
            $time_string,
            esc_attr(get_the_date('c')),
            esc_html(get_the_date())
        );

        // $byline = sprintf(
        //     esc_html_x( 'By %s', 'post author', 'wp-bootstrap-starter' ),
        //     '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
        // );
        $byline = sprintf(
            esc_html_x('By %s', 'post author', 'wp-bootstrap-starter'),
            //'<span class="author vcard">' . esc_html( get_the_author() ) . '</span>'
            '<span class="author vcard">' . esc_html('glion') . '</span>'
        );

        // $posted_on = sprintf(
        //     esc_html_x( '%s', 'post date', 'wp-bootstrap-starter' ),
        //     '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        // );
        $posted_on = sprintf(
            esc_html_x('%s', 'post date', 'wp-bootstrap-starter'),
            $time_string
        );

        echo '<p><span class="byline"> ' . $byline . '</span></p> <p><span class="posted-on">' . $posted_on . '</span></p>'; // WPCS: XSS OK.

        if (!is_single() && !post_password_required() && (comments_open() || get_comments_number())) {
            echo ' | <span class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> ';
            /* translators: %s: post title */
            comments_popup_link(sprintf(wp_kses(__('Leave a Comment<span class="screen-reader-text"> on %s</span>', 'wp-bootstrap-starter'), array('span' => array('class' => array()))), get_the_title()));
            echo '</span>';
        }
    }
endif;




// BLOCK MODULES  
function register_acf_block_types()
{

    // acf_register_block_type(array(
    //     'name'              => 'Callout-01-Image-Overlapping-Left',
    //     'title'             => __('Callout-01 Image Overlapping Left'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-01-image-overlapping-left.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-01-Image-Overlapping-Right',
    //     'title'             => __('Callout-01 Image Overlapping Right'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-01-image-overlapping-right.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-01-Image-Left',
    //     'title'             => __('Callout-01 Image Left'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-01-image-left.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-01-Image-Right',
    //     'title'             => __('Callout-01 Image Right'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-01-image-right.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-01-2-Columns-Text',
    //     'title'             => __('Callout-01 2 Columns Text'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-01-2-columns-text.php',
    // ));
    acf_register_block_type(array(
        'name'              => 'Callout-02-Stats',
        'title'             => __('Callout-02 Stats'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/callout-02-stats.php',
    ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-03-Brands',
    //     'title'             => __('Callout-03 Brands'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-03-brands.php',
    // ));
    acf_register_block_type(array(
        'name'              => 'Callout-04 Alumni',
        'title'             => __('Callout-04-Alumni'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/callout-04-alumni.php',
    ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-05 CTA',
    //     'title'             => __('Callout-05-CTA'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-05-CTA.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-06 Boxes',
    //     'title'             => __('Callout-06-Boxes'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-06-boxes.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Callout-07 3 Boxes',
    //     'title'             => __('Callout-07-3-Boxes'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/callout-07-3boxes.php',
    // ));
    acf_register_block_type(array(
        'name'              => 'Campus-Locations',
        'title'             => __('Campus-Locations'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/campus-locations.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Campus-Locations French Language',
        'title'             => __('Campus-Locations-French_lang'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/campus-locations-french.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Tabarea-01 Programs',
        'title'             => __('Tabarea-01-Programs'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/tabarea-01-programs.php',
    ));

    acf_register_block_type(array(
        'name'              => 'Tabarea-01 Programs French Language',
        'title'             => __('Tabarea-01-Program-French_lang'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/tabarea-01-programs-french.php',
    ));
    // acf_register_block_type(array(
    //     'name'              => 'Tabarea-02 4 Tabs',
    //     'title'             => __('Tabarea-02-4-Tabs'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/tabarea-02-4tabs.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Tabarea-02 3 Tabs',
    //     'title'             => __('Tabarea-02-3-Tabs'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/tabarea-02-3tabs.php',
    // ));
    acf_register_block_type(array(
        'name'              => 'Tabarea-02 2 Tabs',
        'title'             => __('Tabarea-02-2-Tabs'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/tabarea-02-2tabs.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Quote-01',
        'title'             => __('Quote-01'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/quote-01.php',
    ));
    // acf_register_block_type(array(
    //     'name'              => 'Quote-02',
    //     'title'             => __('Quote-02'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/quote-02.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Logos-01',
    //     'title'             => __('Logos-01'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/logos-01.php',
    // ));
    // acf_register_block_type(array(
    //     'name'              => 'Newslist-02',
    //     'title'             => __('Newslist-02'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/newslist-02.php',
    // ));
    acf_register_block_type(array(
        'name'              => 'Accordian-01',
        'title'             => __('Accordian-01'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/accordian-01.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Editorial-Content-01',
        'title'             => __('Editorial-Content-01'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/editorial-content-01.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Image-Video-Block',
        'title'             => __('Image-Video-Block'),
        'description'       => __('A custom block for content with Image or Video.'),
        'render_template'   => 'template-parts/blocks/image-video-block.php',
    ));
    // acf_register_block_type(array(
    //     'name'              => 'FAQ-01',
    //     'title'             => __('FAQ-01'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/faq-01.php',
    // ));	
    // acf_register_block_type(array(
    //     'name'              => 'FAQ-02',
    //     'title'             => __('FAQ-02'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/faq-02.php',
    // ));	 
    // acf_register_block_type(array(
    //     'name'              => 'FAQ-03',
    //     'title'             => __('FAQ-03'),
    //     'description'       => __('A custom testimonial block.'),
    //     'render_template'   => 'template-parts/blocks/faq-03.php',
    // ));			
    acf_register_block_type(array(
        'name'              => 'Hero-01',
        'title'             => __('Hero-01'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/hero-01.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Hero-02 Blog Post With Share',
        'title'             => __('Hero-02-Blog-Post'),
        'description'       => __('A custom Blog Post Hero'),
        'render_template'   => 'template-parts/blocks/hero-02-blog-post.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Video Modal Creator',
        'title'             => __('video-modal-creator'),
        'description'       => __('A custom video block.'),
        'render_template'   => 'template-parts/blocks/video-custom-block.php',
    ));
    acf_register_block_type(array(
        'name'              => 'External News Linker',
        'title'             => __('External News Linker'),
        'description'       => __('A custom external link block.'),
        'render_template'   => 'template-parts/blocks/external_links.php',
    ));
    acf_register_block_type(array(
        'name'              => 'Blog Content',
        'title'             => __('Blog Content'),
        'description'       => __('A custom blog content block.'),
        'render_template'   => 'template-parts/blocks/blog-content.php',
    ));
}
// Check if function exists and hook into setup.
if (function_exists('acf_register_block_type')) {
    add_action('acf/init', 'register_acf_block_types');
}

function wpb_mce_buttons_2($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
		* Callback function to filter the MCE settings
		*/
function my_mce_before_init_insert_formats($init_array)
{

    // Define the style_formats array

    $style_formats = array(
        /*
			* Each array child is a format with it's own settings
			* Notice that each array has title, block, classes, and wrapper arguments
			* Title is the label which will be visible in Formats menu
			* Block defines whether it is a span, div, selector, or inline style
			* Classes allows you to define CSS classes
			* Wrapper whether or not to add a new block-level element around any selected elements
			*/
        array(
            'title' => 'Button',
            'block' => 'span',
            'classes' => 'button',
            'wrapper' => true,

        ),
        array(
            'title' => 'Large',
            'block' => 'span',
            'classes' => 'large',
            'wrapper' => true,
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}
// Attach callback to 'tiny_mce_before_init' 
add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');


// Numeric pagination
function glion_pagination($max_num_pages = 0)
{




    /** Stop execution if there's only 1 page */
    if ($max_num_pages <= 1)
        return;
    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;

    /** Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max_num_pages) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
    echo '<div class="blog-pagination my-5 text-center"><ul>' . "\n";

    /** Previous Post Link */
    if (get_previous_posts_link())
        printf('<li>%s</li>' . "\n", get_previous_posts_link());

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');
        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max_num_pages, $links)) {
        if (!in_array($max_num_pages - 1, $links))
            echo '<li>…</li>' . "\n";
        $class = $paged == $max_num_pages ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max_num_pages)), $max_num_pages);
    }

    /** Next Post Link */
    if (get_next_posts_link())
        printf('<li>%s</li>' . "\n", get_next_posts_link());
    echo '</ul></div>' . "\n";
}

function my_mce4_options($init)
{

    $custom_colours = '
            "253645", "Blue",
            "9E785B", "Bronze",
            "FFFFFF", "White",
            "E2E0DF", "Grey",
            "F0D4C6", "Beige",
            "243646", "Black",
        ';

    // build colour grid default+custom colors
    $init['textcolor_map'] = '[' . $custom_colours . ']';

    // change the number of rows in the grid if the number of colors changes
    // 8 swatches per row
    $init['textcolor_rows'] = 1;

    return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');



//add custom styles to the WordPress editor
function my_custom_styles($init_array)
{




    $style_formats = array(
        // These are the custom styles

        /*
        * Each array child is a format with it's own settings
        * Notice that each array has title, block, classes, and wrapper arguments
        * Title is the label which will be visible in Formats menu
        * Block defines whether it is a span, div, selector, or inline style
        * Classes allows you to define CSS classes
        * Wrapper whether or not to add a new block-level element around any selected elements
        */
        array(
            'title' => 'Quote',
            'block' => 'span',
            'classes' => 'quote',
            'wrapper' => true,
        ),
        array(
            'title' => 'Primary Button',
            'block' => 'a',
            'classes' => 'btn-primary btn',
            'wrapper' => true,
        ),
        array(
            'title' => 'Blog Intro',
            'block' => 'p',
            'classes' => 'blog-intro',
            'wrapper' => false,
        ),
        // array(  
        //     'title' => 'Highlighter',  
        //     'block' => 'span',  
        //     'classes' => 'highlighter',
        //     'wrapper' => true,
        // ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}
// Attach callback to 'tiny_mce_before_init' 
add_filter('tiny_mce_before_init', 'my_custom_styles');

function nullify_empty($value, $post_id, $field)
{
    if (empty($value)) {
        return null;
    }

    return $value;
}

add_filter('acf/format_value/type=repeater', 'nullify_empty', 100, 3);
add_filter('acf/format_value/type=link', 'nullify_empty', 100, 3);
add_filter('acf/format_value/type=image', 'nullify_empty', 100, 3);
add_filter('acf/format_value/type=relationship', 'nullify_empty', 100, 3);
// add_filter('acf/format_value/type=group', 'nullify_empty', 100, 3);
// // not sure if gallery is internally named gallery as well but this should work
// add_filter('acf/format_value/type=gallery', 'nullify_empty', 100, 3); 

// Disable yoast auto redirects 
add_filter('wpseo_premium_post_redirect_slug_change', '__return_true');


// add template slug to graphql query

add_action('graphql_register_types', function () {
    register_graphql_field('Page', 'pageTemplate', [
        'type' => 'String',
        'description' => 'WordPress Page Template',
        'resolve' => function ($page) {
            return get_page_template_slug($page->pageId);
        },
    ]);
});


// whitelistedFieldGroups acf feld groups for grapql pages

// function expose_acf_to_graphql_only($result, $rule, $screen, $field_group)
// {
//     if (!is_graphql_http_request()) {
//         return $result;
//     }

//     $page_template_acf_groups = [
//         'acfProgramFields',
//         'acfAboutUs',
//         'acfCampusPage',
//         'acfCareersPage',
//         'acfFacultyPage',
//         'acfAlumniLanding',
//         'acfHome',
//         'acfVisitCampus',
//     ];

//     if (in_array($field_group['graphql_field_name'], $page_template_acf_groups) && $screen['post_type'] === 'page') {
//         return true;
//     }


//     $post_template_acf_groups = [
//         'blogHeroTest',
//         'blogHeroUpdated',
//         'linkedInShare',

//     ];

//     if (in_array($field_group['graphql_field_name'], $post_template_acf_groups) && $screen['post_type'] === 'post') {
//         return true;
//     }

//     return $result;
// }
// add_filter('acf/location/rule_match', __NAMESPACE__ . '\\expose_acf_to_graphql_only', 10, 4);


function get_primary_category($category)
{
    $useCatLink = true;
    // If post has a category assigned.
    if ($category) {
        $category_display = '';
        $category_link = '';
        if (class_exists('WPSEO_Primary_Term')) {
            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_id());
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term($wpseo_primary_term);
            if (is_wp_error($term)) {
                // Default to first category (not Yoast) if an error is returned
                $category_display = $category[0]->name;
                $category_link = get_category_link($category[0]->term_id);
            } else {
                // Yoast Primary category
                $category_display = $term->name;
                $category_link = get_category_link($term->term_id);
            }
        } else {
            // Default, display the first category in WP's list of assigned categories
            $category_display = $category[0]->name;
            $category_link = get_category_link($category[0]->term_id);
        }
        // Display category
        if (!empty($category_display)) {
            if ($useCatLink == true && !empty($category_link)) {
                return '<span class="post-category"><a href="' . $category_link . '">' . htmlspecialchars($category_display) . '</a></span>';
            } else {
                return '<span class="post-category">' . htmlspecialchars($category_display) . '</span>';
            }
        }
    }
}


//fix for previews showing blank.. thisonly partialy addresses the problem - new acf edits are NOT prviewed when gutenberg editor is used
// function fix_post_id_on_preview($null, $post_id)
// {
//     if (is_preview()) {
//         return get_the_ID();
//     } else {
//         $acf_post_id = isset($post_id->ID) ? $post_id->ID : $post_id;

//         if (!empty($acf_post_id)) {
//             return $acf_post_id;
//         } else {
//             return $null;
//         }
//     }
// }
// add_filter('acf/pre_load_post_id', 'fix_post_id_on_preview', 10, 2);
