<!-- Blog Landing Page!  -->
<?php
/**
 * The main  blog template file
 *
    
 */

get_header(); ?>
<div class="container-fluid">
   <div class="row magazine-header mb-4">
      <div class="col-md-6 col-12 mx-auto">
         <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/the_insider_glion.svg" /> -->
         <img class="blog-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Insider-logo.png" style="max-width: 100%; height: auto" />
      </div>
      <div class="col-12 mr-auto d-flex align-items-center text-md-left text-center">
         <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
            <p class='text-center col'>The magazine of Glion Institute <br class="d-md-none d-block" /> of Higher Education</p>
         <?php else : ?>
            <p class='text-center col'>le magazine de Glion Institut <br class="d-md-none d-block" /> de Hautes Études</p>
         <?php endif; ?>
      </div>
   </div>
</div>
<div class="magazine-header-section">
   <div class="container">
      <div class="row magazine-header d-md-flex d-none">
         <div class="col-12 p-0">
            <ul class="mag_filter mb-0">
               <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                  <li><a href="<?php echo home_url() ?>/magazine/category/leadership-insights/">Leadership insights</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/hospitality-uncovered/">hOSPITALITY UNCOVERED</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/business-of-luxury/">Business of luxury</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/living-well/">LIVING WELL</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/glion-spirit/">GLION SPIRIT</a></li>
               <?php else : ?>
                  <li><a href="<?php echo home_url() ?>/magazine/category/les-voix-du-leadership/">Les voix du leadership</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/au-coeur-de-lhospitality/">Au cœur de l’hospitality</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/nouvelle-vision-du-luxe/">Nouvelle vision du luxe</a></li>
                  <li><a href="<?php echo home_url() ?>/magazine/category/hedonisme/">Hédonisme</a></li>
               <?php endif; ?>
            </ul>
         </div>
      </div>

      <div class="row magazine-header-mob d-md-none d-flex justify-content-center ">
         <div class="col-12">
            <p class="text-center"><?php if (ICL_LANGUAGE_CODE == 'en') : ?>FILTER:<?php else : ?>Activer les filtres<?php endif; ?></p>
         </div>
         <div class="dropdown">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <?php if (ICL_LANGUAGE_CODE == 'en') : ?>All Posts<?php else : ?>Toutes les publications<?php endif; ?>
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
               <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                  <a class="dropdown-item active" href="<?php echo home_url('/'); ?>magazine">All Posts</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/leadership-insights/">Leadership Insights</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/hospitality-uncovered/">Hospitality Uncovered</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/business-of-luxury/">Business of Luxury</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/living-well/">Living Well</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/glion-spirit/">Glion Spirit</a>
               <?php else : ?>
                  <a class="dropdown-item active" href="<?php echo home_url('/'); ?>magazine">Toutes les publications</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/les-voix-du-leadership/">Les voix du leadership</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/au-coeur-de-lhospitality/">Au cœur de l’hospitality</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/nouvelle-vision-du-luxe/">Nouvelle vision du luxe</a>
                  <a class="dropdown-item" href="<?php echo home_url() ?>/magazine/category/hedonisme/">Hédonisme</a>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="container editors-pick mt-5 pt-4">
   <div class="row ">
      <div class="col-12">
         <h1 class="text-center mb-5 mx-auto" style="border-bottom: 3px solid #9E785B; width: max-content;"><?php if (ICL_LANGUAGE_CODE == 'en') : ?>Editor's Pick<?php else : ?>À la une<?php endif; ?></h1>
      </div>
   </div>
   <?php
   if (ICL_LANGUAGE_CODE == 'en') :
      $args = array(
         'category__in' => array(52),
         'posts_per_page' => 1,
      );
   else :
      $args = array(
         'category__in' => array(499),
         'posts_per_page' => 1,
      );
   endif;
   $featured_1_query = new WP_Query($args);
   $featured_1_ID = get_the_ID();
   // var_dump($featured_1_query);
   if ($featured_1_query->have_posts()) : while ($featured_1_query->have_posts()) : $featured_1_query->the_post();
         // $post_id = get_the_ID();
         // echo $post_id;

   ?>
         <div class="row line-after main-featured-image">
            <div class="blogPost col-12 p-0 d-flex justify-content-center">
               <a class="w-100" href="<?php the_permalink() ?>">
                  <div class="image_container">
                     <!-- <img src="<?php echo get_field('hero_image_blog', get_the_ID())['url']; ?>" alt="<?php echo get_field('hero_image_blog', get_the_ID())['alt']; ?>" /> -->
                     <img src="<?php echo get_field('blog_featured_hero_overview', get_the_ID())['url']; ?>" alt="<?php echo get_field('blog_featured_hero_overview', get_the_ID())['alt']; ?>" />
                     <div class="image-overlay"></div>
                  </div>
               </a>
            </div>
         </div>

         <div class="row main-featured-post line-after">
            <div class="col-12">
               <span class='mag_span text-center'>
                  <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                     <?php
                        $categories = get_the_category();
                        foreach ($categories as $category) {
                           if ($category->name !=  'Featured Priority 1') {
                              echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                           }
                        }
                        ?>
                  <?php else : ?>
                     <?php $categories = get_the_category();
                     foreach ($categories as $category) {
                        if ($category->name !=  'FR Featured Priority 1') {
                           echo '<a class="category" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                        }
                     }
                     ?>
                  <?php endif; ?>
                  <p class="post-date text-capitalize d-md-none d-block"><span><?php echo get_the_date('d F Y'); ?></span></p>
                  <h3 class="text-center mb-2"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                  <p class="post-date text-capitalize d-md-block d-none"><span><?php echo get_the_date('d F Y'); ?></span></p>
            </div>
         </div>
   <?php endwhile;

   endif;
   wp_reset_postdata();
   ?>

   <div class="d-block d-md-block">
      <div class="row magazine-header ">
         <?php
         if (ICL_LANGUAGE_CODE == 'en') :
            $args = array(
               'category__in' => array(54),
               'posts_per_page' => 1,
            );
         else :
            $args = array(
               'category__in' => array(501),
               'posts_per_page' => 1,
            );
         endif;
         $featured_2_query = new WP_Query($args);

         if ($featured_2_query->have_posts()) : while ($featured_2_query->have_posts()) : $featured_2_query->the_post();
         ?>

               <div class="col-12 col-md-6">
                  <div class="row">
                     <div class="blogPost col-12 p-0 col-md-10 ml-auto mr-auto ">
                        <a href="<?php the_permalink() ?>">
                           <div class="image-container">
                              <?php the_post_thumbnail(); ?>
                              <div class="image-overlay"></div>
                           </div>
                        </a>
                     </div>
                  </div>
                  <div class="row middle-featured-images line-after">
                     <div class="col-12 col-md-11 m-auto">
                        <span class='mag_span text-center mb-0'>
                           <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name !=  'Featured Priority 2') {
                                    echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }
                              ?>
                           <?php else : ?>
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name != 'FR Featured Priority 2') {
                                    echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }
                              ?>
                           <?php endif; ?>
                        </span>
                        <p class="post-date text-capitalize d-md-none d-block"><span><?php echo get_the_date('d F Y'); ?></span></p>
                        <h3 class="text-center mb-2 mb-3"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        <p class="post-date text-capitalize d-md-block d-none"><span><?php echo get_the_date('d F Y'); ?></span></p>

                     </div>
                  </div>
               </div>
         <?php endwhile;

         endif;
         wp_reset_postdata();
         ?>
         <?php
         if (ICL_LANGUAGE_CODE == 'en') :
            $args = array(
               'category__in' => array(465),
               'posts_per_page' => 1,
            );
         else :
            $args = array(
               'category__in' => array(503),
               'posts_per_page' => 1,
            );
         endif;
         $featured_2_query = new WP_Query($args);

         if ($featured_2_query->have_posts()) : while ($featured_2_query->have_posts()) : $featured_2_query->the_post();
         ?>

               <div class="col-12 col-md-6">
                  <div class="row">
                     <div class="blogPost col-12 p-0 col-md-10 ml-auto mr-auto">
                        <a href="<?php the_permalink() ?>">
                           <div class="image-container">
                              <?php the_post_thumbnail(); ?>
                              <div class="image-overlay"></div>
                           </div>
                        </a>
                     </div>
                  </div>
                  <div class="row middle-featured-images line-after">
                     <div class="col-12 col-md-11 m-auto">
                        <span class='mag_span text-center mb-0'>
                           <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name !=  'Featured Priority 3') {
                                    echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }
                              ?>
                           <?php else : ?>
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name !=  'FR Featured Priority 3') {
                                    echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }
                              ?>
                           <?php endif; ?>
                        </span>
                        <p class="post-date text-capitalize d-md-none d-block"><span><?php echo get_the_date('d F Y'); ?></span></p>
                        <h3 class="text-center mb-2 mb-3"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        <p class="post-date text-capitalize d-md-block d-none"><span><?php echo get_the_date('d F Y'); ?></span></p>
                     </div>
                  </div>
               </div>
         <?php endwhile;

         endif;
         wp_reset_postdata();
         ?>
      </div>
      <?php
      if (ICL_LANGUAGE_CODE == 'en') :
         $args = array(
            'category__in' => array(467),
            'posts_per_page' => 1,

         );
      else :
         $args = array(
            'category__in' => array(505),
            'posts_per_page' => 1,
         );
      endif;
      $featured_2_query = new WP_Query($args);

      if ($featured_2_query->have_posts()) : while ($featured_2_query->have_posts()) : $featured_2_query->the_post();
      ?>

            <div class="row magazine-header main-featured-image last-main-featured-image">
               <div class="col-12 col-md-10 m-auto">
                  <div class="row">
                     <div class="blogPost col-12  line-after">
                        <a href="<?php the_permalink() ?>">
                           <!-- <div class="image-container"> -->
                              <img class="img-fluid" src="<?php echo get_field('blog_featured_hero_overview', get_the_ID())['url']; ?>" alt="<?php echo get_field('blog_featured_hero_overview', get_the_ID())['alt']; ?>" />
                           <!-- </div> -->
                           <div class="image-overlay"></div>
                        </a>
                     </div>
                  </div>
                  <div class="row main-featured-post line-after">
                     <div class="col-12 m-auto">
                        <span class='mag_span text-center mb-0'>
                           <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name !=  'Featured Priority 4') {
                                    echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }
                              ?>
                           <?php else : ?>
                              <?php
                              $categories = get_the_category();
                              foreach ($categories as $category) {
                                 if ($category->name !=  'FR Featured Priority 4') {
                                    echo '<a class="category-editors" href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                 }
                              }
                              ?>
                           <?php endif; ?>
                        </span>
                        <p class="post-date text-capitalize d-md-none d-block"><span><?php echo get_the_date('d F Y'); ?></span></p>
                        <h3 class="text-center mb-2"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        <p class="post-date text-capitalize d-md-block d-none"><span><?php echo get_the_date('d F Y'); ?></span></p>
                     </div>
                  </div>
               </div>
            </div>

      <?php endwhile;

      endif;
      wp_reset_postdata();
      ?>

   </div>
   <div class="container-fluid p-0 d-none d-md-none">
      <div class="row my-md-5 alumni ">
         <div id="alumni-carousel" class="wrapper carousel slide" data-ride="carousel" data-interval="false">
            <ol class="carousel-indicators d-flex d-md-none">
               <li data-target="#alumni-carousel" data-slide-to="0" class="active"></li>
               <li data-target="#alumni-carousel" data-slide-to="1"></li>
               <li data-target="#alumni-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
               <?php
               if (ICL_LANGUAGE_CODE == 'en') :
                  $args = array(
                     'category__in' => array(54, 465, 467),
                     'posts_per_page' => 3,
                  );
               else :
                  $args = array(
                     'category__in' => array(501, 503, 505),
                     'posts_per_page' => 3,
                  );
               endif;
               $featured_carousel = new WP_Query($args);

               $postCount = 0;
               if ($featured_carousel->have_posts()) : while ($featured_carousel->have_posts()) : $featured_carousel->the_post();

                     // $post_id = get_the_ID();
                     // echo $post_id;


               ?>
                     <div class="carousel-item item col-12 col-md-4  p-0  <?php if ($postCount == 0) {
                                                                              echo " active";
                                                                           } ?> ">
                        <div class="row ">
                           <div class="col-12">
                              <?php the_post_thumbnail(); ?>
                           </div>
                        </div>
                        <div class="row d-flex  align-items-end">
                           <div class="col-10 py-5 m-auto text-center featuredCarousel">
                              <h6 class="text-center mb-2"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h6>
                              <div class="row  mt-5">
                                 <div class="learn_more_container m-auto text-center">
                                    <?php
                                    if ($postCount == 0) {
                                       $categories = get_the_category();
                                       foreach ($categories as $category) {
                                          if ($category->name !=  'Featured Priority 2') {
                                             echo '<a href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                          }
                                       }
                                    } else   if ($postCount == 1) {
                                       $categories = get_the_category();
                                       foreach ($categories as $category) {
                                          if ($category->name !=  'Featured Priority 4') {
                                             echo '<a href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                          }
                                       }
                                    } else   if ($postCount == 2) {
                                       $categories = get_the_category();
                                       foreach ($categories as $category) {
                                          if ($category->name !=  'Featured Priority 3') {
                                             echo '<a href="' . esc_url(get_category_link($category->term_id)) . '">' . esc_html($category->name) . '</a>';
                                          }
                                       }
                                    }
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
               <?php
                     $postCount++;
                  endwhile;

               endif;

               wp_reset_postdata();
               ?>
            </div>
            <a class="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
            </a>
         </div>
      </div>
   </div>

   <div class="container-fluid mt-5 pt-5">
      <div class="row">
         <div class="col-12">
            <h2 class="text-center mb-5 mt-md-0 pt-md-0 mt-5 pt-4 mx-auto" style="border-bottom: 3px solid #9E785B; width: max-content;"><?php if (ICL_LANGUAGE_CODE == 'en') : ?>Latest News<?php else : ?>Actualités<?php endif; ?></h2>
         </div>
      </div>
   </div>

   <!-- <div class="container">
   <div class="row magazine-header">
      <div class="col-12 p-0">
         <ul class="mag_filter mb-2">
         <li><a href="<?php echo home_url() ?>/magazine/history-revisited-how-georges-kern-is-transforming-breitling/">Leadership insights</a></li>
            <li><a href="<?php echo home_url() ?>/magazine/category/hospitality-uncovered/">hOSPITALITY UNCOVERED</a></li>
            <li><a href="<?php echo home_url() ?>/magazine/category/business-of-luxury/">Business of luxury</a></li>
            <li><a href="<?php echo home_url() ?>/magazine/category/living-well/">LIVING WELL</a></li>
            <li><a href="<?php echo home_url() ?>/magazine/category/glion-spirit/">GLION SPIRIT</a></li>
         </ul>
      </div>
   </div>
</div> -->


   <div class="container mt-4 overflow-hidden">
      <div class="row latest-news">
         <div class="col-12 col-md-12 m-auto">
            <div class="row ">
               <?php
               /* Start the Loop */


               if (ICL_LANGUAGE_CODE == 'en') :
                  $args = array(
                     'category__not_in' => array(52, 54, 465, 467),
                     'posts_per_page' => 12,
                     'paged' => (get_query_var('paged') ? get_query_var('paged') : 1),
                  );
               else :
                  $args = array(
                     'category__not_in' => array(499, 501, 503, 505),
                     'posts_per_page' => 12,
                     'paged' => (get_query_var('paged') ? get_query_var('paged') : 1),
                  );
               endif;
               $query = new WP_Query($args);
               $max_num_pages = $query->max_num_pages;
               if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                     <div class="blogPost col-12 col-md-6 item mb-5">
                        <div class="row ">
                           <div class="col-12 d-flex col-md-10 ml-auto mr-auto ">
                              <a href="<?php the_permalink() ?>">
                                 <div class="row mt-auto mb-0 line-after">
                                    <div class="image-container">
                                       <?php the_post_thumbnail(); ?>
                                       <div class="image-overlay"></div>
                                    </div>
                                 </div>
                              </a>
                              <div class="row">
                                 <div class="absolute-excerpt col-12 ml-md-auto col-md-12 text-center">
                                    <p class="category mt-3"><?php $categories = get_the_category();
                                                               if (!empty($categories)) {
                                                                  echo '<a href="' . esc_url(get_category_link($categories[0]->term_id)) . '">' . esc_html($categories[0]->name) . '</a>';
                                                               }
                                                               ?></p>
                                    <p class="post-date d-md-none d-block"><span><?php echo get_the_date('d F Y'); ?></span></p>
                                    <h3 class="mt-3"><a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                    </h3>
                                    <p class="post-date d-md-block d-none"><span><?php echo get_the_date('d F Y'); ?></span></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
               <?php endwhile;
                  glion_pagination($max_num_pages);


               endif;
               wp_reset_postdata();
               ?>
            </div>
         </div>
      </div>

   </div>
   <!-- #main -->
   <?php
   get_footer();
