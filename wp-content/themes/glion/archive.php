<!-- Blog Landing Page  -->
<?php
   /**
    * The main  blog template file
    *
    
    */
   
   get_header(); ?>
   <div class="container-fluid">
      <div class="container breacrumb-trail-container d-none d-md-block absolute">
         <div class="row">
            <div class="col">
               <ul id="breadcrumbs">
               <li><a href="<?php echo get_home_url() ?>"><?php  if(ICL_LANGUAGE_CODE=='en') : ?>Home<?php else: ?>PAGE D’ACCUEIL<?php endif; ?></a></li>
                  <li class="separator"> - </li>
                  <li><a href="<?php  if(ICL_LANGUAGE_CODE=='en') : ?><?php echo get_home_url() ?>/magazine/<?php else: ?><?php echo get_home_url() ?>/magazine-fr/<?php endif; ?>">Magazine</a></li>
                  <li class="separator"> - </li>
                  <li><u><?php echo get_the_archive_title();  ?></u></li>
               </ul>
            </div>
         </div>
      </div>

      <div class="row magazine-header mb-4" >
         <div class="col-md-6 col-12 mx-auto my-5">
            <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/the_insider_glion.svg" /> -->
            <a href="<?php echo home_url( '/' ); ?><?php  if(ICL_LANGUAGE_CODE=='en') : ?>magazine<?php else: ?>magazine-fr<?php endif; ?>">
               <img class="blog-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Insider-logo.png" style="max-width: 100%; height: auto" />
            </a>
            <div class="col-12 mr-auto d-flex align-items-center text-md-left text-center">
            <?php  if(ICL_LANGUAGE_CODE=='en') : ?>
            <p class='text-center col'>The magazine of Glion Institute <br class="d-md-none d-block"/> of Higher Education</p>
            <?php else: ?>
            <p class='text-center col'>le magazine de Glion Institut <br class="d-md-none d-block"/> de Hautes Études</p>
            <?php endif; ?>
            </div>
         </div> 
      </div>
</div>

      <?php 
         $title =  single_cat_title("", false);
      ?>

      <div class="magazine-header-section">
         <div class="container">
            <div class="row magazine-header d-md-flex d-none">
               <div class="col-12 p-0">
                  <ul class="mag_filter mb-0">
                  <?php  if(ICL_LANGUAGE_CODE=='en') : ?>
                     <li><a class="<?php if($title == 'LEADERSHIP INSIGHTS') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/leadership-insights/">Leadership insights</a></li>
                     <li><a class="<?php if($title == 'HOSPITALITY UNCOVERED') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/hospitality-uncovered/">hOSPITALITY UNCOVERED</a></li>
                     <li><a class="<?php if($title == 'BUSINESS OF LUXURY') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/business-of-luxury/">Business of luxury</a></li>
                     <li><a class="<?php if($title == 'LIVING WELL') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/living-well/">LIVING WELL</a></li>
                     <li><a class="<?php if($title == 'GLION SPIRIT') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/glion-spirit/">GLION SPIRIT</a></li>
                     <?php else: ?>
                     <li><a class="<?php if($title == 'Les voix du leadership') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/les-voix-du-leadership/">Les voix du leadership</a></li>
                     <li><a class="<?php if($title == 'Au cœur de l’hospitality') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/au-coeur-de-lhospitality/">Au cœur de l’hospitality</a></li>
                     <li><a class="<?php if($title == 'Nouvelle vision du luxe') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/nouvelle-vision-du-luxe/">Nouvelle vision du luxe</a></li>
                     <li><a class="<?php if($title == 'Hédonisme') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/hedonisme/">Hédonisme</a></li>
                     <?php endif; ?>
                  </ul>
               </div>
            </div>

            <div class="row magazine-header-mob d-md-none d-flex justify-content-center ">
               <div class="col-12">
                  <p class="text-center"><?php  if(ICL_LANGUAGE_CODE=='en') : ?>FILTER:<?php else: ?>Activer les filtres<?php endif; ?></p>
               </div>
               <div class="dropdown">
                  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo str_replace('Category: ','',get_the_archive_title()); ?>
                  </a>

                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <?php  if(ICL_LANGUAGE_CODE=='en') : ?>
                     <a class="dropdown-item" href="<?php echo home_url( '/' ); ?>magazine">All Posts</a>
                     <a class="dropdown-item <?php if($title == 'LEADERSHIP INSIGHTS') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/leadership-insights/">Leadership Insights</a>
                     <a class="dropdown-item <?php if($title == 'HOSPITALITY UNCOVERED') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/hospitality-uncovered/">Hospitality Uncovered</a>
                     <a class="dropdown-item <?php if($title == 'BUSINESS OF LUXURY') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/business-of-luxury/">Business of Luxury</a>
                     <a class="dropdown-item <?php if($title == 'LIVING WELL') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/living-well/">Living Well</a>
                     <a class="dropdown-item <?php if($title == 'GLION SPIRIT') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/glion-spirit/">Glion Spirit</a>
                     <?php else: ?>
                     <a class="dropdown-item" href="<?php echo home_url( '/' ); ?>magazine">Toutes les publications</a>
                     <a class="dropdown-item <?php if($title == 'Les voix du leadership') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/les-voix-du-leadership/">Les voix du leadership</a>
                     <a class="dropdown-item <?php if($title == 'Au cœur de l’hospitality') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/au-coeur-de-lhospitality/">Au cœur de l’hospitality</a>
                     <a class="dropdown-item <?php if($title == 'Nouvelle vision du luxe') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/nouvelle-vision-du-luxe/">Nouvelle vision du luxe</a>
                     <a class="dropdown-item <?php if($title == 'Hédonisme') {echo 'active';} ?>" href="<?php echo home_url() ?>/magazine/category/hedonisme/">Hédonisme</a>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <div class="container category-title mt-5 pt-4">
      <div class="row magazine-header mb-5">
         <div class="col-12 col-md-8 mb-5 m-auto">
         <?php  if(ICL_LANGUAGE_CODE=='en') : ?>
            <h2 class="text-center mx-auto pb-0" style="border-bottom: 3px solid #9E785B; width: max-content;"><?php echo str_replace('Category: ','',get_the_archive_title()); ?></h2>
         <?php else: ?>
            <h2 class="text-center mx-auto pb-0" style="border-bottom: 3px solid #9E785B; width: max-content;"><?php echo single_cat_title(); ?></h2>
         <?php endif; ?>
            <?php //the_archive_description( '<div class="taxonomy-description text-center">', '</div>' );  ?>
         </div>
      </div>
   </div>

<div class="container mt-0 archive-news overflow-hidden">
   <div class="row latest-news">
      <div class="col-12 col-md-12 m-auto">
         <div class="row ">
            <?php
               /* Start the Loop */
               $args = array(
                  'category__in' => get_category_by_slug($title)->term_id,
                  'posts_per_page' => 12,
                  'paged' => (get_query_var('paged') ? get_query_var('paged') : 1),
               );
               // var_dump(get_category_by_slug($title)->term_id);
               $query = new WP_Query($args);
               $max_num_pages = $query->max_num_pages;
               // echo $query->found_posts;
               // echo get_option('posts_per_page');
               if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
            <div class="blogPost col-12 col-md-6 item mb-5">
               <div class="row ">
                  <div class="col-12 d-flex col-md-10 ml-auto mr-auto ">
                        <div class="row mt-auto mb-0 line-after archive-post">
                        <a class="w-100" href="<?php the_permalink() ?>">
                           <div class="image-container">
                              <?php the_post_thumbnail(); ?>
                              <div class="image-overlay"></div>
                           </div>
                        </a>
                        </div>
                        <div class="row">
                           <div class="absolute-excerpt col-12 ml-md-auto col-md-12  text-center">
                              <?php 
                              $category_id = get_cat_ID( 'Category Name' );
                              // Get the URL of this category
                              $category_link = get_category_link( $category_id );
                              ?>
                              <p class="category mt-3">
                              <?php  if(ICL_LANGUAGE_CODE=='en') : ?>
                                 <?php echo str_replace('Category: ','',get_the_archive_title()); ?>
                              <?php else: ?>
                                 <?php echo single_cat_title(); ?>
                              <?php endif; ?>
                              </p>
                         
                              <p class="post-date d-md-none d-block"><span><?php echo get_the_date( 'd F Y' ); ?></span></p>
                              <h3 class="mt-3"><a class="blogPost" href="<?php the_permalink() ?>"><?php the_title() ?></a>
                              </h3>
                              <p class="post-date d-md-block d-none"><span><?php echo get_the_date( 'd F Y' ); ?></span></p>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <?php endwhile;
               glion_pagination($max_num_pages);
               
               
               endif; 
               ?>
         </div>
      </div>
   </div>
</div>
<!-- #main -->
<?php
get_footer();